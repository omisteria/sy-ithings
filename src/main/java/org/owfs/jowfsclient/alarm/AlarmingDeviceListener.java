package org.owfs.jowfsclient.alarm;

import org.owfs.jowfsclient.OwfsConnection;
import org.owfs.jowfsclient.OwfsException;

import java.io.IOException;

/**
 * @author Tom Kucharski
 */
public interface AlarmingDeviceListener {

	String getDeviceName();

	void onInitialize(OwfsConnection client) throws IOException, OwfsException;

	void onAlarm(OwfsConnection client) throws IOException, OwfsException;
}
