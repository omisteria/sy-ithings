package com.synapse.ms.ithings;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.logging.SparkUtils;
import com.synapse.ms.ithings.web.ApiController;
import com.synapse.ms.ithings.web.SystemController;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static spark.Spark.*;

public class ApplicationMain {

    public static void main(String[] args) {

        ApplicationOpts.parseArguments(args);
        Logger logger = Logger.getLogger(ApplicationMain.class);
        SparkUtils.createServerWithRequestLog(logger);
        port(8080);
        enableCORS("*", "GET,POST,PUT,DELETE,OPTIONS", "*");


        /*get("/users", (req, res) -> {
            res.type("application/json");
            return new Gson().toJson(
                    new StandardResponse(StatusResponse.SUCCESS,new Gson()
                            .toJsonTree(userService.getUsers())));
        });

        get("/users/:id", (request, response) -> {
            response.type("application/json");
            return new Gson().toJson(
              new StandardResponse(StatusResponse.SUCCESS,new Gson()
                .toJsonTree(userService.getUser(request.params(":id")))));
        });

        options("/users/:id", (request, response) -> {
            response.type("application/json");
            return new Gson().toJson(
              new StandardResponse(StatusResponse.SUCCESS,
                (userService.userExist(
                  request.params(":id"))) ? "User exists" : "User does not exists" ));
        });

        */

        RepresenterManager representerManager = new RepresenterManager();

        Domain shareDomain = DomainDiscoveryService.loadFromFile("eventshare.json");
        if (shareDomain != null) {
            representerManager.buildAllAsync(shareDomain.getConfiguration());
        }

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.schedule(() -> {
            // Arrays.asList("owserver.json")
            Arrays.asList("geeklink.json", "sonoff.json", "mqtt-proxy.json", "modbus.json", "vkmethernet.json", "owserver.json")
                    .forEach(fileName -> {
                        Domain domain = DomainDiscoveryService.loadFromFile(fileName);
                        if (domain != null) {
                            representerManager.buildAllAsync(domain.getConfiguration());
                        }
                    });
        }, 20, TimeUnit.SECONDS);
        scheduler.shutdown();

        try {
            Thread.sleep(20_000);
        } catch (InterruptedException e) {
            // empty
        }

        new SystemController(representerManager);
        new ApiController(representerManager);

        //RulesEngine rulesEngine = new BaseRulesEngine();
        //new Thread(new RulesEngineStarter("rules.js", rulesEngine, representerManager)).start();

        // TODO: fire events after initialization all bridges/representator ???
        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        //SystemEngineContext.getBus().publishAsync(new SystemStartedBusEvent());
    }

    // Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS(final String origin, final String methods, final String headers) {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }
}
