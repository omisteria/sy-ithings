package com.synapse.ms.ithings.core.rules.trigger;

import com.synapse.ms.ithings.core.rules.Rule;

public class BaseRulesEngineTriggerListener implements RulesEngineTriggerListener {

    private Rule rule;

    public BaseRulesEngineTriggerListener(Rule rule) {
        this.rule = rule;
    }

    @Override
    public void execute(RulesEngineTrigger trigger) {
        if(rule.getFilter().filtered(null)) {
            new Thread(() -> {
                try {
                    rule.getHandler().apply(null);
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                }
            }).start();
        }
    }
}
