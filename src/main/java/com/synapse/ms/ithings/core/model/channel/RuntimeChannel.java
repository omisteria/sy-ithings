package com.synapse.ms.ithings.core.model.channel;

import com.synapse.ms.ithings.core.common.datafetch.AcquisitionStrategy;
import com.synapse.ms.ithings.core.common.events.StateObserver;
import com.synapse.ms.ithings.core.common.json.JsonIgnore;
import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.model.represent.Representer;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RuntimeChannel extends AbstractChannel {

    public RuntimeChannel(String gid, Representer representer, Map<String, Object> description, Class stateType, State initState) {
        super(gid, representer, description, stateType, initState);
    }
}
