package com.synapse.ms.ithings.core.rules.impl.entity;

import com.synapse.ms.ithings.core.rules.Filter;
import com.synapse.ms.ithings.core.rules.Rule;
import com.synapse.ms.ithings.core.rules.BaseRulesEngine;
import com.synapse.ms.ithings.core.rules.impl.JSRule;
import com.synapse.ms.ithings.core.rules.trigger.BaseRulesEngineTriggerListener;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;

import java.util.function.Function;

public class RuleStarter {

    private BaseRulesEngine rulesManager;

    public RuleStarter(BaseRulesEngine rulesManager) {
        this.rulesManager = rulesManager;
    }

    public RuleBuilder create(String name) {
        return new RuleBuilder().withName(name);
    }
    public RuleBuilder create() {
        return new RuleBuilder();
    }



    public class RuleBuilder {

        private RulesEngineTrigger trigger;
        private Filter filter = null;
        private Function<Void, Void> handler;
        private String name;

        public RuleBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public RuleBuilder when(RulesEngineTrigger trigger) {
            this.trigger = trigger;
            return this;
        }

        public RuleBuilder filter(Filter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Method declares handler
         * @param handler
         * @return
         */
        public RuleBuilder exec(Function<Void, Void> handler) {
            this.handler = handler;

            Rule rule = new JSRule(trigger, filter, handler);
            trigger.getListeners().add(new BaseRulesEngineTriggerListener(rule));

            rulesManager.registerRule(rule);

            return this;
        }
    }
}
