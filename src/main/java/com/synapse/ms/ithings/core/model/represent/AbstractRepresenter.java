package com.synapse.ms.ithings.core.model.represent;

import com.synapse.ms.ithings.core.common.Identifiable;
import com.synapse.ms.ithings.core.common.datafetch.AcquisitionStrategy;
import com.synapse.ms.ithings.core.common.events.RepresenterListener;
import com.synapse.ms.ithings.core.common.json.JsonIgnore;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.Observable;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.driver.Driver;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepresenter implements
        Representer, Identifiable<String> {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @JsonIgnore
    protected Map<String, Object> description;

    private Observable<ActivateStatus> status;

    private List<RepresenterListener> listeners = new ArrayList<>();

    protected Bridge bridge;

    private final String uid;

    protected RepresenterDataHandler dataHandler;

    private AcquisitionStrategy strategy;

    private List<Channel> channels = new ArrayList<>(0);
    protected List<PredefinedChannel> predefinedChannels = new ArrayList<>();

    public AbstractRepresenter(String uid, Bridge bridge, Map<String, Object> description) {
        this.uid = uid;
        this.bridge = bridge;
        this.description = description;
        status = new Observable(ActivateStatus.UNINITIALIZED);
    }


    public Driver getDriver() {
        return null;
    }

    @Override
    public String getGid() {
        return uid;
    }

    public void setDataHandler(RepresenterDataHandler handler) {
        dataHandler = handler;
        getChannels().forEach(channel -> {
            channel.setDataHandler(handler);

            // TODO needs ??????
            channel.getState().addListener(event -> dataHandler.onChannelStateChanged(event, channel));
        });
    }

    public RepresenterDataHandler getDataHandler() {
        return dataHandler;
    }

    public void activate() throws Exception {
        if (strategy != null) {
            strategy.activate(dataHandler);
        }
    }

    @Override
    public void post(String channelId, Command command) throws Exception {
        Channel channel = getChannel(channelId);

        if (channel != null) {
            channel.post(command);
        }
    }

    @Override
    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

    @Override
    public List<Channel> getChannels() {
        return Collections.unmodifiableList(new ArrayList<>(this.channels));
    }

    @Override
    public void addChannel(Channel channel) {
        channels.add(channel);
    }

    @Override
    public Channel getChannel(String channelId) {
        for (Channel channel : this.channels) {
            if (channel.getGid().equals(channelId)) {
                return channel;
            }
        }
        return null;
    }

    public Map<String, Object> getDescription() {
        return description;
    }

    public void setDescription(Map<String, Object> description) {
        this.description = description;
    }

    @Override
    public PlainConfiguration getConfiguration() {
        return null;
    }

    @Override
    public Observable<ActivateStatus> getActivateStatus() {
        return status;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public AcquisitionStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(AcquisitionStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public List<PredefinedChannel> getPredefinedChannels() {
        return Collections.unmodifiableList(predefinedChannels);
    }

    @Override
    public Bridge getBridge() {
        return bridge;
    }

    @Override
    public String toString() {
        return "Representer{" +
                "uid='" + uid + '\'' +
                ", bridge=" + bridge +
                ", channels=" + channels.size() +
                '}';
    }
}
