package com.synapse.ms.ithings.core.rules.impl;

import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.RuleScriptException;
import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.ScriptLoadEngine;

import javax.script.*;

public class JavaScriptLoadEngine implements ScriptLoadEngine {

    private ScriptEngine engine;
    private Bindings bindings;
    private ScriptContext context;
    private RulesEngine rulesEngine;
    private RepresenterManager representerManager;

    public JavaScriptLoadEngine(RulesEngine rulesEngine, RepresenterManager representerManager) {
        this.rulesEngine = rulesEngine;
        this.representerManager = representerManager;
    }

    @Override
    public void start() {
        System.setProperty("nashorn.args", "--language=es6");

        ScriptEngineManager mgr = new ScriptEngineManager();
        engine = mgr.getEngineByName("Nashorn");
        bindings = engine.createBindings();
        context = new SimpleScriptContext();
        context.setBindings(bindings, ScriptContext.ENGINE_SCOPE);

        rulesEngine.bindingEngineValues(this, representerManager);
    }

    public void bind(String name, Object obj) {
        bindings.put(name, obj);
    }

    public void loadScript(String script) throws RuleScriptException {
        try {
            engine.eval(script, context);
        } catch (ScriptException e) {
            throw new RuleScriptException(e);
        }
    }



    public void createGroup() {
        System.out.println("++++++++++++++++++++++");
    }

    public void createAlias(String aliasName, String entityId) {

    }
}
