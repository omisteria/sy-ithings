package com.synapse.ms.ithings.core.utils;

import com.synapse.ms.ithings.ApplicationOpts;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.stream.Collectors;

public class ConfigContent {

    public String loadContent(String fileName) throws UnsupportedEncodingException, FileNotFoundException {
        fileName = buildFilePath(fileName);
        if (fileName.contains(File.separator)) {
            return loadContentAsFileStream(fileName);
        } else {
            return loadContentAsResourceStream(fileName);
        }
    }

    public String loadContentAsResourceStream(String fileName) throws UnsupportedEncodingException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        return bufferedReader.lines().collect(Collectors.joining("\n"));
    }

    public String loadContentAsFileStream(String fileName) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileName))));
        return bufferedReader.lines().collect(Collectors.joining("\n"));
    }

    public String buildFilePath(String fileName) {
        String configLocation = ApplicationOpts.getProperty(ApplicationOpts.CONFIG_DIR);
        if (configLocation != null) {
            if (!StringUtils.endsWith(configLocation, File.separator)) {
                configLocation = configLocation + File.separator;
            }
            return configLocation + fileName;
        }
        return fileName;
    }
}
