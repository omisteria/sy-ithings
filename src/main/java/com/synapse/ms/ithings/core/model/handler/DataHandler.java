package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.common.datafetch.AcquisitionStrategy;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;

public interface DataHandler<E, S> {

    default void initialize() {}

    void sendRequest(Command command) throws Exception;
    void onReceivedResponse(S responseData);

    // AcquisitionStrategy getAcquisitionStrategy();
}
