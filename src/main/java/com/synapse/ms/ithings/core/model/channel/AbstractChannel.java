package com.synapse.ms.ithings.core.model.channel;

import com.synapse.ms.ithings.core.common.datafetch.AcquisitionStrategy;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.Observable;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.represent.Representer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public abstract class AbstractChannel implements Channel {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private Map<String, Object> description;

    // State
    private Observable<State> state;
    private Class stateType;
    private State initState;

    private final String gid;
    private String selector;
    private boolean inverseState;

    protected Representer representer;

    private String label;

    private ChannelType type;


    private Observable<ActivateStatus> status;


    protected RepresenterDataHandler dataHandler;
    private AcquisitionStrategy strategy;

    public AbstractChannel(String gid, Representer representer, Map<String, Object> description, Class stateType, State initState) {
        this.gid = gid;
        this.representer = representer;
        this.description = description;
        this.status = new Observable(ActivateStatus.UNINITIALIZED);
        this.state = new Observable(UnDefType.UNDEF);
        this.stateType = stateType;
        this.initState = initState;
    }

    public void activate() throws Exception {
        //driver.activate();

        /*if (strategy != null) {
            strategy.activate(dataHandler);
        }*/
    }

    public String getGid() {
        return gid;
    }

    public Representer getRepresenter() {
        return representer;
    }

    public Map<String, Object> getDescription() {
        return description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDataHandler(RepresenterDataHandler handler) {
        dataHandler = handler;
    }

    @Override
    public RepresenterDataHandler getDataHandler() {
        return dataHandler;
    }

    public ChannelType getType() {
        return type;
    }

    public void setType(ChannelType type) {
        this.type = type;
    }

    public AcquisitionStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(AcquisitionStrategy strategy) {
        this.strategy = strategy;
    }

    public Observable<ActivateStatus> getActivateStatus() {
        return status;
    }

    public Observable<State> getState() {
        return state;
    }

    public Class getStateType() {
        return stateType;
    }

    public void setStateType(Class stateType) {
        this.stateType = stateType;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    @Override
    public boolean isInverseState() {
        return inverseState;
    }

    @Override
    public void setInverseState(boolean inverseState) {
        this.inverseState = inverseState;
    }

    @Override
    public void post(Command command) throws Exception {
        if (dataHandler != null) {

            if (!status.getValue().equals(ActivateStatus.ONLINE)) {
                return;
            }

            if (!getState().getValue().equals(command)) {
                dataHandler.sendRequest(command, gid);
            }
        } else {
            logger.warn("Handler for channel ["+getGid()+"] is not exists");
        }
    }

    public State getInitState() {
        return initState;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "gid='" + gid + '\'' +
                '}';
    }
}
