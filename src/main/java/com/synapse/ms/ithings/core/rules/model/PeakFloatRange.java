package com.synapse.ms.ithings.core.rules.model;

@Deprecated
public class PeakFloatRange implements Range<Double> {
    private FloatRange leftPart;
    private FloatRange rightPart;

    public PeakFloatRange(Double start, Double devider, Double end) {
        if (start < devider && devider < end) {
            leftPart = new FloatRange(start, devider);
            rightPart = new FloatRange(devider, end);
        } else {
            throw new IllegalStateException("Incorrect ranges");
        }
    }

    @Override
    public Double getStart() {
        return leftPart.getStart();
    }

    @Override
    public Double getEnd() {
        return rightPart.getEnd();
    }

    @Override
    public double index(Double value) {
        return leftPart.index(value) * (1 - rightPart.index(value));
    }

    @Override
    public Double partBy(double index) {
        throw new IllegalStateException("Doesn't have implementation");
    }

    @Override
    public Range<Double> reverse() {
        throw new IllegalStateException("Doesn't have implementation");
    }
}
