package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.model.FloatRange;
import com.synapse.ms.ithings.core.rules.model.IntRange;
import com.synapse.ms.ithings.core.rules.model.RangeValueHolder;

import java.util.Random;
import java.util.function.Function;

public class FuncProvider {

    public int random(int min, int max) {
        Random r = new Random();
        return r.ints(min, (max + 1)).findFirst().getAsInt();
    }

    public Function<Void, Void> test() {
        return null;
    }

    public RangeValueHolder rangeConverter(double s1, double s2, int t1, int t2) {
        return new RangeValueHolder<>(
                new FloatRange(s1, s2),
                new IntRange(t1, t2));
    }
}
