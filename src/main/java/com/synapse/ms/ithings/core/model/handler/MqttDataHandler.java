package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.common.types.Command;

public interface MqttDataHandler {

    default void initialize() {}

    void sendRequest(String toTopic, Command command) throws Exception;
    void onReceivedResponse(String fromTopic, byte[] responseData);
}
