package com.synapse.ms.ithings.core.model.handler;

import java.util.concurrent.Executors;

public class RequestExecutor {

    // TODO add Map of Executors by type of request
    // TODO add engine for trying, and check result of write value to device
    public static void doSubmit(Runnable request) {
        Executors.newSingleThreadExecutor().submit(() -> {
            request.run();
        });
    }
}
