package com.synapse.ms.ithings.core.model.channel;

public enum ChannelStatus {

    UNINITIALIZED,
    INITIALIZING,
    UNKNOWN,
    ONLINE,
    OFFLINE
}
