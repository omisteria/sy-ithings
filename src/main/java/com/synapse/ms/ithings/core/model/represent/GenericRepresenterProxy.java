package com.synapse.ms.ithings.core.model.represent;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class GenericRepresenterProxy<T extends Representer> implements InvocationHandler {

    private Representer representer;

    private GenericRepresenterProxy(Representer representer) {
        this.representer = representer;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Representer> Representer newInstance(Representer representer) {
        return (Representer) Proxy.newProxyInstance(
                representer.getClass().getClassLoader(),
                representer.getClass().getInterfaces(),
                new GenericRepresenterProxy(representer));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        Class<?> declaringClass = method.getDeclaringClass();

        for (Class<?> interf : representer.getClass().getInterfaces()) {

            if (declaringClass.isAssignableFrom(interf)) {
                try {

                    return method.invoke(representer, args);

                } catch (InvocationTargetException e) {
                    throw e.getTargetException();
                }
            }
        }

        System.out.println("will be requested by name " + representer.getGid() + "-" + method.getName());

        return null;
    }

    public static void main(String[] args) {

        //Representer rep = (Representer) GenericRepresenterProxy.newInstance(new Representer());
        //rep.getChannels();
    }
}
