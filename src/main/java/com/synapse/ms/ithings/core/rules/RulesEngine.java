package com.synapse.ms.ithings.core.rules;

import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;

import java.util.Timer;
import java.util.function.Function;

public interface RulesEngine {

    void start();

    void bindingEngineValues(ScriptLoadEngine sle, RepresenterManager representerManager);

    RulesEngineTrigger getOrCreateTrigger(String triggerKey, Function<String, RulesEngineTrigger> factoryFunc);

    void registerRule(Rule rule);

    void registerTimer(Timer timer);
    void unregisterTimer(Timer timer);
}
