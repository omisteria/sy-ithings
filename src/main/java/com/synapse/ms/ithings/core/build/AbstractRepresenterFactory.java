package com.synapse.ms.ithings.core.build;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.common.types.StateClassHolder;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.config.ConfKey;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.channel.RuntimeChannel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractRepresenterFactory implements RepresenterFactory {

    protected final String GID_DELIM = ".";

    protected Logger logger = Logger.getLogger(getClass().getSimpleName());

    protected Domain domain;

    public AbstractRepresenterFactory(Domain domain) {
        this.domain = domain;
    }

    public Bridge buildBridgeWithUnits(PlainConfiguration conf) {
        try {

            final Bridge bridge = createBridge(conf.properties());
            bridge.configure();

            List<Map<String, Object>> representersMaps = (List<Map<String, Object>>) conf.get(ConfKey.representers);

            if (representersMaps != null) {
                representersMaps.forEach(repdesc -> {

                    String representerGid = bridge.getGid() + GID_DELIM + repdesc.get(ConfKey.gid);

                    try {
                        Representer representer = createRepresenter(repdesc, bridge);

                        List<Map<String, Object>> channels = (List<Map<String, Object>>) repdesc.get(ConfKey.channels);

                        if (channels != null) {
                            channels.forEach(des -> {
                                // create available RuntimeChannel channels
                                Channel representerChannel = createChannel(des, representer);
                                if (representerChannel != null) {
                                    representer.addChannel(representerChannel);
                                }
                            });
                        }

                        representer.configure();

                        bridge.addRepresenter(representer);

                    } catch (Exception e) {
                        logger.error("Can't create representer with ID: "+representerGid, e);
                        throw new IllegalStateException("Can't create representer with ID: "+representerGid, e);
                    }

                });
            }

            return bridge;

        } catch (Exception e) {
            throw new IllegalStateException("Error due creating representers", e);
        }
    }

    @Override
    public Bridge createBridge(Map<String, Object> description) throws Exception {

        String bridgeClassName = domain.getBridgeClassName();
        String bridgeGid = (String) description.get(ConfKey.gid);

        try {
            Constructor c = Class.forName(bridgeClassName).getConstructor(String.class, Map.class);
            return (Bridge) c.newInstance(new Object[]{bridgeGid, description});
        } catch (Exception e) {
            throw new IllegalStateException("Can't create bridge ["+bridgeGid+", "+bridgeClassName+"]", e);
        }
    }

    @Override
    public Representer createRepresenter(Map<String, Object> description, Bridge bridge) throws Exception {

        String representerDriver = (String)description.get(ConfKey.driver);
        String fullRepresenterGid = bridge.getGid() + GID_DELIM + description.get(ConfKey.gid);

        try {
            String driverName = domain.resolveRepresenterClassName(representerDriver);
            if (StringUtils.isEmpty(driverName)) {
                throw new IllegalStateException("Class name for driver ["+representerDriver+"] not found");
            }

            Constructor c = Class.forName(driverName)
                    .getConstructor(String.class, Bridge.class, Map.class);

            return (Representer) c.newInstance(new Object[]{fullRepresenterGid, bridge, description});
        } catch (Exception e) {
            throw new IllegalStateException("Can't create representer ["+fullRepresenterGid+", "+representerDriver+"]", e);
        }
    }

    /**
     * Create RuntimeChannel from description & PredefinedChannel if it exists
     * @param channelDescription
     * @param representer
     * @return
     */
    @Override
    public Channel createChannel(Map<String, Object> channelDescription, Representer representer) {

        String channelGid = (String) channelDescription.get(ConfKey.gid);
        String fullChannelGid = representer.getGid() + GID_DELIM + channelGid;
        PredefinedChannel predefinedChannel = null;
        Optional<PredefinedChannel> optPredefinedChannel = findPredefinedChannel(representer, channelGid);
        Class defaultStateType = findDefaultStateType(channelDescription);

        // Initialize state
        State initState = UnDefType.UNDEF;

        if (optPredefinedChannel.isPresent()) {
            predefinedChannel = optPredefinedChannel.get();
            initState = predefinedChannel.getInitState();
        }

        if (channelDescription.containsKey(ConfKey.initState)) {
            try {
                initState = new RawType(((String) channelDescription.get(ConfKey.initState)).getBytes())
                        .as(defaultStateType);
            } catch (Exception e) {
                String message = String.format("Can't initialize value from description for channel '%s'", channelGid);
                logger.warn(message);
            }
        }

        RuntimeChannel ch = new RuntimeChannel(fullChannelGid, representer, channelDescription, defaultStateType, initState);

        if (channelDescription.containsKey(ConfKey.selector)) {
            ch.setSelector((String) channelDescription.get(ConfKey.selector));
        }
        if (channelDescription.containsKey(ConfKey.label)) {
            ch.setLabel((String) channelDescription.get(ConfKey.label));
        }
        if (channelDescription.containsKey(ConfKey.inverse)) {
            ch.setInverseState((Boolean)channelDescription.get(ConfKey.inverse));
        }

        if (predefinedChannel != null) {
            ch.setType(predefinedChannel.getType());
        }

        if (channelDescription.containsKey(ConfKey.type)) {
            ChannelType type = ChannelType.valueOf((String) channelDescription.get(ConfKey.type));
            if (type != null) {
                ch.setType(type);
            }
        }

        return ch;
    }

    private Optional<PredefinedChannel> findPredefinedChannel(Representer representer, String shortId) {
        return representer.getPredefinedChannels()
                .stream()
                .filter(pdch -> pdch.getGid().equals(shortId))
                .findFirst();
    }

    protected Class findDefaultStateType(Map<String, Object> description) {
        Class defaultStateType = RawType.class;
        String nameStateType = (String) description.get(ConfKey.stateType);
        if (StringUtils.isNoneEmpty(nameStateType)) {
            StateClassHolder stHolder = StateClassHolder.findByShortName(nameStateType);
            if (stHolder != null) {
                defaultStateType = stHolder.getStateClass();
            }
        }
        return defaultStateType;
    }
}
