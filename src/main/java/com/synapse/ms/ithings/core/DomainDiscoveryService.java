package com.synapse.ms.ithings.core;

import com.synapse.ms.ithings.binding.eventshare.EventShareDomain;
import com.synapse.ms.ithings.binding.geeklink.GeeklinkDomain;
import com.synapse.ms.ithings.binding.modbus.ModbusDomain;
import com.synapse.ms.ithings.binding.mqtt.MqttDomain;
import com.synapse.ms.ithings.binding.owserver.OwserverDomain;
import com.synapse.ms.ithings.binding.sonoff.SonoffDomain;
import com.synapse.ms.ithings.binding.vkmodule.VkModuleDomain;
import com.synapse.ms.ithings.core.config.ConfKey;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.utils.ConfigContent;

import java.util.HashMap;
import java.util.Map;

public class DomainDiscoveryService {

    private static Map<String, Domain> domains = new HashMap<>();

    static {
        registerDomain(new OwserverDomain());
        registerDomain(new SonoffDomain());
        registerDomain(new GeeklinkDomain());
        registerDomain(new MqttDomain());
        registerDomain(new VkModuleDomain());
        registerDomain(new ModbusDomain());
        registerDomain(new EventShareDomain());
    }

    public static synchronized void registerDomain(Domain domain) {
        domains.put(domain.getGid(), domain);
    }

    public static synchronized Domain getDomain(String domainGid) {
        return domains.get(domainGid);
    }

    public static Domain loadFromFile(String fileName) {
        PlainConfiguration configuration = PlainConfiguration.loadFromFile(fileName);
        if (configuration != null) {
            String domainId = (String) configuration.get(ConfKey.domain);
            Domain domain = domains.get(domainId);
            if (domain != null) {
                domain.setConfiguration(configuration);
                return domain;
            }
        }
        return null;
    }

}
