package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.Filter;
import com.synapse.ms.ithings.core.rules.RulesModule;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateProvider {

    private RulesModule rulesModule;

    public DateProvider(RulesModule rulesModule) {
        this.rulesModule = rulesModule;
    }

    // Date.between('4.*.*','6.*.*')
    // Date.between('*.4.*','*.6.*')
    // Date.between('4.5.*','6.7.*')
    // Date.between('4.05.2000','6.07.2001')
    public Filter between(String from, String to) {

        DatePattern pFrom = DatePattern.parse(from);
        DatePattern pTo = DatePattern.parse(to);

        return (Filter<Object>) data -> {
            LocalDate now = LocalDate.now();
            if (pFrom.compareTo(pTo) == -1) {
                return pFrom.isBefore(now) && pTo.isAfter(now);
            } else if (pFrom.compareTo(pTo) == 1) {
                return !(pTo.isBefore(now) && pFrom.isAfter(now));
            } else  {
                return false;
            }
        };
    }

    public LocalDateTime now() {
        return LocalDateTime.now();
    }

    public Filter is(String pattern) {

        DatePattern dp = DatePattern.parse(pattern);

        return (Filter<Object>) data -> {
            LocalDate now = LocalDate.now();
            return dp.isMatch(now);
        };
    }

    public static class DatePattern implements Comparable<DatePattern> {

        public final static String CL_DAY = "day";
        public final static String CL_MONTH = "month";
        public final static String CL_DAY_MONTH = "day_month";
        public final static String CL_DATE = "date";

        public String compareLevel;
        public String[] parts;
        public int day, month, year;

        public static DatePattern parse(String pattern) {
            String[] parts = pattern.split("\\.");
            DatePattern dp;

            if (parts.length == 3) {
                // '4.*.*'
                if (!parts[0].equals("*") && parts[1].equals("*") && parts[2].equals("*")) {
                    dp = new DatePattern(CL_DAY);
                    dp.day = Integer.parseInt(parts[0]);
                    return dp;
                // '*.4.*'
                } else if (parts[0].equals("*") && !parts[1].equals("*") && parts[2].equals("*")) {
                    dp = new DatePattern(CL_MONTH);
                    dp.month = Integer.parseInt(parts[1]);
                    return dp;
                // '1.4.*'
                } else if (!parts[0].equals("*") && !parts[1].equals("*") && parts[2].equals("*")) {
                    dp = new DatePattern(CL_DAY_MONTH);
                    dp.day = Integer.parseInt(parts[0]);
                    dp.month = Integer.parseInt(parts[1]);
                    return dp;
                // '4.05.2000'
                } else if (!parts[0].equals("*") && !parts[1].equals("*")) {
                    dp = new DatePattern(CL_DATE);
                    dp.day = Integer.parseInt(parts[0]);
                    dp.month = Integer.parseInt(parts[1]);
                    dp.year = Integer.parseInt(parts[2]);
                    return dp;
                } else {
                    throw new IllegalStateException("Can't parse date pattern");
                }

            } else {
                throw new IllegalStateException("Incorrect format");
            }
        }

        public DatePattern(String compareLevel) {
            this.compareLevel = compareLevel;
        }

        public boolean isBefore(LocalDate date) {
            switch (compareLevel) {
                case CL_DAY:
                    return day<date.getDayOfMonth();
                case CL_MONTH:
                    return month<date.getMonthValue();
                case CL_DAY_MONTH:
                    return month<date.getMonthValue() || (month==date.getMonthValue() && day<date.getDayOfMonth());
                case CL_DATE:
                    LocalDate d = LocalDate.of(day, month, year);
                    return d.isBefore(date);
            }
            return false;
        }

        public boolean isAfter(LocalDate date) {
            switch (compareLevel) {
                case CL_DAY:
                    return day>date.getDayOfMonth();
                case CL_MONTH:
                    return month>date.getMonthValue();
                case CL_DAY_MONTH:
                    return month>date.getMonthValue() || (month==date.getMonthValue() && day>date.getDayOfMonth());
                case CL_DATE:
                    LocalDate d = LocalDate.of(day, month, year);
                    return d.isAfter(date);
            }
            return false;
        }

        public boolean isMatch(LocalDate date) {
            switch (compareLevel) {
                case CL_DAY:
                    return day==date.getDayOfMonth();
                case CL_MONTH:
                    return month==date.getMonthValue();
                case CL_DAY_MONTH:
                    return month==date.getMonthValue() && day==date.getDayOfMonth();
                case CL_DATE:
                    LocalDate d = LocalDate.of(day, month, year);
                    return d.isEqual(date);
            }
            return false;
        }

        @Override
        public int compareTo(DatePattern o) {
            switch (compareLevel) {
                case CL_DAY:
                    if (day<o.day) return -1;
                    if (day>o.day) return 1;
                case CL_MONTH:
                    if (month<o.month) return -1;
                    if (month>o.month) return 1;
                case CL_DAY_MONTH:
                    if (month<o.month || (month==o.month && day<o.day)) return -1;
                    if (month>o.month || (month==o.month && day>o.day)) return 1;
                case CL_DATE:
                    LocalDate cd = LocalDate.of(day, month, year);
                    LocalDate td = LocalDate.of(o.day, o.month, o.year);
                    if (cd.isBefore(td)) return -1;
                    if (cd.isAfter(td)) return 1;
            }
            return 0;
        }
    }
}
