package com.synapse.ms.ithings.core.common.events;

public interface BridgeListener {

    void onBridgeActivateStatusChanged(BridgeActivateStatusEvent event);
}
