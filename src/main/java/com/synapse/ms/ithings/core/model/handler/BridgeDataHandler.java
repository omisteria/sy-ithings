package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.common.events.BridgeActivateStatusEvent;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.bridge.Bridge;

public class BridgeDataHandler extends AbstractBridgeDataHandler {

    public BridgeDataHandler(Bridge bridge) {
        super(bridge);
    }

    @Override
    public void initialize() {
        bridge.getActivateStatus().addListener(bridgeEvent ->
                onBridgeActivateStatusChanged(
                        new BridgeActivateStatusEvent(bridge,
                                (ActivateStatus)bridgeEvent.getPreviousValue(),
                                (ActivateStatus)bridgeEvent.getValue()))
        );
    }

    @Override
    public void sendRequest(Command command) throws Exception {

    }

    @Override
    public void onReceivedResponse(byte[] responseData) {

    }

    @Override
    public void onBridgeActivateStatusChanged(BridgeActivateStatusEvent event) {
        if (ActivateStatus.ONLINE.equals(event.getStatus())) {
            // TODO: double initialization after restoring status of bridge ????
            new Thread(() -> {
                Bridge bridge = event.getSource();
                bridge.getRepresenters().forEach(r -> {
                    DataHandler dh = r.getDataHandler();
                    if (dh != null) {
                        dh.initialize();
                    }
                });
            }).start();
        }
        SystemEngineContext.getBus().post(event).now();
    }
}
