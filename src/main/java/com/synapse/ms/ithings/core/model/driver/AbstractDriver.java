package com.synapse.ms.ithings.core.model.driver;

import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.common.datafetch.AcquisitionStrategy;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.config.DriverDescription;

public class AbstractDriver implements Driver {

    private Bridge bridge;
    private DriverDescription description;
    private DataHandler dataHandler;
    private AcquisitionStrategy strategy;

    public AbstractDriver(Bridge bridge, DriverDescription description) {
        this.bridge = bridge;
        this.description = description;
    }

    public void setDataHandler(DataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }

    public DataHandler getDataHandler() {
        return dataHandler;
    }

    public void activate() throws Exception {

        activateAcquisitionStrategy();
    }

    private void activateAcquisitionStrategy() throws Exception {
        /*if (description.getPollingPeriod() != null) {
            strategy = new PollingAcquisitionStrategy(description.getPollingPeriod());
            strategy.activate(dataHandler);
        }*/
    }

    public void dispose() {

    }
}
