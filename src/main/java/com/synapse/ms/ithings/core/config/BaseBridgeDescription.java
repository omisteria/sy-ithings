package com.synapse.ms.ithings.core.config;

import java.util.Arrays;
import java.util.Map;

@Deprecated
public class BaseBridgeDescription implements BridgeDescription {

    private String gid;

    private String className;

    private Long pollingPeriod;

    private BaseRepresenterDescription[] representers;

    private Map<String, String> params;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Long getPollingPeriod() {
        return pollingPeriod;
    }

    public void setPollingPeriod(Long pollingPeriod) {
        this.pollingPeriod = pollingPeriod;
    }

    public BaseRepresenterDescription[] getRepresenters() {
        return representers;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public BaseRepresenterDescription findRepresenterDescription(String guid) {
        return Arrays.asList(representers).stream()
                .filter(r->r.getGid().equals(guid)).findFirst().orElse(null);
    }
}
