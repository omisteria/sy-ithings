package com.synapse.ms.ithings.core.config;

import java.util.List;

public class DriverDescription {

    private String observe;

    private String className;



    private List<BaseChannelDescription> channels;

    public String getObserve() {
        return observe;
    }

    public void setObserve(String observe) {
        this.observe = observe;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


    public List<BaseChannelDescription> getChannels() {
        return channels;
    }

    public void setChannels(List<BaseChannelDescription> channels) {
        this.channels = channels;
    }
}
