package com.synapse.ms.ithings.core.rules;


public abstract class BaseEntityBuilder {

    private String name;

    public BaseEntityBuilder withName(String name) {
        this.name = name;
        return this;
    }
}
