package com.synapse.ms.ithings.core.model.bridge;

import com.synapse.ms.ithings.core.common.Identifiable;
import com.synapse.ms.ithings.core.common.events.BridgeListener;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.Configurable;
import com.synapse.ms.ithings.core.model.Observable;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;

import java.util.List;

public interface Bridge extends Identifiable<String>, Configurable {

    void configure();

    void activate() throws Exception;

    void dispose() throws Exception;

    Observable<ActivateStatus> getActivateStatus();

    void addRepresenter(Representer representer);

    void removeRepresenter(Representer representer);

    List<Representer> getRepresenters();

    void initAcquisitions();

    // ActivateStatus getStatus();

    BridgeListener addListener(BridgeListener listener);

    RepresenterManager getRepresenterManager();
}
