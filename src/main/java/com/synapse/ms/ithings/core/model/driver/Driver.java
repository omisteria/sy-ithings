package com.synapse.ms.ithings.core.model.driver;

import com.synapse.ms.ithings.core.model.handler.DataHandler;

public interface Driver {

    void activate() throws Exception;

    DataHandler getDataHandler();
}
