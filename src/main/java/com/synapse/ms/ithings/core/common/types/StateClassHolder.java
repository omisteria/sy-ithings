package com.synapse.ms.ithings.core.common.types;

import java.util.HashMap;
import java.util.Map;

public enum StateClassHolder {

    BOOLEAN("boolean", BooleanType.class),
    DECIMAL("decimal", DecimalType.class),
    JSON("json", FlatJsonType.class),
    PERCENT("percent", PercentType.class),
    RAW("raw", RawType.class),
    STRINNG("string", StringType.class),
    ONOFF("onoff", OnOffType.class),
    UNDEFINED("undef", UnDefType.class);

    private static final Map<String, StateClassHolder> map;
    static {
        map = new HashMap<>();
        for (StateClassHolder v : StateClassHolder.values()) {
            map.put(v.shortName.toLowerCase(), v);
        }
    }

    private String shortName;
    private Class stateClass;

    StateClassHolder(String shortName, Class stateClass) {
        this.shortName = shortName;
        this.stateClass = stateClass;
    }

    public Class getStateClass() {
        return stateClass;
    }

    public static StateClassHolder findByShortName(String name) {
        return map.get(name.toLowerCase());
    }
}
