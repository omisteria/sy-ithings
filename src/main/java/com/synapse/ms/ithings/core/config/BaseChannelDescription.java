package com.synapse.ms.ithings.core.config;

public class BaseChannelDescription implements ChannelDescription {

    private String gid;
    private String type;

    private Long pollingPeriod;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getPollingPeriod() {
        return pollingPeriod;
    }

    public void setPollingPeriod(Long pollingPeriod) {
        this.pollingPeriod = pollingPeriod;
    }

}
