package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.RulesModule;

public abstract class AbstractModule implements RulesModule {

    private RulesEngine rulesEngine;

    public AbstractModule(RulesEngine rulesManager) {
        this.rulesEngine = rulesManager;
    }

    public RulesEngine getRulesEngine() {
        return rulesEngine;
    }
}
