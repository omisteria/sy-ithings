package com.synapse.ms.ithings.core.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

public class Observable<V> {

    private final Logger logger = LoggerFactory.getLogger(Observable.class);

    private V value;
    private List<ObservableListener> listeners = new ArrayList<>();

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    public Observable(V initValue) {
        this.value = initValue;
    }

    public V getValue() {
        return value;
    }

    public void update(V newValue) {
        synchronized (this) {
            if (!this.value.equals(newValue)) {
                logger.trace("changed to: {}", newValue);
                V oldValue = this.value;
                this.value = newValue;
                notifyListeners(listener ->
                        listener.onChanged(new ObservableEvent(this, oldValue, newValue)));
            }
        }
    }

    public void pulse(V newValue) {
        synchronized (this) {
            if (!this.value.equals(newValue)) {
                notifyListeners(listener ->
                        listener.onChanged(new ObservableEvent(this, this.value, newValue)));
            }
        }
    }

    public ObservableListener addListener(ObservableListener listener) {
        this.writeLock.lock();
        try {
            listeners.add(listener);
            return listener;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    public void removeListener(ObservableListener listener) {
        this.writeLock.lock();
        try {
            listeners.remove(listener);
        }
        finally {
            this.writeLock.unlock();
        }
    }

    public void notifyListeners(Consumer<? super ObservableListener> algorithm) {
        this.readLock.lock();
        try {
            //this.listeners.forEach(listener -> new Thread(() -> algorithm.accept(listener)).start());
            this.listeners.forEach(algorithm::accept);
        }
        finally {
            this.readLock.unlock();
        }
    }
}
