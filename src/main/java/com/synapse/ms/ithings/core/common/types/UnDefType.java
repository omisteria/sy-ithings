package com.synapse.ms.ithings.core.common.types;

public enum UnDefType implements State, Command {
    UNDEF,
    NULL;

    @Override
    public <T extends State> T as(Class<T> target) {
        return null;
    }

    @Override
    public UnDefType clone(State fromState) {
        return (UnDefType)fromState;
    }
}
