package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.common.events.RepresenterActivateStatusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.ObservableEvent;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;

public class RepresenterDataHandler extends AbstractRepresenterDataHandler {

    public RepresenterDataHandler(Bridge bridge, Representer representer) {
        super(bridge, representer);
    }

    @Override
    public void initialize() {
        representer.getActivateStatus().addListener(bridgeEvent ->
                onRepresenterActivateStatusChanged(
                        new RepresenterActivateStatusEvent(representer,
                                (ActivateStatus)bridgeEvent.getPreviousValue(),
                                (ActivateStatus)bridgeEvent.getValue()))
        );
        representer.getChannels().forEach(this::initializeChannel);
        representer.getActivateStatus().update(ActivateStatus.ONLINE);
    }

    private void initializeChannel(Channel channel) {
        channel.getState().addListener(event -> onChannelStateChanged(event, channel));

        channel.getActivateStatus().update(ActivateStatus.ONLINE);

        try {
            if (!UnDefType.UNDEF.equals(channel.getInitState())) {
                logger.info("Initialize channel {} to {}", channel.getGid(), channel.getInitState().toString());
                sendRequest((Command) channel.getInitState(), channel.getGid());
            }
        } catch (Exception e) {
            logger.error("Can't send init value", e);
        }
    }

    @Override
    public void sendRequest(Command command) throws Exception {

    }

    @Override
    public void onReceivedResponse(byte[] responseData) {

    }

    @Override
    public void onReceivedResponse(byte[] responseData, String channelId) {
        State rawState = new RawType(responseData);

        Channel channel = representer.getChannel(channelId);
        if (channel != null) {
            State newState = rawState.as(channel.getStateType());
            if (newState != null) {
                channel.getState().update(newState);
            }
        }
    }

    @Override
    public void sendRequest(Command command, String channelId) {

    }

    @Override
    public void onChannelStateChanged(ObservableEvent<State, Channel> event, Channel channel) {
        // logger.info(">>> Publish StateEventBusEvent: ch="+channel+", new state="+event.getValue());
        SystemEngineContext.getBus().post(new StateEventBusEvent(channel, event.getPreviousValue(), event.getValue())).now();
    }

    @Override
    public void onRepresenterActivateStatusChanged(RepresenterActivateStatusEvent event) {
        SystemEngineContext.getBus().post(event).now();
    }
}
