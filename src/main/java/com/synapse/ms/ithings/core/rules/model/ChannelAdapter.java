package com.synapse.ms.ithings.core.rules.model;

import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;

public interface ChannelAdapter {

    RulesEngineTrigger turned(String toState);
    RulesEngineTrigger turned(String fromState, String toState);
    RulesEngineTrigger greater(Double val);
    RulesEngineTrigger less(Double val);

    void toggle();
    void command(String command);
}
