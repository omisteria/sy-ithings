package com.synapse.ms.ithings.core.rules;

public interface RulesModule {

    RulesEngine getRulesEngine();
}
