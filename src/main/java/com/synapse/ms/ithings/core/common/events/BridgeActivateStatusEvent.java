package com.synapse.ms.ithings.core.common.events;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.Bridge;

import java.io.Serializable;

public class BridgeActivateStatusEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    protected transient Bridge source;
    protected transient ActivateStatus oldStatus;
    protected transient ActivateStatus status;

    public BridgeActivateStatusEvent(Bridge source, ActivateStatus oldStatus, ActivateStatus newStatus) {
        this.source = source;
        this.oldStatus = oldStatus;
        this.status = newStatus;
    }

    public Bridge getSource() {
        return source;
    }

    public ActivateStatus getStatus() {
        return status;
    }

    public ActivateStatus getOldStatus() {
        return oldStatus;
    }

    @Override
    public String toString() {
        return "BridgeActivateStatusEvent [bridge="+source.getGid()+", new status="+status+"]";
    }
}
