package com.synapse.ms.ithings.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.synapse.ms.ithings.core.utils.ConfigContent;
import spark.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class PlainConfiguration {

    private final Map<String, Object> properties;

    public PlainConfiguration() {
        this(null);
    }

    public PlainConfiguration(Map<String, Object> properties) {
        this.properties = properties == null ? new HashMap<>() : properties;
    }

    public boolean containsKey(String key) {
        synchronized (this) {
            return properties.containsKey(key);
        }
    }

    public Object get(String key) {
        synchronized (this) {
            return properties.get(key);
        }
    }

    public Object put(String key, Object value) {
        synchronized (this) {
            return properties.put(key, value);
        }
    }

    public Object remove(String key) {
        synchronized (this) {
            return properties.remove(key);
        }
    }

    public Set<String> keySet() {
        synchronized (this) {
            return Collections.unmodifiableSet(new HashSet<>(properties.keySet()));
        }
    }

    public Collection<Object> values() {
        synchronized (this) {
            return Collections.unmodifiableCollection(new ArrayList<>(properties.values()));
        }
    }

    public Map<String, Object> properties() {
        synchronized (this) {
            return Collections.unmodifiableMap(properties);
        }
    }

    @Override
    public int hashCode() {
        synchronized (this) {
            return properties.hashCode();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PlainConfiguration)) {
            return false;
        }
        return this.hashCode() == obj.hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Configuration[");
        boolean first = true;
        for (final Map.Entry<String, Object> prop : properties.entrySet()) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            Object value = prop.getValue();
            sb.append(String.format("{key=%s; type=%s; value=%s}", prop.getKey(),
                    value != null ? value.getClass().getSimpleName() : "?", value));
        }
        sb.append("]");
        return sb.toString();
    }

    public static PlainConfiguration loadFromFile(String fileName) throws ConfigurationException {
        ConfigContent configContent = new ConfigContent();
        try {
            return new PlainConfiguration(new Gson().fromJson(configContent.loadContent(fileName), Map.class));
        } catch (Exception e) {
            throw new ConfigurationException("Can't read configuration", e);
        }
    }
}
