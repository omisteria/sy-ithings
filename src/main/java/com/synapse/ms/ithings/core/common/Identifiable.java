package com.synapse.ms.ithings.core.common;

public interface Identifiable<T> {

    T getGid();
}
