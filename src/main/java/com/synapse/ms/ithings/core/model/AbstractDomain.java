package com.synapse.ms.ithings.core.model;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.build.BaseFactory;
import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.config.PlainConfiguration;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractDomain implements Domain {

    protected Map<String, String> drivers = new HashMap<>();

    private PlainConfiguration configuration;

    public AbstractDomain() {
        //drivers.put("basic", "com.synapse.ms.ithings.binding.sonoff.SonoffBasic");
    }

    public RepresenterFactory getRepresenterFactory() {
        return new BaseFactory(this);
    }

    @Override
    public String resolveRepresenterClassName(String name) {
        return drivers.get(name);
    }

    @Override
    public PlainConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void setConfiguration(PlainConfiguration configuration) {
        this.configuration = configuration;
    }
}
