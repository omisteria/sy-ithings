package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.common.events.ChannelListener;
import com.synapse.ms.ithings.core.common.events.RepresenterListener;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.represent.Representer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRepresenterDataHandler implements DataHandler<byte[], byte[]>, RepresenterListener, ChannelListener {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected Bridge bridge;
    protected Representer representer;

    public AbstractRepresenterDataHandler(Bridge bridge, Representer representer) {
        this.bridge = bridge;
        this.representer = representer;
    }

    abstract void onReceivedResponse(byte[] responseData, String channelId);

    abstract void sendRequest(Command command, String channelId);
}
