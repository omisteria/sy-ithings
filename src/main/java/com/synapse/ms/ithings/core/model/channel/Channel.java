package com.synapse.ms.ithings.core.model.channel;

import com.synapse.ms.ithings.core.common.Identifiable;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.Configurable;
import com.synapse.ms.ithings.core.model.Observable;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;

public interface Channel extends Identifiable<String>, Configurable {

    void setDataHandler(RepresenterDataHandler handler);

    RepresenterDataHandler getDataHandler();

    void post(Command command) throws Exception;

    Observable<ActivateStatus> getActivateStatus();

    Observable<State> getState();
    State getInitState();
    Class getStateType();

    ChannelType getType();

    String getSelector();
    void setSelector(String selector);

    boolean isInverseState();
    void setInverseState(boolean value);
}
