package com.synapse.ms.ithings.core.common.types;

public enum OnOffType implements State, Command {
    ON,
    OFF;

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == DecimalType.class) {
            return target.cast(this == ON ? new DecimalType(1) : DecimalType.ZERO);
        } else if (target == RawType.class) {
            return target.cast(this == ON ? new RawType("1".getBytes()) : new RawType("0".getBytes()) );
        } else if (target == PercentType.class) {
            return target.cast(this == ON ? PercentType.HUNDRED : PercentType.ZERO);
        } else if (target == BooleanType.class) {
            return target.cast(this == ON ? BooleanType.TRUE : BooleanType.FALSE);
        } else {
            return null;
        }
    }

    @Override
    public OnOffType clone(State fromState) {
        return (OnOffType)fromState;
    }

    public OnOffType inverse() {
        return this == ON ? OFF : ON;
    }
}
