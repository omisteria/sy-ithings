package com.synapse.ms.ithings.core.common;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class SimpleRegistry<K, T extends Identifiable<K>> {

    private Map<K, T> registry = new ConcurrentHashMap();

    public void register(T unit) {
        if (unit == null) {
            return;
        }
        if (registry.containsKey(unit.getGid()))
            return;
        registry.putIfAbsent(unit.getGid(), unit);
    }

    public T get(K key) {
        if (key == null)
            return null;
        return registry.get(key);
    }

    public boolean contains(K key) {
        if (key == null)
            return false;
        return registry.containsKey(key);
    }

    public T remove(K key) {
        if (!registry.containsKey(key))
            return null;

        return registry.remove(key);
    }

    public T remove(T unit) {
        if (unit == null) {
            return null;
        }
        if (!registry.containsKey(unit.getGid()))
            return null;

        return registry.remove(unit.getGid());
    }

    public int size() {
        return registry.size();
    }

    public void forEach(Consumer<T> consumer) {
        registry.forEach((k, v)->consumer.accept(v));
    }

    public Stream<T> stream() {
        return registry.values().stream();
    }

    public Collection<T> values() {
        return registry.values();
    }
}
