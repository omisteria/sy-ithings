package com.synapse.ms.ithings.core.common.events;

public abstract class BridgeListenerAdapter implements BridgeListener {

    public void onBridgeActivateStatusChanged(BridgeActivateStatusEvent event) {}
}
