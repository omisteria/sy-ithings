package com.synapse.ms.ithings.core.model.represent;

import com.synapse.ms.ithings.core.common.Identifiable;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.Configurable;
import com.synapse.ms.ithings.core.model.Observable;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;

import java.util.List;

public interface Representer extends Identifiable<String>, Configurable {

    void configure();

    void setChannels(List<Channel> channels);
    List<Channel> getChannels();
    void addChannel(Channel channel);

    Channel getChannel(String channelId);

    void setDataHandler(RepresenterDataHandler handler);
    RepresenterDataHandler getDataHandler();

    PlainConfiguration getConfiguration();

    Observable<ActivateStatus> getActivateStatus();

    boolean isEnabled();

    void activate() throws Exception;

    void post(String channelId, Command command) throws Exception;

    List<PredefinedChannel> getPredefinedChannels();

    Bridge getBridge();
}
