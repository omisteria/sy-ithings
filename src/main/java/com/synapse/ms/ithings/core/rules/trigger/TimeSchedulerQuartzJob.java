package com.synapse.ms.ithings.core.rules.trigger;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class TimeSchedulerQuartzJob implements Job {
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
        if (data!= null && data.containsKey("trigger")) {
            RulesEngineTrigger trigger = (RulesEngineTrigger) data.get("trigger");
            trigger.fire();
        }
    }
}
