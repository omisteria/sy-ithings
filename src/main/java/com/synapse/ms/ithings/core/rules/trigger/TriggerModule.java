package com.synapse.ms.ithings.core.rules.trigger;

import com.synapse.ms.ithings.core.rules.BaseRulesEngine;

public class TriggerModule {

    private BaseRulesEngine rulesManager;

    public TriggerModule(BaseRulesEngine rulesManager) {
        this.rulesManager = rulesManager;
    }

    /*public RulesEngineTrigger any(NativeArguments arguments) {
        if (arguments.isArguments()) {

            RulesEngineTrigger[] triggers = Stream.of(arguments.getArray().asObjectArray())
                    .map(m-> (RulesEngineTrigger)m)
                    .filter(distinctByKey(RulesEngineTrigger::getGid))
                    .toArray(size -> new RulesEngineTrigger[size]);

            String triggerId = "trigger."; //TODO generate unique ID
            return rulesManager.getOrCreateTrigger(triggerId, s -> new AgrAnyRulesEngineTrigger(s, triggers));
        }

        return null; //TODO return empty trigger
    }*/

    /*public RulesEngineTrigger any(RulesEngineTrigger ...arguments) {
        RulesEngineTrigger[] triggers = Arrays.stream(arguments)
                .filter(distinctByKey(RulesEngineTrigger::getGid))
                .toArray(size -> new RulesEngineTrigger[size]);

        String triggerId = "trigger."; //TODO generate unique ID
        return rulesManager.getOrCreateTrigger(triggerId, s -> new AgrAnyRulesEngineTrigger(s, triggers));
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }*/
}
