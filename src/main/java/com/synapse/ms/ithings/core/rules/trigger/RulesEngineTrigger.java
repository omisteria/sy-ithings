package com.synapse.ms.ithings.core.rules.trigger;

import com.synapse.ms.ithings.core.common.Identifiable;

import java.util.List;

public interface RulesEngineTrigger extends Identifiable<String> {

    void setActivator(Runnable activator);

    void activate();

    // List<Rule> getRules();
    List<RulesEngineTriggerListener> getListeners();

    void fire();
}
