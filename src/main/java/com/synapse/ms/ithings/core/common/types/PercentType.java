package com.synapse.ms.ithings.core.common.types;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PercentType extends DecimalType {

    private static final long serialVersionUID = -9066279845951780879L;

    public static final PercentType ZERO = new PercentType(0);
    public static final PercentType HUNDRED = new PercentType(100);

    public PercentType() {
        this(0);
    }

    public PercentType(PercentType from) {
        super(from.value);
    }

    public PercentType(int value) {
        super(value);
        validateValue(this.value);
    }

    public PercentType(String value) {
        super(value);
        validateValue(this.value);
    }

    public PercentType(BigDecimal value) {
        super(value);
        validateValue(this.value);
    }

    private void validateValue(BigDecimal value) {
        if (BigDecimal.ZERO.compareTo(value) > 0 || BigDecimal.valueOf(100).compareTo(value) < 0) {
            throw new IllegalArgumentException("Value must be between 0 and 100");
        }
    }

    public static PercentType valueOf(String value) {
        return new PercentType(value);
    }

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == OnOffType.class) {
            return target.cast(equals(ZERO) ? OnOffType.OFF : OnOffType.ON);
        } else if (target == StringType.class) {
            return target.cast(value.toString());
        } else if (target == DecimalType.class) {
            return target.cast(new DecimalType(toBigDecimal().divide(BigDecimal.valueOf(100), 8, RoundingMode.UP)));
        } else if (target == BooleanType.class) {
            return target.cast(equals(HUNDRED) ? BooleanType.TRUE : BooleanType.FALSE);
        } else {
            return null;
        }
    }

    @Override
    public PercentType clone(State fromState) {
        return new PercentType((PercentType)fromState);
    }

    @Override
    public String toString() {
        return "PercentType:" + value;
    }
}
