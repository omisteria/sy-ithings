package com.synapse.ms.ithings.core.rules.trigger;

import org.quartz.*;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;

public class CronRulesEngineTrigger extends AbstractRuresEngineTrigger {

    private static final String SCHEDULER_GROUP = "CronTriggers";

    protected TriggerKey triggerKey;
    protected JobKey jobKey;
    protected Trigger quartzTrigger;
    protected JobDetail quartzJob;

    public CronRulesEngineTrigger(String expression) {
        super(expression);

        triggerKey = genTriggerKey();
        quartzTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(cronSchedule(expression))
                .build();

        jobKey = genJobKey();

        JobDataMap map = new JobDataMap();

        quartzJob = newJob(TimeSchedulerQuartzJob.class)
                .withIdentity(jobKey)
                .setJobData(map)
                .build();
    }

    public Trigger getQuartzTrigger() {
        return quartzTrigger;
    }

    public JobDetail getQuartzJob() {
        return quartzJob;
    }

    protected JobKey genJobKey() {
        String jobIdentity = "TimeJob_" + System.currentTimeMillis() + "_" + hashCode();
        return new JobKey(jobIdentity, SCHEDULER_GROUP);
    }

    protected TriggerKey genTriggerKey() {
        String triggerIdentity = "TimeTrigger_" + System.currentTimeMillis() + "_" + hashCode();
        return new TriggerKey(triggerIdentity, SCHEDULER_GROUP);
    }
}
