package com.synapse.ms.ithings.core.config;

import com.synapse.ms.ithings.core.common.Identifiable;

@Deprecated
public interface ChannelDescription extends Identifiable<String> {

}
