package com.synapse.ms.ithings.core.config;

public interface ConfKey {

    String domain           = "domain";
    String id               = "id";
    String gid              = "gid";
    String driver           = "driver";
    String className        = "className";
    String representers     = "representers";
    String pollingPeriod    = "pollingPeriod";
    String channels         = "channels";
    String type             = "type";
    String stateType        = "stateType";
    String label            = "label";
    String initState        = "initState";
    String selector         = "selector";
    String inverse          = "inverse";

    // MQTT
    String mqttHost         = "mqttHost";
    String mqttPort         = "mqttPort";
    String stateTopic       = "stateTopic";
    String commandTopic     = "commandTopic";
}
