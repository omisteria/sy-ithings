package com.synapse.ms.ithings.core.config;

import com.synapse.ms.ithings.core.common.Identifiable;

public interface BridgeDescription extends Identifiable<String> {

}
