package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.BaseRulesEngine;
import com.synapse.ms.ithings.core.rules.RulesModule;
import com.synapse.ms.ithings.core.rules.trigger.CronRulesEngineTrigger;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;
import com.synapse.ms.ithings.core.rules.trigger.TimeRulesEngineTrigger;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class SchedulerProvider {

    private RulesModule rulesModule;
    private Scheduler scheduler;
    private Map<String, TimerScheduler> namedTimerSchedulers = new ConcurrentHashMap<>();

    public SchedulerProvider(RulesModule rulesModule) {
        this.rulesModule = rulesModule;

        try {

            Properties props = new Properties();
            props.setProperty("org.quartz.threadPool.threadCount","50");
            props.setProperty("org.quartz.scheduler.skipUpdateCheck","true");
            props.setProperty("org.quartz.scheduler.makeSchedulerThreadDaemon","true");

            scheduler = new StdSchedulerFactory(props).getScheduler();
            scheduler.start();

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public TimerScheduler create(String name) {
        return new TimerScheduler().withName(name);
    }
    public TimerScheduler create() {
        return new TimerScheduler();
    }

    public RulesEngineTrigger cron(String expression) {
        return rulesModule.getRulesEngine().getOrCreateTrigger(expression, s -> {
            RulesEngineTrigger newTrigger = new CronRulesEngineTrigger(s);
            newTrigger.setActivator(() -> {
                CronRulesEngineTrigger tr = (CronRulesEngineTrigger)newTrigger;
                JobDataMap map = tr.getQuartzJob().getJobDataMap();
                map.put("trigger", tr);

                try {
                    scheduler.scheduleJob(tr.getQuartzJob(), tr.getQuartzTrigger());
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            });
            return newTrigger;
        });
    }

    public RulesEngineTrigger at(String expression) {
        return rulesModule.getRulesEngine().getOrCreateTrigger(expression, s -> {
            RulesEngineTrigger newTrigger = new TimeRulesEngineTrigger(expression);
            newTrigger.setActivator(() -> {
                TimeRulesEngineTrigger tr = (TimeRulesEngineTrigger) newTrigger;
                JobDataMap map = tr.getQuartzJob().getJobDataMap();
                map.put("trigger", tr);

                try {
                    scheduler.scheduleJob(tr.getQuartzJob(), tr.getQuartzTrigger());
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            });
            return newTrigger;
        });
    }

    public class TimerScheduler {

        private String name;
        private Function<Void, Void> handler;
        private long delay;
        private long interval;
        private Timer timer;
        private TimerTask timerTask;

        public TimerScheduler withName(String name) {
            this.name = name;
            return this;
        }

        public TimerScheduler delay(long delay) {
            this.delay = delay;
            return this;
        }
        public TimerScheduler interval(long interval) {
            this.interval = interval;
            return this;
        }

        public TimerScheduler exec(Function<Void, Void> handler) {
            this.handler = handler;
            startTimer();

            return this;
        }

        private void startTimer() {
            if (delay > 0) {
                setTimerRequest(name, handler, delay, 0);
            } else if (interval>0) {
                setTimerRequest(name, handler, 0, interval);
            }
        }

        private synchronized void setTimerRequest(String name, Function<Void, Void> handler, long delay, long interval) {

            String timerName = "Timer_" + (name != null ? name : UUID.randomUUID().toString());

            if (name != null) {
                if (namedTimerSchedulers.containsKey(name)) {
                    TimerScheduler scheduler = namedTimerSchedulers.get(name);
                    if (scheduler != null) {
                        scheduler.timerTask.cancel();
                        scheduler.timer.cancel();
                        rulesModule.getRulesEngine().unregisterTimer(scheduler.timer);
                        namedTimerSchedulers.remove(name);
                    }
                }
            }

            timer = new Timer(timerName, true);
            rulesModule.getRulesEngine().registerTimer(timer);

            timerTask = new TimerTask() {
                @Override
                public void run() {
                    handler.apply(null);

                    rulesModule.getRulesEngine().unregisterTimer(timer);

                    // clear timer
                    if (name != null) {
                        namedTimerSchedulers.remove(name);
                    }
                }
            };

            if (interval > 0) {
                timer.schedule(timerTask, delay, interval);
            } else {
                timer.schedule(timerTask, delay);
            }

            if (name != null) {
                namedTimerSchedulers.put(name, this);
            }
        }
    }
}
