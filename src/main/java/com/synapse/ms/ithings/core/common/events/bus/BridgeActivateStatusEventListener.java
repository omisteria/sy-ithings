package com.synapse.ms.ithings.core.common.events.bus;

import com.synapse.ms.ithings.core.common.events.BridgeActivateStatusEvent;
import net.engio.mbassy.listener.Handler;
import net.engio.mbassy.listener.Listener;
import net.engio.mbassy.listener.References;

@Listener(references = References.Strong)
public class BridgeActivateStatusEventListener {

    @Handler
    public void handle(BridgeActivateStatusEvent event) {
        throw new IllegalStateException("Not Implemented");
    }
}
