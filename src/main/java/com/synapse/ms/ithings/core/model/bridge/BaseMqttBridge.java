package com.synapse.ms.ithings.core.model.bridge;

import com.synapse.ms.ithings.core.config.ConfKey;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.model.handler.MqttDataHandler;
import com.synapse.ms.ithings.io.transport.mqtt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;

public abstract class BaseMqttBridge extends AbstractBridge {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private BridgeMqttConnectionInstance connectionInstance;
    private MqttBrokerConnectionConfig mqttBrokerConnectionConfig;
    private MqttService mqttService;

    public BaseMqttBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    public void activateMqttBrokerConnection() {
        mqttService = new MqttService();

        connectionInstance = new BridgeMqttConnectionInstance();
        connectionInstance.setMqttService(mqttService);

        String host = (String)description.get(ConfKey.mqttHost);
        String port = (String)description.get(ConfKey.mqttPort);
        MqttBrokerConnectionConfig connectionConfig = getMqttBrokerConnectionConfig(host, port);

        getActivateStatus().update(ActivateStatus.INITIALIZING);

        connectionInstance.activate(connectionConfig, (state, error) -> {

            if (MqttConnectionState.DISCONNECTED.equals(state)) {
                getActivateStatus().update(ActivateStatus.OFFLINE);

            } else if (MqttConnectionState.CONNECTED.equals(state)) {
                getActivateStatus().update(ActivateStatus.ONLINE);
            }

            if (error != null) {
                logger.info(error.toString());
            }

        });
    }

    public MqttBrokerConnectionConfig getMqttBrokerConnectionConfig(String host, String port) {
        if (mqttBrokerConnectionConfig == null) {
            mqttBrokerConnectionConfig = new MqttBrokerConnectionConfig();
            mqttBrokerConnectionConfig.host = host;
            try {
                mqttBrokerConnectionConfig.port = Integer.parseInt((String) description.get(port));
            } catch (Exception e) {
                mqttBrokerConnectionConfig.port = 1883;
            }
            UUID uuid = UUID.randomUUID();
            mqttBrokerConnectionConfig.name = "bridge-" + getGid() + "-" + uuid.toString();
            mqttBrokerConnectionConfig.clientID = getGid() + "-" + uuid.toString();
        }
        return mqttBrokerConnectionConfig;
    }

    public void subscribe(MqttMessageSubscriber subscriber) throws MqttException {
        if (mqttBrokerConnectionConfig != null) {
            String brokerId = mqttBrokerConnectionConfig.getBrokerID();
            MqttBrokerConnection connection = mqttService.getBrokerConnection(brokerId);
            if (connection != null) {
                connection.addConsumer(subscriber);
            }
        }
    }

    public void subscribe(String topic, DataHandler hadler) throws MqttException {
        subscribe(new MqttMessageSubscriber() {
            @Override
            public void processMessage(String procTopic, byte[] payload) {
                hadler.onReceivedResponse(payload);
            }

            @Override
            public String getTopic() {
                return topic;
            }
        });
    }

    public void subscribe(String topic, MqttDataHandler hadler) throws MqttException {
        subscribe(new MqttMessageSubscriber() {
            @Override
            public void processMessage(String procTopic, byte[] payload) {
                hadler.onReceivedResponse(procTopic, payload);
            }

            @Override
            public String getTopic() {
                return topic;
            }
        });
    }

    public void publish(String topic, byte[] data) throws MqttException {
        if (mqttBrokerConnectionConfig != null) {
            String brokerId = mqttBrokerConnectionConfig.getBrokerID();
            MqttBrokerConnection connection = mqttService.getBrokerConnection(brokerId);
            if (connection != null) {
                connection.publish(topic, data, new MqttPublishCallback() {

                    @Override
                    public void onSuccess(MqttPublishResult result) {

                    }

                    @Override
                    public void onFailure(MqttPublishResult result, Throwable error) {
                        logger.error("MQTT publish problem: " + result.getTopic());
                    }
                });
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        if (connectionInstance != null) {
            connectionInstance.deactivate();
        }
    }
}
