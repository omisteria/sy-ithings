package com.synapse.ms.ithings.core.common.types;

public interface State {

    <T extends State> T as(Class<T> target);

    <T extends State> T clone(T fromState);
}
