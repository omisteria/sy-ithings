package com.synapse.ms.ithings.core.common.events;

import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.model.channel.Channel;

import java.beans.PropertyChangeListener;

public interface StateObserver extends PropertyChangeListener {

    void onChanged(Channel channel, State state);
}
