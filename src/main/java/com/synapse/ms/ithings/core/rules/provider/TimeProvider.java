package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.Filter;
import com.synapse.ms.ithings.core.rules.BaseRulesEngine;
import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.RulesModule;

import java.time.LocalTime;

public class TimeProvider {

    private RulesModule rulesModule;

    public TimeProvider(RulesModule rulesModule) {
        this.rulesModule = rulesModule;
    }

    public Filter greater(String stime) {
        return new Filter<Object>() {
            private LocalTime time = LocalTime.parse(stime);

            public boolean filtered(Object data) {
                LocalTime now = LocalTime.now();
                return now.isAfter(time);
            }
        };
    }

    public Filter less(String stime) {
        return new Filter<Object>() {
            private LocalTime time = LocalTime.parse(stime);

            public boolean filtered(Object data) {
                LocalTime now = LocalTime.now();
                return now.isBefore(time);
            }
        };
    }

    public Filter between(String from, String to) {
        return new Filter<Object>() {
            private LocalTime fromTime = LocalTime.parse(from);
            private LocalTime toTime = LocalTime.parse(to);

            public boolean filtered(Object data) {
                LocalTime now = LocalTime.now();
                if (fromTime.isAfter(toTime)) {
                    return !now.isBefore(fromTime) || !now.isAfter(toTime);
                } else {
                    return !now.isBefore(fromTime) && !now.isAfter(toTime);
                }
            }
        };
    }

    public class BetweenTimeFilter implements Filter<Object> {
        private LocalTime fromTime;
        private LocalTime toTime;

        public BetweenTimeFilter(String from, String to) {
            fromTime = LocalTime.parse(from);
            toTime = LocalTime.parse(to);
        }

        public boolean filtered(Object data) {
            LocalTime now = LocalTime.now();
            if (fromTime.isAfter(toTime)) {
                return !now.isBefore(fromTime) || !now.isAfter(toTime);
            } else {
                return !now.isBefore(fromTime) && !now.isAfter(toTime);
            }
        }
    }

}
