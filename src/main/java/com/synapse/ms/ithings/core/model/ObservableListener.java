package com.synapse.ms.ithings.core.model;

public interface ObservableListener {

    void onChanged(ObservableEvent event);
}
