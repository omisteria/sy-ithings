package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.model.ChannelAdapter;

import java.util.function.Function;

public class RepresenterModule extends AbstractModule {

    private RepresenterManager representerManager;

    public RepresenterModule(RulesEngine rulesEngine, RepresenterManager representerManager) {
        super(rulesEngine);
        this.representerManager = representerManager;
    }

    public Function<String, ChannelAdapter> getChannelProvider() {
        return channelName -> {
            return getChannel(channelName);
        };
    }

    /**
     * Return proxy object of Representer's channel
     * @param fullChannelName
     * @return
     */
    public ChannelAdapter ch(String fullChannelName) {
        return getChannel(fullChannelName);
    }

    private ChannelAdapter getChannel(String fullChannelName) {
        //representerManager.getRepresenterRegistry().get()
        // TODO find channel in registry
        // TODO warning into log if channel has not been found
        return new BaseChannelAdapter(fullChannelName, representerManager, getRulesEngine());
    }
}
