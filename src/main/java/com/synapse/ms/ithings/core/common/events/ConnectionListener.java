package com.synapse.ms.ithings.core.common.events;

public interface ConnectionListener {

    void onConnectionChanged(boolean connected);

    void onReceiveData(byte[] message);
}
