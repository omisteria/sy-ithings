package com.synapse.ms.ithings.core;

import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.common.Identifiable;
import com.synapse.ms.ithings.core.config.PlainConfiguration;

public interface Domain extends Identifiable<String> {

    RepresenterFactory getRepresenterFactory();

    String getBridgeClassName();

    String resolveRepresenterClassName(String name);

    PlainConfiguration getConfiguration();

    void setConfiguration(PlainConfiguration configuration);
}
