package com.synapse.ms.ithings.core.rules.model;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class DiscreteProcess {

    private final ScheduledExecutorService scheduler;
    private long stepDuration; //millisecs
    private long pulseDuration;
    private long pauseDuration;
    private long counter = 0;
    private Function<Void, Map<String, Integer>> configResolver;
    private boolean wayUp = false;

    public DiscreteProcess() {
        scheduler = Executors.newScheduledThreadPool(1);
    }

    public DiscreteProcess create(Function<Void, Map<String, Integer>> configResolver) {
        return create(1000L, configResolver);
    }

    public DiscreteProcess create(long stepDuration, Function<Void, Map<String, Integer>> configResolver) {
        DiscreteProcess proc = new DiscreteProcess();
        proc.stepDuration = stepDuration;
        proc.configResolver = configResolver;
        return proc;
    }

    public void exec(Function<Boolean, Void> runnable) {
        scheduler.scheduleAtFixedRate(() -> {

            try {
                calcCounterMax();

                if (wayUp) {
                    if (counter < pulseDuration-1) {
                        ++counter;
                    } else {
                        wayUp = !wayUp;
                        counter = 0;
                        if (pauseDuration>0) {
                            runnable.apply(wayUp);
                        }
                    }
                } else {
                    if (counter < pauseDuration-1) {
                        ++counter;
                    } else {
                        wayUp = !wayUp;
                        counter = 0;
                        if (pulseDuration>0) {
                            runnable.apply(wayUp);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }, 500, stepDuration, TimeUnit.MILLISECONDS);
    }

    private void calcCounterMax() {
        if (configResolver != null) {
            try {
                Map<String, Integer> conf = configResolver.apply(null);
                pulseDuration = conf.get("pulseDuration");
                pauseDuration = conf.get("pulsePeriod") - pulseDuration;
            } catch (Exception e) {
                throw new IllegalStateException("Can't convert to DiscretStepResolver", e);
            }
        }
    }
}
