package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.common.types.DecimalType;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.common.types.StringType;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.model.ChannelAdapter;
import com.synapse.ms.ithings.core.rules.trigger.AbstractRuresEngineTrigger;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;
import com.synapse.ms.ithings.ref.IRCode;

public class BaseChannelAdapter implements ChannelAdapter {

    private String id;
    private RulesEngine rulesEngine;
    private RepresenterManager representerManager;

    public BaseChannelAdapter(String id, RepresenterManager representerManager, RulesEngine rulesEngine) {
        this.id = id;
        this.rulesEngine = rulesEngine;
        this.representerManager = representerManager;
    }

    @Override
    public RulesEngineTrigger turned(String toState) {
        final String triggerId = id + ".state." + toState;
        RulesEngineTrigger trigger = rulesEngine.getOrCreateTrigger(triggerId, s -> {
            RulesEngineTrigger newTrigger = new AbstractRuresEngineTrigger(triggerId) {
            };
            newTrigger.setActivator(() -> {
                SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
                    @Override
                    public void handle(StateEventBusEvent event) {
                        Channel channel = event.getChannel();
                        State state = event.getState();
                        if (channel != null && channel.getGid().equals(id)) {
                            State expState = new StringType(toState).as(channel.getStateType());
                            if (expState.equals(state)) newTrigger.fire();
                        }
                    }
                });
            });
            return newTrigger;
        });
        return trigger;
    }

    @Override
    public RulesEngineTrigger turned(String fromState, String toState) {
        final String triggerId = id + ".state." + fromState + "." + toState;
        RulesEngineTrigger trigger = rulesEngine.getOrCreateTrigger(triggerId, s -> {
            RulesEngineTrigger newTrigger = new AbstractRuresEngineTrigger(triggerId) {
            };
            newTrigger.setActivator(() -> {
                SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
                    @Override
                    public void handle(StateEventBusEvent event) {
                        Channel channel = event.getChannel();
                        State oldState = event.getOldState();
                        State newState = event.getState();
                        if (channel != null && channel.getGid().equals(id)) {
                            State expStateFrom = new StringType(fromState).as(channel.getStateType());
                            State expStateTo = new StringType(toState).as(channel.getStateType());
                            if (expStateFrom.equals(oldState) && expStateTo.equals(newState)) newTrigger.fire();
                        }
                    }
                });
            });
            return newTrigger;
        });
        return trigger;
    }

    @Override
    public RulesEngineTrigger greater(Double val) {
        final String triggerId = id + ".state." + val;
        RulesEngineTrigger trigger = rulesEngine.getOrCreateTrigger(triggerId, s -> {
            RulesEngineTrigger newTrigger = new AbstractRuresEngineTrigger(triggerId) {
            };
            newTrigger.setActivator(() -> {
                SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
                    @Override
                    public void handle(StateEventBusEvent event) {
                        Channel channel = event.getChannel();
                        State state = event.getState();
                        if (channel != null && channel.getGid().equals(id)) {
                            if (state instanceof DecimalType && ((DecimalType)state).doubleValue() > val) newTrigger.fire();
                        }
                    }
                });
            });
            return newTrigger;
        });
        return trigger;
    }

    @Override
    public RulesEngineTrigger less(Double val) {
        final String triggerId = id + ".state." + val;
        RulesEngineTrigger trigger = rulesEngine.getOrCreateTrigger(triggerId, s -> {
            RulesEngineTrigger newTrigger = new AbstractRuresEngineTrigger(triggerId) {
            };
            newTrigger.setActivator(() -> {
                SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
                    @Override
                    public void handle(StateEventBusEvent event) {
                        Channel channel = event.getChannel();
                        State state = event.getState();
                        if (channel != null && channel.getGid().equals(id)) {
                            if (state instanceof DecimalType && ((DecimalType)state).doubleValue() < val) newTrigger.fire();
                        }
                    }
                });
            });
            return newTrigger;
        });
        return trigger;
    }

    // COMMANDS

    public void toggle() {
        Channel ch = representerManager.getChannelRegistry().get(id);
        if (ch != null) {

            try {
                System.out.println("Current =" + ch.getState().getValue());

                if (ch.getType().equals(ChannelType.RELAY)) {
                    if (ch.getState().getValue().equals(OnOffType.ON)) {
                        ch.post(OnOffType.OFF);
                    } else {
                        ch.post(OnOffType.ON);
                    }
                }

                if (ch.getType().equals(ChannelType.IR)) {
                    ch.post(StringType.valueOf(IRCode.Fireplace.powerToggle));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void post(String command) {
        Channel ch = representerManager.getChannelRegistry().get(id);
        if (ch != null) {
            try {
                ch.post(StringType.valueOf(command));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void command(String command) {
        Channel ch = representerManager.getChannelRegistry().get(id);
        if (ch != null) {
            try {
                StringType stType = StringType.valueOf(command);

                if (ch.getType().equals(ChannelType.RELAY)) {
                    ch.post(stType.as(OnOffType.class));
                } else if (ch.getType().equals(ChannelType.IR) || ch.getType().equals(ChannelType.IR_TRANSMITTER)) {
                    ch.post(stType);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
