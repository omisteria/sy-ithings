package com.synapse.ms.ithings.core.model;

import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import net.engio.mbassy.bus.MBassador;

public class SystemEngine {

    private final RepresenterManager representerManager;

    public SystemEngine() {
        representerManager = new RepresenterManager();
    }

    public MBassador getBus() {
        return SystemEngineContext.getBus();
    }

    public RepresenterManager getRepresenterManager() {
        return representerManager;
    }

    public void shutdown() {

    }
}
