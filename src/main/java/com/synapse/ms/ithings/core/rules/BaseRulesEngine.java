package com.synapse.ms.ithings.core.rules;

import com.synapse.ms.ithings.core.common.events.bus.SystemStartedBusEvent;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.impl.entity.RuleStarter;
import com.synapse.ms.ithings.core.rules.model.DiscreteProcess;
import com.synapse.ms.ithings.core.rules.provider.DateTimeModule;
import com.synapse.ms.ithings.core.rules.provider.FuncProvider;
import com.synapse.ms.ithings.core.rules.provider.OperationModule;
import com.synapse.ms.ithings.core.rules.provider.RepresenterModule;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;
import com.synapse.ms.ithings.core.rules.trigger.SystemTriggers;
import com.synapse.ms.ithings.core.rules.trigger.TriggerModule;

import java.util.List;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

public class BaseRulesEngine implements RulesEngine {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ConcurrentHashMap<String, Rule> rulesRegistry = new ConcurrentHashMap();
    private ConcurrentHashMap<String, RulesEngineTrigger> triggers = new ConcurrentHashMap();
    private List<Timer> timers = new CopyOnWriteArrayList<>();

    public BaseRulesEngine() {
    }
    
    public synchronized RulesEngineTrigger getOrCreateTrigger(String triggerKey, Function<String, RulesEngineTrigger> factoryFunc) {
        if (!triggers.containsKey(triggerKey)) {
            RulesEngineTrigger newTrigger = factoryFunc.apply(triggerKey);
            triggers.putIfAbsent(triggerKey, newTrigger);
        }
        return triggers.get(triggerKey);
    }

    public synchronized void registerRule(Rule rule) {
        rulesRegistry.putIfAbsent(rule.getName(), rule);
    }

    public synchronized void registerTimer(Timer timer) {
        timers.add(timer);
    }

    public synchronized void unregisterTimer(Timer timer) {
        if (timer != null) {
            timers.remove(timer);
        }
    }

    @Override
    public void start() {
        triggers.forEachValue(1, trigger -> {
            trigger.activate();
        });

        SystemEngineContext.getBus().post(new SystemStartedBusEvent()).now();
    }
    
    public void bindingEngineValues(ScriptLoadEngine sle, RepresenterManager representerManager) {
        sle.bind("$RE", this);
        sle.bind("$E", this);

        // Holders
        DateTimeModule dateTimeModule = new DateTimeModule(this);
        sle.bind("Time", dateTimeModule.getTimeProvider());
        sle.bind("Date", dateTimeModule.getDateProvider());
        sle.bind("Scheduler", dateTimeModule.getSchedulerProvider());
        //sle.bind("Timer", new TimeProvider(this));

        //sle.bind("Time", new TimeTriggers(this));
        sle.bind("Rule", new RuleStarter(this));

        //sle.bind("Scheduler", new TimeProvider(this));
        sle.bind("System", new SystemTriggers(this));

        OperationModule opModule = new OperationModule(this);
        sle.bind("Op", opModule);

        TriggerModule triggerModule = new TriggerModule(this);
        sle.bind("Trigger", triggerModule);
        sle.bind("$T", triggerModule);

        RepresenterModule representerModule = new RepresenterModule(this, representerManager);
        sle.bind("Gear", representerModule);
        sle.bind("ch", representerModule.getChannelProvider());

        sle.bind("Process", new DiscreteProcess());
        sle.bind("Func", new FuncProvider());
    }
}
