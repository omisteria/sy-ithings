package com.synapse.ms.ithings.core.rules.trigger;

import com.synapse.ms.ithings.core.rules.Rule;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractRuresEngineTrigger implements RulesEngineTrigger {

    private String id;
    private Runnable activator;
    // private List<Rule> rules = new LinkedList<>();
    private List<RulesEngineTriggerListener> listeners = new LinkedList<>();
    private AtomicBoolean activated = new AtomicBoolean(false);

    public AbstractRuresEngineTrigger(String id) {
        this.id = id;
    }

    @Override
    public String getGid() {
        return id;
    }

    @Override
    public void setActivator(Runnable activator) {
        this.activator = activator;
    }

    public void activate() {
        if (activator!=null && !activated.getAndSet(true)) {
            activator.run();
        }
    }

    /*public List<Rule> getRules() {
        return rules;
    }*/
    public List<RulesEngineTriggerListener> getListeners() {
        return listeners;
    }

    public void fire() {
        listeners.forEach(listener -> listener.execute(this));
    }
}
