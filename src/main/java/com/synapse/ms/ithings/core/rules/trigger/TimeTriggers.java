package com.synapse.ms.ithings.core.rules.trigger;

import com.synapse.ms.ithings.core.rules.BaseRulesEngine;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Properties;

public class TimeTriggers {

    private BaseRulesEngine rulesManager;
    private Scheduler scheduler;


    public TimeTriggers(BaseRulesEngine rulesManager) {
        this.rulesManager = rulesManager;

        try {

            Properties props = new Properties();
            props.setProperty("org.quartz.threadPool.threadCount","50");
            props.setProperty("org.quartz.scheduler.skipUpdateCheck","true");
            props.setProperty("org.quartz.scheduler.makeSchedulerThreadDaemon","true");

            scheduler = new StdSchedulerFactory(props).getScheduler();
            scheduler.start();

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public RulesEngineTrigger at() {
        return null;
    }

    public RulesEngineTrigger cron(String expression) {
        RulesEngineTrigger trigger = rulesManager.getOrCreateTrigger(expression, s -> {
            RulesEngineTrigger newTrigger = new CronRulesEngineTrigger(s);
            newTrigger.setActivator(() -> {
                CronRulesEngineTrigger tr = (CronRulesEngineTrigger)newTrigger;
                JobDataMap map = tr.getQuartzJob().getJobDataMap();
                //map.put("handlers", tr.getHandlers());
                map.put("trigger", tr);

                try {
                    scheduler.scheduleJob(tr.getQuartzJob(), tr.getQuartzTrigger());
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            });
            return newTrigger;
        });

        return trigger;
    }
}
