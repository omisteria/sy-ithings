package com.synapse.ms.ithings.core.common.events;

public class ConnectionListenerAdapter implements ConnectionListener {

    @Override
    public void onConnectionChanged(boolean connected) {
    }

    @Override
    public void onReceiveData(byte[] message) {
    }
}
