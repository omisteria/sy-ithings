package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.BaseRulesEngine;

public class DateTimeModule extends AbstractModule {

    private TimeProvider timeProvider;
    private DateProvider dateProvider;
    private SchedulerProvider schedulerProvider;

    public DateTimeModule(BaseRulesEngine rulesManager) {
        super(rulesManager);

        timeProvider = new TimeProvider(this);
        dateProvider = new DateProvider(this);
        schedulerProvider = new SchedulerProvider(this);
    }

    public TimeProvider getTimeProvider() {
        return timeProvider;
    }

    public DateProvider getDateProvider() {
        return dateProvider;
    }

    public SchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }
}
