package com.synapse.ms.ithings.core.build;

import com.synapse.ms.ithings.core.config.*;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;

import java.util.Map;

public interface RepresenterFactory {

    Representer createRepresenter(Map<String, Object> description, Bridge bridge) throws Exception;

    Bridge createBridge(Map<String, Object> description) throws Exception;

    Channel createChannel(Map<String, Object> description, Representer representer);

    Bridge buildBridgeWithUnits(PlainConfiguration configuration);
}
