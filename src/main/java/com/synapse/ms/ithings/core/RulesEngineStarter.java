package com.synapse.ms.ithings.core;

import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.RuleScriptException;
import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.ScriptLoadEngine;
import com.synapse.ms.ithings.core.rules.impl.JavaScriptLoadEngine;

import java.io.*;
import java.util.stream.Collectors;

import com.synapse.ms.ithings.core.utils.ConfigContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RulesEngineStarter implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ScriptLoadEngine scriptLoadEngine;
    private RulesEngine rulesEngine;
    private String scriptFile;

    public RulesEngineStarter(String scriptFile, RulesEngine rulesEngine, RepresenterManager representerManager) {
        this.scriptFile = scriptFile;
        this.rulesEngine = rulesEngine;
        scriptLoadEngine = new JavaScriptLoadEngine(rulesEngine, representerManager);
    }

    private void startEngine() {
        scriptLoadEngine.start();

        try {
            ConfigContent configContent = new ConfigContent();
            scriptLoadEngine.loadScript(configContent.loadContent(scriptFile));
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            logger.error("Can't read Rules file", e);
        } catch (RuleScriptException e) {
            logger.error("Can't eval Rules script", e);
        }

        rulesEngine.start();
    }

    @Override
    public void run() {

        startEngine();

        while (true) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
