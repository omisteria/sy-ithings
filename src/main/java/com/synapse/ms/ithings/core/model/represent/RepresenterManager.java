package com.synapse.ms.ithings.core.model.represent;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.common.SimpleRegistry;
import com.synapse.ms.ithings.core.config.ConfKey;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.bridge.AbstractBridge;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import org.apache.log4j.Logger;

import java.util.Arrays;


public class RepresenterManager {

    Logger logger = Logger.getLogger(RepresenterManager.class);

    private SimpleRegistry<String, Bridge> bridgeRegistry = new SimpleRegistry<>();
    private SimpleRegistry<String, Representer> representerRegistry = new SimpleRegistry<>();
    private SimpleRegistry<String, Channel> channelRegistry = new SimpleRegistry<>();

    public RepresenterManager() {
    }

    public void buildAll(PlainConfiguration... configurations) {
        buildAllUnits(configurations);
    }

    public void buildAllAsync(PlainConfiguration... configurations) {
        new Thread(() -> buildAllUnits(configurations)).start();
    }

    private void buildAllUnits(PlainConfiguration... configurations) {
        Arrays.asList(configurations).forEach((PlainConfiguration configuration) -> {
            String domainGid = (String)configuration.get(ConfKey.domain);

            if (domainGid != null) {

                Domain domain = DomainDiscoveryService.getDomain(domainGid);

                if (domain != null) {

                    RepresenterFactory factory = domain.getRepresenterFactory();

                    Bridge bridge = factory.buildBridgeWithUnits(configuration);

                    if (!bridgeRegistry.contains(bridge.getGid())) {
                        bridgeRegistry.register(bridge);
                        logger.info("Registered: "+ bridge);

                        ((AbstractBridge)bridge).setRepresenterManager(this);

                        final int[] representerCnt = new int[] {0};

                        bridge.getRepresenters().forEach(rep -> {
                            representerRegistry.register(rep);
                            representerCnt[0]++;
                            logger.info("Registered: "+ rep);

                            rep.getChannels().forEach(channel -> {
                                channelRegistry.register(channel);
                                logger.info("Registered: "+ channel);
                            });
                            // subscribe for events from channels, emmit to bus
                            /*rep.getChannels().forEach(channel -> {
                                channel.addListener(new ChannelListenerAdapter() {
                                    @Override
                                    public void onChannelStateChanged(ChannelStateEvent event) {
                                        bus.publish(new StateEventBusEvent(event.getState()));
                                    }
                                });
                            });*/
                        });

                        logger.info(String.format("+++ Bridge '%s' is created with %d representer(s)", bridge.getGid(), representerCnt[0]));
                    }



                    try {
                        bridge.activate();
                        bridge.initAcquisitions();
                    } catch (Exception e) {
                        logger.error("Can't activate bridge '"+bridge.getGid()+"'", e);
                    }

                } else {
                    logger.warn("Domain ["+domainGid+"] is not registered");
                }

            } else {
                logger.warn("Configuration doesn't contain domain ID, "+ configuration);
            }
        });
    }

    public SimpleRegistry<String, Representer> getRepresenterRegistry() {
        return representerRegistry;
    }

    public SimpleRegistry<String, Bridge> getBridgeRegistry() {
        return bridgeRegistry;
    }

    public SimpleRegistry<String, Channel> getChannelRegistry() {
        return channelRegistry;
    }
/*private void activateAllUnits() {
        bridgeRegistry.forEach(bridge -> {
            try {
                bridge.activate();
            } catch (Exception e) {
                throw new IllegalStateException("Error due activating of bridge", e);
            }

            bridge.getRepresenters().forEach(representer -> {

                try {
                    representer.activate();
                } catch (Exception e) {
                    throw new IllegalStateException("Error due activating of representer", e);
                }

            });
        });
    }*/
}
