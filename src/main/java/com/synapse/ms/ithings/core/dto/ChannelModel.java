package com.synapse.ms.ithings.core.dto;


import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;

public class ChannelModel {

    private String gid;
    private State state;
    private ActivateStatus activateStatus;
    private ChannelType channelType;
    private String stateType;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public ActivateStatus getActivateStatus() {
        return activateStatus;
    }

    public void setActivateStatus(ActivateStatus activateStatus) {
        this.activateStatus = activateStatus;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getStateType() {
        return stateType;
    }

    public void setStateType(String stateType) {
        this.stateType = stateType;
    }

    public static ChannelModel toModel(Channel ch) {
        ChannelModel m = new ChannelModel();
        m.gid = ch.getGid();
        m.state = ch.getState().getValue();
        m.activateStatus = ch.getActivateStatus().getValue();
        m.channelType = ch.getType();
        m.stateType = ch.getStateType().getSimpleName();
        return m;
    }
}
