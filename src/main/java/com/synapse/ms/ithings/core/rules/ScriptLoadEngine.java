package com.synapse.ms.ithings.core.rules;

public interface ScriptLoadEngine {

    void loadScript(String script) throws RuleScriptException;

    void bind(String name, Object obj);

    void start();
}
