package com.synapse.ms.ithings.core.common.events.bus;

import com.synapse.ms.ithings.core.common.events.RepresenterActivateStatusEvent;
import net.engio.mbassy.listener.Handler;
import net.engio.mbassy.listener.Listener;
import net.engio.mbassy.listener.References;

@Listener(references = References.Strong)
public class RepresenterActivateStatusEventListener {

    @Handler
    public void handle(RepresenterActivateStatusEvent event) {
        throw new IllegalStateException("Not Implemented");
    }
}
