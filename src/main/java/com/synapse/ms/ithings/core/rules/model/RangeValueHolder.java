package com.synapse.ms.ithings.core.rules.model;

public class RangeValueHolder<S extends Number, T extends Number> {
    private Range<S> srcRange;
    private Range<T> targetRange;
    private S value;

    public RangeValueHolder(Range<S> srcRange, Range<T> targetRange) {
        this.srcRange = srcRange;
        this.targetRange = targetRange;
        this.value = srcRange.getStart();
    }

    public void setSourceValue(S value) {
        this.value = value;
    }

    public T getTargetValue() {
        return targetRange.partBy(srcRange.index(value));
    }

    public T convert(S value) {
        this.value = value;
        return targetRange.partBy(srcRange.index(value));
    }
}
