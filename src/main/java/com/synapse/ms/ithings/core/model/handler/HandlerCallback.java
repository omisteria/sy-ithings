package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.model.channel.Channel;

public interface HandlerCallback<T> {

    void apply(Channel channel, T data) throws Exception;
}
