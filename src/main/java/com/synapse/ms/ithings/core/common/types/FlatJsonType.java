package com.synapse.ms.ithings.core.common.types;

import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class FlatJsonType implements State, Command {

    private final Map<String, String> value;

    private static Gson gson = new Gson();

    public FlatJsonType(Map<String, String> value) {
        this.value = value;
    }
    public FlatJsonType(FlatJsonType from) {
        this.value = new HashMap<>(from.value);
    }

    public static FlatJsonType valueOf(String value) {
        return new FlatJsonType(gson.fromJson(value, Map.class));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlatJsonType)) return false;
        FlatJsonType that = (FlatJsonType) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public String getByKey(String key) {
        return value.get(key);
    }

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == RawType.class) {
            return target.cast(new RawType(gson.toJson(value).getBytes()));
        } else if (target == BooleanType.class) {
            return target.cast(value != null ? BooleanType.TRUE : BooleanType.FALSE);
        } else if (target == StringType.class) {
            return target.cast(value != null ? new StringType(gson.toJson(value)) : new StringType());
        } else {
            return null;
        }
    }

    @Override
    public FlatJsonType clone(State fromState) {
        return new FlatJsonType((FlatJsonType)fromState);
    }

    @Override
    public String toString() {
        return "FlatJsonType: " + value;
    }
}
