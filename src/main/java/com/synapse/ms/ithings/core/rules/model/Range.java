package com.synapse.ms.ithings.core.rules.model;

public interface Range<T extends Number> {
    T getStart();
    T getEnd();
    double index(T value);
    T partBy(double index);
    Range<T> reverse();
}
