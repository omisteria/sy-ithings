package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.common.events.BridgeListener;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractBridgeDataHandler implements DataHandler<byte[], byte[]>, BridgeListener {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected Bridge bridge;

    public AbstractBridgeDataHandler(Bridge bridge) {
        this.bridge = bridge;
    }
}
