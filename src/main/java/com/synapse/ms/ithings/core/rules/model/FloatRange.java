package com.synapse.ms.ithings.core.rules.model;

public class FloatRange implements Range<Double> {

    private Double start;
    private Double end;
    private boolean revers;

    public FloatRange(Double start, Double end) {
        this.start = start;
        this.end = end;
        this.revers = this.start > this.end;
    }

    @Override
    public Double getStart() {
        return start;
    }

    @Override
    public Double getEnd() {
        return end;
    }

    @Override
    public Range reverse() {
        return new FloatRange(end, start);
    }

    @Override
    public double index(Double value) {
        if (revers) {
            if (value >= this.start) return 0D;
            if (value <= this.end) return 1D;
            return (start - value) / (start - end);
        } else {
            if (value <= this.start) return 0D;
            if (value >= this.end) return 1D;
            return (value - start) / (end - start);
        }
    }

    @Override
    public Double partBy(double index) {
        return start + (end - start) * index;
    }
}
