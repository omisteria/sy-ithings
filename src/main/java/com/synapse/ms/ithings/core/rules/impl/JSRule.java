package com.synapse.ms.ithings.core.rules.impl;

import com.synapse.ms.ithings.core.rules.Filter;
import com.synapse.ms.ithings.core.rules.Rule;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;

import java.util.function.Function;

public class JSRule implements Rule {

    private String name;
    private RulesEngineTrigger trigger;
    private Filter filter;
    private Function<Void, Void> handler;

    public JSRule(RulesEngineTrigger trigger,
                  Filter filter,
                  Function<Void, Void> handler) {
        this.name = "Noname_"+hashCode();
        this.trigger = trigger;
        this.filter = filter;
        if (this.filter == null) {
            this.filter = new DefaultFilter();
        }
        this.handler = handler;
    }

    public JSRule(String name, RulesEngineTrigger trigger,
                  Filter filter,
                  Function<Void, Void> handler) {
        this.name = name;
        this.trigger = trigger;
        this.filter = filter;
        if (this.filter == null) {
            this.filter = new DefaultFilter();
        }
        this.handler = handler;
    }

    public RulesEngineTrigger getTrigger() {
        return trigger;
    }

    public Function<Void, Void> getHandler() {
        return handler;
    }

    public Filter getFilter() {
        return filter;
    }

    @Override
    public String getName() {
        return name;
    }

    public boolean triggered() {
        /*if (trigger != null) {
            Boolean res = trigger.apply(null);
            return res != null && res;
        }*/
        return false;
    }

    @Override
    public void execute() {
        if (handler != null) {
            handler.apply(null);
        }
    }

    public class DefaultFilter implements Filter<Object> {

        @Override
        public boolean filtered(Object data) {
            return true;
        }
    }
}
