package com.synapse.ms.ithings.core.rules;

public class RuleScriptException extends Exception {
    public RuleScriptException() {
        super();
    }

    public RuleScriptException(String message) {
        super(message);
    }

    public RuleScriptException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuleScriptException(Throwable cause) {
        super(cause);
    }

    protected RuleScriptException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
