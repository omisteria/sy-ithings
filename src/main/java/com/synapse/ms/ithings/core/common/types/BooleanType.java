package com.synapse.ms.ithings.core.common.types;

import java.util.Objects;

public class BooleanType implements State, Command {

    public static final BooleanType TRUE = new BooleanType(true);
    public static final BooleanType FALSE = new BooleanType(false);

    protected boolean value;

    public BooleanType(boolean value) {
        this.value = value;
    }

    public BooleanType(BooleanType from) {
        this.value = from.value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BooleanType)) return false;
        BooleanType that = (BooleanType) o;
        return getValue() == that.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == DecimalType.class) {
            return target.cast(value ? new DecimalType(1) : DecimalType.ZERO);
        } else if (target == RawType.class) {
            return target.cast(value ? new RawType("1".getBytes()) : new RawType("0".getBytes()) );
        } else if (target == PercentType.class) {
            return target.cast(value ? PercentType.HUNDRED : PercentType.ZERO);
        } else if (target == OnOffType.class) {
            return target.cast(value ? OnOffType.ON : OnOffType.OFF);
        } else if (target == BooleanType.class) {
            return target.cast(value ? BooleanType.TRUE : BooleanType.FALSE);
        } else {
            return null;
        }
    }

    @Override
    public BooleanType clone(State fromState) {
        return new BooleanType((BooleanType)fromState);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
