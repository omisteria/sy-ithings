package com.synapse.ms.ithings.core.common.events.bus;

import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.model.channel.Channel;

import java.io.Serializable;

public class StateEventBusEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private Channel channel;
    private State oldState;
    private State state;

    public StateEventBusEvent(Channel channel, State oldState, State state) {
        this.channel = channel;
        this.oldState = oldState;
        this.state = state;
    }

    public Channel getChannel() {
        return channel;
    }

    public State getState() {
        return state;
    }

    public State getOldState() {
        return oldState;
    }

    @Override
    public String toString() {
        return "StateEventBusEvent [channel="+channel.getGid()+", new state="+state+"]";
    }
}
