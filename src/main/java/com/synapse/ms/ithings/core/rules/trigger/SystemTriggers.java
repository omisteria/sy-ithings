package com.synapse.ms.ithings.core.rules.trigger;

import com.synapse.ms.ithings.core.common.events.bus.SystemStartedBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.SystemStartedBusEventListener;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.rules.BaseRulesEngine;

public class SystemTriggers {

    public static final String TRIGGER_ID_STARTED = "system.started";

    private BaseRulesEngine rulesManager;

    public SystemTriggers(BaseRulesEngine rulesManager) {
        this.rulesManager = rulesManager;
    }

    public RulesEngineTrigger started() {
        RulesEngineTrigger trigger = rulesManager.getOrCreateTrigger(TRIGGER_ID_STARTED, s -> {
            RulesEngineTrigger newTrigger = new SimpleRulesEngineTrigger(s);
            newTrigger.setActivator(() -> {
                SystemStartedBusEventListener listener = new SystemStartedBusEventListener() {
                    @Override
                    public void handle(SystemStartedBusEvent event) {
                        newTrigger.fire();
                    }
                };
                SystemEngineContext.getBus().subscribe(listener);
            });
            return newTrigger;
        });

        return trigger;
    }
}
