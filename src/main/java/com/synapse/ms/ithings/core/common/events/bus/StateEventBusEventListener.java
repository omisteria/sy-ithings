package com.synapse.ms.ithings.core.common.events.bus;

import net.engio.mbassy.listener.Handler;
import net.engio.mbassy.listener.Listener;
import net.engio.mbassy.listener.References;

@Listener(references = References.Strong)
public class StateEventBusEventListener {

    @Handler
    public void handle(StateEventBusEvent event) {
        throw new IllegalStateException("Not Implemented");
    }
}
