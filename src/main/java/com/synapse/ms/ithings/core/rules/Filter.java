package com.synapse.ms.ithings.core.rules;

public interface Filter<T> {

    boolean filtered(T data);
}
