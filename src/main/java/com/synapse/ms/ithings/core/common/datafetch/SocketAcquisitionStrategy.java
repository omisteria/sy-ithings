package com.synapse.ms.ithings.core.common.datafetch;

import com.synapse.ms.ithings.core.model.handler.DataHandler;

public class SocketAcquisitionStrategy implements AcquisitionStrategy {

    public SocketAcquisitionStrategy(String host, int port) {
    }

    @Override
    public void activate(DataHandler dataHandler) throws Exception {
        //Socket socket = new Socket();
        dataHandler.onReceivedResponse(null);
    }

    @Override
    public void deactivate() throws Exception {

    }
}
