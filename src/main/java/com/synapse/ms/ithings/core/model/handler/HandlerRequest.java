package com.synapse.ms.ithings.core.model.handler;

import com.synapse.ms.ithings.core.model.channel.Channel;

public interface HandlerRequest<T> {

    void doRequest(Channel channel, T data, HandlerCallback callback);
}
