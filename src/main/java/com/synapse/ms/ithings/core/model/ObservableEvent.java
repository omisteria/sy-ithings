package com.synapse.ms.ithings.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ObservableEvent<V, T> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected transient T source;
    protected transient V prevValue;
    protected transient V value;
    private LocalDateTime changedAt = LocalDateTime.now();

    public ObservableEvent(T source, V prevValue, V newValue) {
        this.source = source;
        this.prevValue = prevValue;
        this.value = newValue;
    }

    public T getSource() {
        return source;
    }

    public V getValue() {
        return value;
    }

    public V getPreviousValue() {
        return prevValue;
    }

    public LocalDateTime getChangedAt() {
        return changedAt;
    }
}
