package com.synapse.ms.ithings.core.common.events;

import com.synapse.ms.ithings.core.common.types.State;
import com.synapse.ms.ithings.core.model.ObservableEvent;
import com.synapse.ms.ithings.core.model.channel.Channel;

public interface ChannelListener {

    void onChannelStateChanged(ObservableEvent<State, Channel> event, Channel channel);
}
