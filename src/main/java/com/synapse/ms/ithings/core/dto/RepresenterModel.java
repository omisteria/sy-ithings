package com.synapse.ms.ithings.core.dto;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;

import java.util.List;
import java.util.stream.Collectors;

public class RepresenterModel {

    private String bridgeGid;
    private String gid;
    private boolean enabled;
    private List<ChannelModel> channels;
    private ActivateStatus activateStatus;

    public String getGid() {
        return gid;
    }
    public void setGid(String gid) {
        this.gid = gid;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<ChannelModel> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelModel> channels) {
        this.channels = channels;
    }

    public static RepresenterModel toModel(Representer rep) {
        RepresenterModel m = new RepresenterModel();
        m.bridgeGid = rep.getBridge().getGid();
        m.gid = rep.getGid();
        m.enabled = rep.isEnabled();
        m.channels = rep.getChannels().stream().map(ChannelModel::toModel).collect(Collectors.toList());
        m.activateStatus = rep.getActivateStatus().getValue();
        return m;
    }
}
