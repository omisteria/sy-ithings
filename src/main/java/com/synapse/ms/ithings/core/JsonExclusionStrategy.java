package com.synapse.ms.ithings.core;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.synapse.ms.ithings.core.common.json.JsonIgnore;

public class JsonExclusionStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes field) {
        return field.getAnnotation(JsonIgnore.class) != null;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
