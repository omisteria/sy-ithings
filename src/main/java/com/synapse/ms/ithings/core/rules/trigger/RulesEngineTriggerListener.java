package com.synapse.ms.ithings.core.rules.trigger;

public interface RulesEngineTriggerListener {

    void execute(RulesEngineTrigger trigger);
}
