package com.synapse.ms.ithings.core.common.types;

import java.math.BigDecimal;

public class DecimalType extends Number implements State, Command, Comparable<DecimalType> {

    private static final long serialVersionUID = 4226845847123464690L;

    public static final DecimalType ZERO = new DecimalType(0);

    protected BigDecimal value;

    public DecimalType() {
        this.value = BigDecimal.ZERO;
    }

    public DecimalType(BigDecimal value) {
        this.value = value;
    }

    public DecimalType(long value) {
        this.value = BigDecimal.valueOf(value);
    }

    public DecimalType(double value) {
        this.value = BigDecimal.valueOf(value);
    }

    public DecimalType(float value) {
        this.value = BigDecimal.valueOf(value);
    }

    public DecimalType(String value) {
        this.value = new BigDecimal(value);
    }

    public DecimalType(DecimalType from) {
        value = from.value;
    }

    @Override
    public String toString() {
        return value == null ? "null" : value.toString();
    }

    public static DecimalType valueOf(String value) {
        return new DecimalType(value);
    }

    public BigDecimal toBigDecimal() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DecimalType)) {
            return false;
        }
        DecimalType other = (DecimalType) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (value.compareTo(other.value) != 0) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(DecimalType o) {
        return value.compareTo(o.toBigDecimal());
    }

    @Override
    public double doubleValue() {
        return value.doubleValue();
    }

    @Override
    public float floatValue() {
        return value.floatValue();
    }

    @Override
    public int intValue() {
        return value.intValue();
    }

    @Override
    public long longValue() {
        return value.longValue();
    }

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == OnOffType.class) {
            return target.cast(equals(ZERO) ? OnOffType.OFF : OnOffType.ON);
        } else if (target == StringType.class) {
            return target.cast(value.toString());
        } else if (target == PercentType.class) {
            return target.cast(new PercentType(toBigDecimal().multiply(BigDecimal.valueOf(100))));
        } else if (target == BooleanType.class) {
            return target.cast(equals(ZERO) ? BooleanType.FALSE : BooleanType.TRUE);
        } else if (target == DecimalType.class) {
            return target.cast(new DecimalType(value));
        } else {
            return null;
        }
    }

    @Override
    public DecimalType clone(State fromState) {
        return new DecimalType((DecimalType)fromState);
    }

}
