package com.synapse.ms.ithings.core.model.channel;

import com.synapse.ms.ithings.core.common.Identifiable;
import com.synapse.ms.ithings.core.common.types.State;

public class PredefinedChannel implements Identifiable<String> {

    private String gid;

    private ChannelType type;

    private State initState;

    public PredefinedChannel(String gid, ChannelType type, State initState) {
        this.gid = gid;
        this.type = type;
        this.initState = initState;
    }

    @Override
    public String getGid() {
        return gid;
    }

    public ChannelType getType() {
        return type;
    }

    public State getInitState() {
        return initState;
    }
}
