package com.synapse.ms.ithings.core.rules;

import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;

import java.util.List;
import java.util.function.Function;

public interface Rule {

    String getName();

    RulesEngineTrigger getTrigger();
    Function<Void, Void> getHandler();
    Filter getFilter();

    boolean triggered();

    void execute();
}
