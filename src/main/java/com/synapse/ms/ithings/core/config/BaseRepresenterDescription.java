package com.synapse.ms.ithings.core.config;

import java.util.List;

@Deprecated
public class BaseRepresenterDescription implements RepresenterDescription {

    //private String bridgeGID;

    private String gid;

    private String className;
    //private DriverDescription driver;

    private List<BaseChannelDescription> channels;

    private Long pollingPeriod;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    /*public String getBridgeGid() {
        return bridgeGid;
    }

    public void setBridgeGid(String bridgeGid) {
        this.bridgeGid = bridgeGid;
    }*/

    /*public DriverDescription getDriver() {
        return driver;
    }

    public void setDriver(DriverDescription driver) {
        this.driver = driver;
    }
*/

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<BaseChannelDescription> getChannels() {
        return channels;
    }

    public void setChannels(List<BaseChannelDescription> channels) {
        this.channels = channels;
    }

    public Long getPollingPeriod() {
        return pollingPeriod;
    }

    public void setPollingPeriod(Long pollingPeriod) {
        this.pollingPeriod = pollingPeriod;
    }
}
