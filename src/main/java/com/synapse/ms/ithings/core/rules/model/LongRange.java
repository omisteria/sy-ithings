package com.synapse.ms.ithings.core.rules.model;

public class LongRange implements Range<Long> {

    private Long start;
    private Long end;
    private boolean revers;

    public LongRange(Long start, Long end) {
        this.start = start;
        this.end = end;
        this.revers = this.start > this.end;
    }

    @Override
    public Long getStart() {
        return start;
    }

    @Override
    public Long getEnd() {
        return end;
    }

    @Override
    public double index(Long value) {
        if (revers) {
            if (value >= this.start) return 0D;
            if (value <= this.end) return 1D;
            return (start - value) / (start - end);
        } else {
            if (value <= this.start) return 0D;
            if (value >= this.end) return 1D;
            return (value - start) / (end - start);
        }
    }

    @Override
    public Long partBy(double index) {
        return Math.round(start + (end - start) * index);
    }

    @Override
    public Range<Long> reverse() {
        return new LongRange(end, start);
    }
}
