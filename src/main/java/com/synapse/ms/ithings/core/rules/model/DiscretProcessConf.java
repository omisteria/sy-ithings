package com.synapse.ms.ithings.core.rules.model;

public class DiscretProcessConf {

    /**
     * pulsePeriod = pulseDuration + pauseDuration
     * unit: seconds
     */
    private Integer pulsePeriod;

    /**
     *
     */
    private Integer pulseDuration;
    //private Long pauseDuration;

    public DiscretProcessConf(Integer pulsePeriod, Integer pulseDuration) {
        this.pulsePeriod = pulsePeriod;
        this.pulseDuration = pulseDuration;
    }

    public Integer getPulsePeriod() {
        return pulsePeriod;
    }

    public Integer getPulseDuration() {
        return pulseDuration;
    }
}
