package com.synapse.ms.ithings.core.model;

import net.engio.mbassy.bus.MBassador;

public class SystemEngineContext {

    public static final MBassador<Object> bus = new MBassador<>();

    public static MBassador getBus() {
        return bus;
    }
}
