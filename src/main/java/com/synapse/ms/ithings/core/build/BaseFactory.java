package com.synapse.ms.ithings.core.build;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;

import java.util.Map;

public class BaseFactory extends AbstractRepresenterFactory {

    public BaseFactory(Domain domain) {
        super(domain);
    }

    @Override
    public Channel createChannel(Map<String, Object> description, Representer representer) {
        return super.createChannel(description, representer);
    }
}
