package com.synapse.ms.ithings.core.model.bridge;

import com.synapse.ms.ithings.core.common.datafetch.AcquisitionStrategy;
import com.synapse.ms.ithings.core.common.datafetch.PollingAcquisitionStrategy;
import com.synapse.ms.ithings.core.common.events.BridgeActivateStatusEvent;
import com.synapse.ms.ithings.core.common.events.BridgeListener;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.config.ConfKey;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.Observable;
import com.synapse.ms.ithings.core.model.channel.AbstractChannel;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;
import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public abstract class AbstractBridge implements Bridge {

    protected final Logger logger = LoggerFactory.getLogger(AbstractBridge.class);

    protected final Map<String, Object> description;
    private final String gid;

    private transient List<Representer> representers = new CopyOnWriteArrayList<>();
    private List<BridgeListener> listeners = new ArrayList<>();

    protected PropertyChangeSupport valuesChanges;

    private BridgeDataHandler dataHandler;
    private AcquisitionStrategy strategy;
    private Observable<ActivateStatus> status;

    private RepresenterManager representerManager;



    public AbstractBridge(String gid, Map<String, Object> description) {
        this.gid = gid;
        this.description = description;
        status = new Observable(ActivateStatus.UNINITIALIZED);
        valuesChanges = new PropertyChangeSupport(this);
    }

    @Override
    public String getGid() {
        return gid;
    }

    public void configure() {
        // empty
    }

    public void initAcquisitions() {
        getRepresenters().forEach(rep -> {

            //
            try {
                if (rep.getDescription().get(ConfKey.pollingPeriod) != null){
                    long period = ((Double) rep.getDescription().get(ConfKey.pollingPeriod)).longValue();
                    PollingAcquisitionStrategy strategy = new PollingAcquisitionStrategy(period);
                    strategy.activate(new DataHandler<byte[], byte[]>() {
                        @Override
                        public void sendRequest(Command command) throws Exception {
                            if (rep.getActivateStatus().getValue().equals(ActivateStatus.ONLINE)) {
                                rep.getDataHandler().sendRequest(command);
                            }
                        }

                        @Override
                        public void onReceivedResponse(byte[] responseData) {
                            if (rep.getActivateStatus().getValue().equals(ActivateStatus.ONLINE)) {
                                rep.getDataHandler().onReceivedResponse(responseData);
                            }
                        }
                    });
                    ((AbstractRepresenter) rep).setStrategy(strategy);
                }

            } catch (Exception e) {
                logger.error("Can't set polling strategy", e);
            }

            rep.getChannels().forEach(channel -> {
                //
                try {
                    if (channel.getDescription().get(ConfKey.pollingPeriod) != null) {
                        long period = ((Double) channel.getDescription().get(ConfKey.pollingPeriod)).longValue();
                        PollingAcquisitionStrategy strategy = new PollingAcquisitionStrategy(period);
                        strategy.activate(new DataHandler<byte[], byte[]>() {
                            @Override
                            public void sendRequest(Command command) throws Exception {
                                if (channel.getActivateStatus().getValue().equals(ActivateStatus.ONLINE)) {
                                    rep.getDataHandler().sendRequest(command, channel.getGid());
                                }
                            }

                            @Override
                            public void onReceivedResponse(byte[] responseData) {
                                if (channel.getActivateStatus().getValue().equals(ActivateStatus.ONLINE)) {
                                    rep.getDataHandler().onReceivedResponse(responseData, channel.getGid());
                                }
                            }
                        });
                        ((AbstractChannel) channel).setStrategy(strategy);
                    }
                } catch (Exception e) {
                    logger.error("Can't set polling strategy", e);
                }
            });
        });
    }

    @Override
    public void activate() throws Exception {
        /*if (strategy != null) {
            strategy.activate(dataHandler);
        }*/

        if (dataHandler != null) {
            new Thread(() -> dataHandler.initialize()).start();
        }
    }

    @Override
    public void dispose() throws Exception {
        getActivateStatus().update(ActivateStatus.OFFLINE);
    }

    public void addRepresenter(Representer representer) {
        representers.add(representer);
    }

    public void removeRepresenter(Representer representer) {
        representers.remove(representer);
    }

    @Override
    public List<Representer> getRepresenters() {
        return Collections.unmodifiableList(new ArrayList<>(representers));
    }

    public Representer getRepresenterByUID(String uid) {
        for (Representer rep : representers) {
            if (rep.getGid().equals(uid)) {
                return rep;
            }
        }

        return null;
    }

    /*@Override
    public ActivateStatus getStatus() {
        return status;
    }*/

    /*public void updateStatus(ActivateStatus newStatus) {
        synchronized (this) {
            ActivateStatus oldValue = this.status;
            this.status = newStatus;
            notifyListeners(listener -> listener.onBridgeActivateStatusChanged(new BridgeActivateStatusEvent(this, oldValue, newStatus)));
        }
    }*/

    public DataHandler getDataHandler() {
        return dataHandler;
    }

    public void setDataHandler(BridgeDataHandler dataHandler) {
        this.dataHandler = dataHandler;
        // strategy = dataHandler.getAcquisitionStrategy();
        getRepresenters().forEach(rep -> {
            rep.getActivateStatus().addListener(event -> {
                BridgeActivateStatusEvent oldStyleEvent =
                        new BridgeActivateStatusEvent(this, (ActivateStatus)event.getPreviousValue(), (ActivateStatus)event.getValue());
                dataHandler.onBridgeActivateStatusChanged(oldStyleEvent);
            });
        });
    }

    @Override
    public synchronized BridgeListener addListener(BridgeListener listener) {
        listeners.add(listener);
        return listener;
    }
    public synchronized void removeListener(BridgeListener listener) {
        this.listeners.remove(listener);
    }
    public synchronized void notifyListeners(Consumer<? super BridgeListener> algorithm) {
        this.listeners.forEach(listener -> new Thread(() -> algorithm.accept(listener)).start());
    }

    public Map<String, Object> getDescription() {
        return description;
    }

    @Override
    public Observable<ActivateStatus> getActivateStatus() {
        return status;
    }

    @Override
    public RepresenterManager getRepresenterManager() {
        return representerManager;
    }

    public void setRepresenterManager(RepresenterManager representerManager) {
        this.representerManager = representerManager;
    }

    @Override
    public String toString() {
        return "Bridge{" +
                "gid='" + gid + '\'' +
                '}';
    }
}
