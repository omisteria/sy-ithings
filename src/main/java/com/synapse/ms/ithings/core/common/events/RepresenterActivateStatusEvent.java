package com.synapse.ms.ithings.core.common.events;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.represent.Representer;

import java.io.Serializable;

public class RepresenterActivateStatusEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    protected transient Representer source;
    protected transient ActivateStatus oldStatus;
    protected transient ActivateStatus status;

    public RepresenterActivateStatusEvent(Representer source, ActivateStatus oldStatus, ActivateStatus newStatus) {
        this.source = source;
        this.oldStatus = oldStatus;
        this.status = newStatus;
    }

    public Representer getSource() {
        return source;
    }

    public ActivateStatus getStatus() {
        return status;
    }

    public ActivateStatus getOldStatus() {
        return oldStatus;
    }

    @Override
    public String toString() {
        return "RepresenterActivateStatusEvent [representer="+source.getGid()+", new status="+status+"]";
    }
}
