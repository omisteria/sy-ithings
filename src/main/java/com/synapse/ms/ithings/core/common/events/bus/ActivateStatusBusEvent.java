package com.synapse.ms.ithings.core.common.events.bus;

import com.synapse.ms.ithings.core.model.ActivateStatus;

public class ActivateStatusBusEvent {

    private ActivateStatus activateStatus;

    public ActivateStatusBusEvent(ActivateStatus activateStatus) {
        this.activateStatus = activateStatus;
    }
}
