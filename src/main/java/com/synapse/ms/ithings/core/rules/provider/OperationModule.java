package com.synapse.ms.ithings.core.rules.provider;

import com.synapse.ms.ithings.core.rules.Filter;
import com.synapse.ms.ithings.core.rules.BaseRulesEngine;
import com.synapse.ms.ithings.core.rules.RulesEngine;
import com.synapse.ms.ithings.core.rules.trigger.AbstractRuresEngineTrigger;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTrigger;
import com.synapse.ms.ithings.core.rules.trigger.RulesEngineTriggerListener;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class OperationModule extends AbstractModule {

    public OperationModule(RulesEngine rulesEngine) {
        super(rulesEngine);
    }

    public RulesEngineTrigger or(RulesEngineTrigger ...arguments) {
        RulesEngineTrigger[] triggers = Arrays.stream(arguments)
                .filter(distinctByKey(RulesEngineTrigger::getGid))
                .toArray(size -> new RulesEngineTrigger[size]);

        String triggerId = "trigger."; //TODO generate unique ID
        return getRulesEngine().getOrCreateTrigger(triggerId, s -> new AgrAnyRulesEngineTrigger(s, triggers));
    }

    public Boolean or(Boolean ...arguments) {
        return true;
    }

    public Filter or(Filter...arguments) {
        return new ArgAnyFilter(arguments);
    }

    // TODO: implement Operation.AND
    public RulesEngineTrigger and(RulesEngineTrigger ...arguments) {
        RulesEngineTrigger[] triggers = Arrays.stream(arguments)
                .filter(distinctByKey(RulesEngineTrigger::getGid))
                .toArray(size -> new RulesEngineTrigger[size]);

        String triggerId = "trigger."; //TODO generate unique ID
        return getRulesEngine().getOrCreateTrigger(triggerId, s -> new AgrAnyRulesEngineTrigger(s, triggers));
    }

    public Boolean and(Boolean ...arguments) {
        return true;
    }

    public Filter and(Filter...arguments) {
        return new ArgAnyFilter(arguments);
    }



    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public static class AgrAnyRulesEngineTrigger extends AbstractRuresEngineTrigger {

        private long lastTime = System.currentTimeMillis();

        public AgrAnyRulesEngineTrigger(String id, RulesEngineTrigger...triggers) {
            super(id);

            RulesEngineTriggerListener internalListener = aVoid -> {
                long t = System.currentTimeMillis();
                long diff = t - lastTime;
                lastTime = t;

                if (diff > 200) {
                    fire();
                }
            };

            Stream.of(triggers).forEach(t -> {
                t.getListeners().add(internalListener);
            });

            setActivator(() -> {
                Stream.of(triggers).forEach(t -> {
                    t.activate();
                });
            });
        }
    }

    public static class ArgAnyFilter implements Filter {
        public ArgAnyFilter(Filter ...filters) {
        }

        @Override
        public boolean filtered(Object data) {
            return false;
        }
    }
}
