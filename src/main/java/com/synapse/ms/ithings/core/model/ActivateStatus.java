package com.synapse.ms.ithings.core.model;

public enum ActivateStatus {
    UNINITIALIZED,
    INITIALIZING,
    UNKNOWN,
    ONLINE,
    OFFLINE,
    REMOVING,
    REMOVED
}
