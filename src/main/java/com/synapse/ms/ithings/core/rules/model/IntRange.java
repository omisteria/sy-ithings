package com.synapse.ms.ithings.core.rules.model;

public class IntRange implements Range<Integer> {

    private Integer start;
    private Integer end;
    private boolean revers;

    public IntRange(Integer start, Integer end) {
        this.start = start;
        this.end = end;
        this.revers = this.start > this.end;
    }

    @Override
    public Integer getStart() {
        return start;
    }

    @Override
    public Integer getEnd() {
        return end;
    }

    @Override
    public double index(Integer value) {
        if (revers) {
            if (value >= this.start) return 0D;
            if (value <= this.end) return 1D;
            return (start - value) / (start - end);
        } else {
            if (value <= this.start) return 0D;
            if (value >= this.end) return 1D;
            return (value - start) / (end - start);
        }
    }

    @Override
    public Integer partBy(double index) {
        return Math.toIntExact(Math.round(start + (end - start) * index));
    }

    @Override
    public Range<Integer> reverse() {
        return new IntRange(end, start);
    }
}
