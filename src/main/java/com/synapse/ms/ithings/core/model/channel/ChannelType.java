package com.synapse.ms.ithings.core.model.channel;

public enum ChannelType {

    RELAY,
    PROXY,

    IR,
    IR_SENSOR,
    IR_TRANSMITTER,

    DRY_CONTACT,
    SWITCH,
    BUTTON,

    COLOR,

    TEMPERATURE,
    HUMIDITY,
    PRESSURE,

    NUMBER,
    NUM_FLOAT,
    NUM_INT,
    NUM_SHORT
}
