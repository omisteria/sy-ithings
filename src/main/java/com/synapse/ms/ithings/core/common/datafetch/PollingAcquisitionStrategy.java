package com.synapse.ms.ithings.core.common.datafetch;

import com.synapse.ms.ithings.core.model.handler.DataHandler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PollingAcquisitionStrategy implements AcquisitionStrategy {

    private Long pollInterval;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public PollingAcquisitionStrategy(Long pollInterval) {
        this.pollInterval = pollInterval;
        if (this.pollInterval == null) {
            this.pollInterval = AcquisitionStrategy.DEFAULT_POLLING_INTERVAL;
        }
    }

    @Override
    public void activate(DataHandler dataHandler) throws Exception {
        scheduler.scheduleAtFixedRate(() -> {
            try {
                dataHandler.sendRequest(null);
            } catch (Exception e) {
                throw new IllegalStateException("Error during make send request", e);
            }
        }, 10, pollInterval, TimeUnit.SECONDS);
    }

    @Override
    public void deactivate() throws Exception {

    }
}
