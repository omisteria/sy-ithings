package com.synapse.ms.ithings.core.rules.utils;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtils {
    private static Location location = new Location("50.6195733", "26.2512302"); // Ukraine, Rivne
    private static SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, TimeZone.getDefault().getID()); // +0300

    public static LocalDateTime getSunset() {
        return convertToLocalDateTime(calculator.getOfficialSunsetCalendarForDate(Calendar.getInstance()).getTime());
    }

    public static LocalDateTime getSunrise() {
        return convertToLocalDateTime(calculator.getOfficialSunriseCalendarForDate(Calendar.getInstance()).getTime());

    }

    public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
