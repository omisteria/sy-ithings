package com.synapse.ms.ithings.core.common.datafetch;

import com.synapse.ms.ithings.core.model.handler.DataHandler;

public interface AcquisitionStrategy {

    Long DEFAULT_POLLING_INTERVAL = 10L; // seconds

    void activate(DataHandler dataHandler) throws Exception;

    void deactivate() throws Exception;
}
