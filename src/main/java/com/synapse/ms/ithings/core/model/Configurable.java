package com.synapse.ms.ithings.core.model;

import java.util.Map;

public interface Configurable {

    Map<String, Object> getDescription();
}
