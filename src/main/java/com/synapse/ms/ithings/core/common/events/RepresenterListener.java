package com.synapse.ms.ithings.core.common.events;

import com.synapse.ms.ithings.core.model.channel.Channel;

public interface RepresenterListener {

    void onRepresenterActivateStatusChanged(RepresenterActivateStatusEvent event);
}
