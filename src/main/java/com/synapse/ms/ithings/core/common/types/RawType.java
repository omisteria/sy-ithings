package com.synapse.ms.ithings.core.common.types;

import java.math.BigInteger;
import java.util.Arrays;

public class RawType implements State, Command {

    protected byte[] bytes;

    public RawType(byte[] bytes) {
        this.bytes = bytes;
    }

    public RawType(RawType from) {
        if (from.bytes != null) {
            this.bytes = from.bytes.clone();
        }
    }

    public RawType(int i) {
        BigInteger bigInt = BigInteger.valueOf(i);
        bytes = bigInt.toByteArray();
    }

    public byte[] getBytes() {
        return bytes;
    }

    public static RawType valueOf(String value) {
        if (value == null) {
            throw new IllegalArgumentException("Argument must not be null");
        } else if (value.isEmpty()) {
            throw new IllegalArgumentException("Argument must not be blank");
        }
        return new RawType(value.getBytes());
    }

    @Override
    public String toString() {
        return new String(bytes);
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(bytes);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RawType other = (RawType) obj;
        if (!Arrays.equals(bytes, other.bytes)) {
            return false;
        }
        return true;
    }

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == OnOffType.class) {
            return target.cast(bytes != null && (bytes[0]==49 || bytes[0]==1) ? OnOffType.ON : OnOffType.OFF);
        } else if (target == PercentType.class) {
            return target.cast(bytes != null && (bytes[0]==49 || bytes[0]==1) ? PercentType.HUNDRED : PercentType.ZERO);
        } else if (target == DecimalType.class) {
            return target.cast(bytes != null ? DecimalType.valueOf(new String(bytes).trim()) : null);
        } else if (target == BooleanType.class) {
            return target.cast(bytes != null && (bytes[0]==49 || bytes[0]==1) ? BooleanType.TRUE : BooleanType.FALSE);
        } else if (target == StringType.class) {
            return target.cast(bytes != null ? new StringType(new String(bytes)) : new StringType());
        } else if (target == FlatJsonType.class) {
            return target.cast(bytes != null ? FlatJsonType.valueOf(new String(bytes)) : null);
        } else {
            return null;
        }
    }

    @Override
    public RawType clone(State fromState) {
        return new RawType((RawType)fromState);
    }
}
