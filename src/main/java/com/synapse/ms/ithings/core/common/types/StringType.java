package com.synapse.ms.ithings.core.common.types;

import java.util.Objects;

public class StringType implements State, Command {

    public static final StringType EMPTY = new StringType();

    private final String value;

    public StringType() {
        this("");
    }

    public StringType(String value) {
        this.value = value;
    }
    public StringType(StringType from) {
        this.value = from.value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static StringType valueOf(String value) {
        return new StringType(value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj instanceof String) {
            return obj.equals(value);
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        StringType other = (StringType) obj;
        return Objects.equals(this.value, other.value);
    }

    @Override
    public <T extends State> T as(Class<T> target) {
        if (target == RawType.class) {
            return target.cast(new RawType(value.getBytes()));
        } else if (target == BooleanType.class) {
            return target.cast(value != null && (value.equalsIgnoreCase ("ON") || value.equalsIgnoreCase ("1")) ? BooleanType.TRUE : BooleanType.FALSE);
        } else if (target == OnOffType.class) {
            return target.cast(value != null && (value.equalsIgnoreCase ("ON") || value.equalsIgnoreCase ("1")) ? OnOffType.ON : OnOffType.OFF);
        } else if (target == PercentType.class) {
            return target.cast(value == null ? PercentType.ZERO : PercentType.valueOf(value));
        } else if (target == DecimalType.class) {
            return target.cast(value == null ? DecimalType.ZERO : DecimalType.valueOf(value));
        } else if (target == FlatJsonType.class) {
            return target.cast(value == null ? null : FlatJsonType.valueOf(value));
        } else {
            return null;
        }
    }

    @Override
    public StringType clone(State fromState) {
        return new StringType((StringType)fromState);
    }
}
