package com.synapse.ms.ithings.io.transport.socket;

public enum ConnectionState {
    DISCONNECTED,
    CONNECTING,
    CONNECTED
}
