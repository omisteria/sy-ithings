package com.synapse.ms.ithings.io.transport.mqtt;

/**
 * Implement this interface to get notified of new and removed MQTT broker.
 * Register this observer at {@see MqttService}.
 *
 * @author David Graeff - Initial contribution and API
 */
public interface MqttServiceObserver {
    /**
     * Called, if a new broker has been added to the {@see MqttService}.
     * If a broker connection is replaced, you will be notified by a brokerRemoved call,
     * followed by a brokerAdded call.
     *
     * @param brokerID The unique ID withing {@link MqttService}
     * @param broker The new broker connection
     */
    void brokerAdded(String brokerID, MqttBrokerConnection broker);

    /**
     * Called, if a broker has been removed from the {@see MqttService}.
     *
     * @param brokerID The unique ID withing {@link MqttService}
     * @param broker The removed broker connection.
     *            Please unsubscribe from all topics and unregister
     *            all your listeners.
     */
    void brokerRemoved(String brokerID, MqttBrokerConnection broker);
}
