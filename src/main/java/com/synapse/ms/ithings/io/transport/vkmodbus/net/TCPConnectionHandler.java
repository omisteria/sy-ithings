package com.synapse.ms.ithings.io.transport.vkmodbus.net;

import com.synapse.ms.ithings.io.transport.vkmodbus.Modbus;
import com.synapse.ms.ithings.io.transport.vkmodbus.ModbusCoupler;
import com.synapse.ms.ithings.io.transport.vkmodbus.ModbusIOException;
import com.synapse.ms.ithings.io.transport.vkmodbus.io.ModbusTransport;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.ModbusRequest;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.ModbusResponse;

/**
 * Class implementing a handler for incoming Modbus/TCP requests.
 *
 * @author Dieter Wimberger
 * @version 1.2rc1 (09/11/2004)
 */
public class TCPConnectionHandler implements Runnable {

  private TCPSlaveConnection m_Connection;
  private ModbusTransport m_Transport;

  /**
   * Constructs a new <tt>TCPConnectionHandler</tt> instance.
   *
   * @param con an incoming connection.
   */
  public TCPConnectionHandler(TCPSlaveConnection con) {
    setConnection(con);
  }//constructor

  /**
   * Sets a connection to be handled by this <tt>
   * TCPConnectionHandler</tt>.
   *
   * @param con a <tt>TCPSlaveConnection</tt>.
   */
  public void setConnection(TCPSlaveConnection con) {
    m_Connection = con;
    m_Transport = m_Connection.getModbusTransport();
  }//setConnection

  public void run() {
    try {
      do {
        //1. read the request
        ModbusRequest request = m_Transport.readRequest();
        //System.out.println("Request:" + request.getHexMessage());
        ModbusResponse response = null;

        //test if Process image exists
        if (ModbusCoupler.getReference().getProcessImage() == null) {
          response =
              request.createExceptionResponse(Modbus.ILLEGAL_FUNCTION_EXCEPTION);
        } else {
          response = request.createResponse();
        }
        /*DEBUG*/
        if (Modbus.debug) System.out.println("Request:" + request.getHexMessage());
        if (Modbus.debug) System.out.println("Response:" + response.getHexMessage());

        //System.out.println("Response:" + response.getHexMessage());
        m_Transport.writeMessage(response);
      } while (true);
    } catch (ModbusIOException ex) {
      if (!ex.isEOF()) {
        //other troubles, output for debug
        ex.printStackTrace();
      }
    } finally {
      try {
        m_Connection.close();
      } catch (Exception ex) {
        //ignore
      }

    }
  }//run

}//TCPConnectionHandler

