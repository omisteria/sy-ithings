package com.synapse.ms.ithings.io.transport.vkmodbus.net;

import com.synapse.ms.ithings.io.transport.vkmodbus.io.ModbusTCPVKMTransport;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TCPVKMMasterConnection extends TCPMasterConnection {

    /**
     * Constructs a <tt>TCPMasterConnection</tt> instance
     * with a given destination address.
     *
     * @param adr the destination <tt>InetAddress</tt>.
     */
    public TCPVKMMasterConnection(InetAddress adr) {
        super(adr);
    }

    public TCPVKMMasterConnection(InetSocketAddress adr) {
        super(adr.getAddress());
        setPort(adr.getPort());
    }

    protected void prepareTransport(Socket socket) throws IOException {
        if (m_ModbusTransport == null) {
            m_ModbusTransport = new ModbusTCPVKMTransport(socket);
        } else {
            m_ModbusTransport.setSocket(socket);
        }
    }

}
