//License
/***
 * Java Modbus Library (jamod)
 * Copyright (c) 2002-2004, jamod development team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the author nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS
 * IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ***/
package com.synapse.ms.ithings.io.transport.vkmodbus.tests;

import java.net.InetAddress;

import com.synapse.ms.ithings.io.transport.vkmodbus.io.ModbusTCPTransaction;
import com.synapse.ms.ithings.io.transport.vkmodbus.io.ModbusTransaction;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.ModbusRequest;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.ReadInputDiscretesRequest;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.ReadInputRegistersRequest;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.ReadInputRegistersResponse;
import com.synapse.ms.ithings.io.transport.vkmodbus.net.TCPMasterConnection;
import com.synapse.ms.ithings.io.transport.vkmodbus.Modbus;
import com.synapse.ms.ithings.io.transport.vkmodbus.net.TCPVKMMasterConnection;

/**
 * Class that implements a simple connect
 *
 * @author Dieter Wimberger
 * @version 1.2rc1 (09/11/2004)
 */
public class ReadDataFromSMD220Test {

  public static void main(String[] args) {

    InetAddress addr = null;
    TCPMasterConnection con = null;
    ModbusRequest req = null;
    ModbusTransaction trans = null;
    int repeat = 1;
    int port = 9761;//Modbus.DEFAULT_PORT;

    try {

        addr = InetAddress.getByName("192.168.3.170");

      //2. Open the connection
      con = new TCPVKMMasterConnection(addr);
      con.setPort(port);
      con.connect();

      if (Modbus.debug) System.out.println("Connected to " + addr.toString() + ":" + con.getPort());

      //3. Prepare the request
      req = buildReqToSMD220();
      if (Modbus.debug) System.out.println("Request: " + req.getHexMessage());



      //4. Prepare the transaction
      trans = new ModbusTCPTransaction(con);
      trans.setRequest(req);

      //5. Execute the transaction repeat times
      int k = 0;
      do {
        trans.execute();
     
        ReadInputRegistersResponse res = (ReadInputRegistersResponse) trans.getResponse();
        if (Modbus.debug)
          System.out.println("Response: " +
              res.getHexMessage()
          );

        for (int n = 0; n < res.getWordCount(); n++) {
          System.out.println("Word " + n + "=" + res.getRegisterValue(n));
        }

        k++;
      } while (k < repeat);

      //6. Close the connection
      con.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//main

    // Need
    // request  01 04 01 56 00 02 90 27
    // response 01 04 04 46 8E 9E 75 27 60
    // Sent:    01 04 01 56 00 02 90 27
    // Response 01 04 04 46 b8 03 3e ef c9
    public static ModbusRequest buildReqToSMD220() {
        int register = 342; // equals to 01 56
        ModbusRequest req = new ReadInputRegistersRequest(342, 2);
        req.setHeadless();
        req.setUnitID(1);
        return req;
    }

    // << 03 03 00 03 00 08 b5 ee
    // << 03 01 00 00 00 04 3c 2b
    // >> 03 01 01 00 50 30
    public static ModbusRequest buildReqTo4x4Module() {
        ModbusRequest req = new ReadInputDiscretesRequest(0, 4);
        req.setHeadless();
        req.setUnitID(3);
        return req;
    }
}//class AITest
