package com.synapse.ms.ithings.io.transport.socket;

import com.synapse.ms.ithings.core.common.events.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

public class SocketConnection {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String address;
    private int port;
    private int timeout;
    private Socket socket = null;

    private static final int CONNECTION_MAX_RETRY = 10;
    private int connectionErrorCounter = 0;
    private boolean connected;
    private Thread  checkConnectingTread;
    private Thread  connectingTread;
    private Callable<Boolean> checkConnectionFunc;
    private long checkConnectionMillis = 8000;
    private long heartbeatDelayMillis = 5000;
    private int socketTimeoutMillis = 3000;

    private ConnectionState connectionState;

    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private List<ConnectionListener> listeners = new ArrayList<>();

    public SocketConnection(String address, int port) {
        this.address = address;
        this.port = port;
        checkConnectionFunc = () -> {
            try {
                outputStream.write(new byte[] {0x01});
                return true;
            } catch (IOException e) {
                return false;
            }
        };
    }

    public synchronized void connect() {

        if (connectingTread != null && connectingTread.isAlive()) {
            connectingTread.interrupt();
        }

        connectingTread = new Thread(() -> {
            try {
                tryConnect();
            } catch (Exception e) {
                logger.error("Connection problem", e);
            }
        });
        connectingTread.start();

        runCheckConnection();
    }

    public synchronized void close() {
        tryClose();

        connectionState = ConnectionState.DISCONNECTED;
        notifyListeners(listener -> listener.onConnectionChanged(connected));
    }

    private boolean tryConnect() throws IOException {
        try {
            if (connectionState == ConnectionState.DISCONNECTED) {

                logger.info("Connect to: "+this.address+", "+this.port);

                this.socket = new Socket();

                InetSocketAddress inetAddress = new InetSocketAddress(address, port);
                setTimeout(socketTimeoutMillis);

                this.socket.connect(inetAddress, socketTimeoutMillis);

                connectionState = ConnectionState.CONNECTED;
                connected = true;
                logger.info("Successfully connected");

                notifyListeners(listener -> listener.onConnectionChanged(connected));

                inputStream = new DataInputStream(socket.getInputStream());
                if (inputStream == null) {
                    logger.warn("could not get input stream after opening connection");
                    closeOnError();
                    return false;
                }

                outputStream = new DataOutputStream(socket.getOutputStream());
                if (outputStream == null) {
                    logger.warn("could not get output stream after opening connection");
                    closeOnError();
                    return false;
                }

                try
                {
                    while(connected) {
                        int bytesRead = 0;
                        byte[] inStream = new byte[1025];

                        try {
                            bytesRead = inputStream.read(inStream);
                        } catch (IOException e) {
                            logger.warn("Can't read from inputStream");
                        }

                    }
                }
                catch (Exception e)
                {
                    logger.error("Error while reading from socket: " + e);
                    throw new IOException("I/O exception - failed to read.\n" + e);
                } finally {
                    tryClose();
                }
                return true;

            } else if (connectionState == ConnectionState.CONNECTED) {
                // socket already open, clear input buffer
                logger.trace("Connection already open, skipping input buffer");
                while (inputStream != null) {
                    if (inputStream.skip(inputStream.available()) == 0) {
                        return true;
                    }
                }
                logger.debug("input stream not available on skipping");
                closeOnError();
                return false;

            } else {
                return false;
            }

        } catch (IOException e) {
            logger.debug("could not open Connection to {}:{}: {}", address, port, e.getMessage());
            closeOnError();
            return false;
        }
    }

    private void tryClose() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                logger.warn("could not close connection: {}", e.getMessage());
            }
        }

        socket = null;
        inputStream = null;
        outputStream = null;
    }

    private void runCheckConnection() {

        if (checkConnectingTread != null && checkConnectingTread.isAlive()) {
            checkConnectingTread.interrupt();
        }

        checkConnectingTread = new Thread(() -> {

            try {
                Thread.sleep(checkConnectionMillis + heartbeatDelayMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            while(!Thread.interrupted()) {

                try {
                    Thread.sleep(checkConnectionMillis);
                } catch (InterruptedException e) {
                    // empty
                }

                try {

                    if (!checkConnection()) {
                        throw new IllegalStateException("Brocken connection");
                    }
                    Thread.sleep(heartbeatDelayMillis);

                } catch (InterruptedException e) {
                    // empty
                } catch (Exception e) {
                    try {
                        connected = false;
                        tryConnect();
                    } catch (Exception e1) {
                        logger.error("Connection problem", e1);
                    }
                }

            }
        });
        checkConnectingTread.start();
    }

    public void setCheckConnectionFunc(Callable<Boolean> checkConnectionFunc) {
        this.checkConnectionFunc = checkConnectionFunc;
    }

    public boolean isConnected() {
        return connectionState.equals(ConnectionState.CONNECTED);
    }

    public Socket getSocket() {
        return socket;
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }

    public boolean checkConnection() throws Exception {
        if (checkConnectionFunc == null) return false;
        return checkConnectionFunc.call();
    }

    public ConnectionListener addListener(ConnectionListener listener) {
        listeners.add(listener);
        return listener;
    }

    public void removeListener(ConnectionListener listener) {
        listeners.remove(listener);
    }

    public synchronized void notifyListeners(Consumer<? super ConnectionListener> algorithm) {
        this.listeners.forEach(listener -> algorithm.accept(listener));
    }

    private void setTimeout(int timeout) {
        this.socketTimeoutMillis = timeout;
        if (this.socket != null) {
            try {
                this.socket.setSoTimeout(socketTimeoutMillis);
            } catch (IOException ex) {
                logger.warn("Can't set socket timeout");
            }
        }
    }

    private void closeOnError() {
        connectionErrorCounter++;
        tryClose();

        connectionState = ConnectionState.CONNECTING;

        if (connectionErrorCounter > CONNECTION_MAX_RETRY) {
            connectionState = ConnectionState.DISCONNECTED;
        }

        notifyListeners(listener -> listener.onConnectionChanged(connected));
    }
}
