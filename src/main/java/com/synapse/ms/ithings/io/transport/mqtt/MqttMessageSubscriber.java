package com.synapse.ms.ithings.io.transport.mqtt;

/**
 * Implement this interface and register on the {@see MqttBrokerConnection} to get notified
 * of incoming Mqtt messages on the given topic.
 *
 * @author David Graeff
 */
public interface MqttMessageSubscriber {
    /**
     * Process a received MQTT message.
     *
     * @param topic The mqtt topic on which the message was received.
     * @param payload content of the message.
     */
    public void processMessage(String topic, byte[] payload);

    /**
     * @return topic to subscribe to. May contain + or # wildcards
     */
    public String getTopic();
}
