package com.synapse.ms.ithings.io.transport;

public class TransportUtils {

    public static String toHexString(byte[] message) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i<message.length; i++) {
            if (i>0) sb.append(" ");
            sb.append(String.format("%02X", message[i]));
        }

        return sb.toString();
    }

    public static int convertFromChannelId(String channelId) {
        if (channelId.length()>1 && channelId.contains(".")) {
            String[] p = channelId.split("\\.");
            channelId = p[p.length-1];
        }
        return Integer.parseInt(channelId);
    }

    public static String convertToChannelId(int relayIndex) {
        return String.valueOf(relayIndex);
    }
}
