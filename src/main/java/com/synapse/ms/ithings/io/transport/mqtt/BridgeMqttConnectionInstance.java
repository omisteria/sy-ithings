package com.synapse.ms.ithings.io.transport.mqtt;

import com.synapse.ms.ithings.core.config.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BridgeMqttConnectionInstance {
    private final Logger logger = LoggerFactory.getLogger(BridgeMqttConnectionInstance.class);
    private MqttBrokerConnection connection;
    private MqttService mqttService;

    public void setMqttService(MqttService service) {
        mqttService = service;
    }

    public void unsetMqttService(MqttService service) {
        mqttService = null;
    }

    /**
     * Create broker connections based on the service configuration. This will disconnect and
     * discard all existing textual configured brokers.
     */
    public void modified(MqttBrokerConnectionConfig config, MqttConnectionObserver observer) {
        if (connection != null) {
            connection.stop();
        }

        activate(config, observer);
    }

    public void activate(MqttBrokerConnectionConfig config, MqttConnectionObserver observer) {
        logger.debug("MQTT Broker connection service started...");

        if (config == null || mqttService == null) {
            return;
        }
        final MqttService service = (MqttService) mqttService;

        try {
            // Compute brokerID and make sure it is not empty
            String brokerID = config.getBrokerID();
            if (StringUtils.isBlank(brokerID) || brokerID == null) {
                logger.warn("Ignore invalid broker connection configuration: {}", config);
                return;
            }

            // Add connection and make sure it succeeded
            MqttBrokerConnection c = service.addBrokerConnection(brokerID, config);
            connection = c;
            if (c == null) {
                logger.warn("Ignore existing broker connection configuration for: {}", brokerID);
                return;
            }
            c.addConnectionObserver(observer);
            c.start(); // Start connection
        } catch (ConfigurationException | IllegalArgumentException e) {
            logger.warn("MqttBroker connection configuration faulty: {}", e.getMessage());
        } catch (MqttException e) {
            logger.warn("MqttBroker start failed: {}", e.getMessage(), e);
        }
    }

    public void deactivate() {
        if (connection != null) {
            connection.stop();
        }
        connection = null;
    }

}
