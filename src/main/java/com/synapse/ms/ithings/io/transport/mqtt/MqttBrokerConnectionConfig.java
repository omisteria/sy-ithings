package com.synapse.ms.ithings.io.transport.mqtt;

import org.apache.commons.lang3.StringUtils;

/**
 * Contains configuration for a MqttBrokerConnection. Necessary to add a new broker connection the {@link MqttService}.
 *
 * @author David Graeff - Initial contribution and API
 */
public class MqttBrokerConnectionConfig {

    // Optional connection name
    public String name;
    // Connection parameters (host+port+secure)
    public String host;
    public Integer port;

    // Authentication parameters
    public String username;
    public String password;
    public String clientID;
    // MQTT parameters
    public Integer qos = MqttBrokerConnection.DEFAULT_QOS;
    public Boolean retainMessages = false;
    /** Keepalive in seconds */
    public Integer keepAlive;
    // Last will parameters
    public String lwtTopic;
    public String lwtMessage;
    public Integer lwtQos = MqttBrokerConnection.DEFAULT_QOS;
    public Boolean lwtRetain = false;

    /**
     * Return the brokerID of this connection. This is either the name or host:port(:s), for instance "myhost:8080:s".
     * This method will return an empty string, if none of the parameters is set.
     */
    public String getBrokerID() {
        if (StringUtils.isNotBlank(name)) {
            return name;
        } else {
            StringBuffer b = new StringBuffer();
            if (host != null) {
                b.append(host);
            }
            if (port != null) {
                b.append(":");
                b.append(port.toString());
            }
            return b.toString();
        }
    }

    /**
     * Output the name, host, port and secure flag
     */
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        if (name != null) {
            b.append(name);
            b.append(", ");
        }
        if (host != null) {
            b.append(host);
        }
        if (port != null) {
            b.append(":");
            b.append(port.toString());
        }
        return b.toString();
    }
}
