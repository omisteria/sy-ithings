package com.synapse.ms.ithings.io.transport.mqtt;

/**
 * The connection state of a {@link MqttBrokerConnection}.
 *
 * @author David Graeff
 */
public enum MqttConnectionState {
    DISCONNECTED,
    CONNECTING,
    CONNECTED
}
