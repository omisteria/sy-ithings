package com.synapse.ms.ithings.io.transport.mqtt;


/**
 * Implement this to be notified of the success or error of a {@link MqttBrokerConnection}.publish().
 *
 * @author David Graeff - Initial contribution
 */
public interface MqttPublishCallback {
    public void onSuccess(MqttPublishResult result);

    public void onFailure(MqttPublishResult result, Throwable error);
}
