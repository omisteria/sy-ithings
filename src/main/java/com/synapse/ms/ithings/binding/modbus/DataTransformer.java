package com.synapse.ms.ithings.binding.modbus;

public interface DataTransformer {

    byte[] transform(byte[] data);
}
