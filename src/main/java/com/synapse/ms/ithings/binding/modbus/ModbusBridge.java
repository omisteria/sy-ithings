package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.common.events.ConnectionListenerAdapter;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.AbstractBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;

import java.util.Map;

public class ModbusBridge extends AbstractBridge {

    private ModbusConnection connection;

    public ModbusBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {

            @Override
            public void initialize() {
                logger.info("Initialize bridge "+getGid());
                super.initialize();

                Map<String, Object> bridgeConfig = bridge.getDescription();
                String deviceHost = (String) bridgeConfig.get(ModbusConfKey.socketHost);
                Integer devicePort = Integer.valueOf((String)bridgeConfig.get(ModbusConfKey.socketPort));

                try {
                    connection = new ModbusConnection(deviceHost, devicePort);
                    connection.addListener(new ConnectionListenerAdapter() {
                        @Override
                        public void onReceiveData(byte[] message) {
                            onReceivedResponse(message);
                        }

                        @Override
                        public void onConnectionChanged(boolean connected) {
                            logger.info(String.format("Bridge %s is connected", getGid()));
                            getActivateStatus().update(connected ? ActivateStatus.ONLINE : ActivateStatus.OFFLINE);
                        }
                    });
                    connection.connect();
                } catch (Exception e) {
                    logger.error("Problem set device connection", e);
                }
            }
        });
    }

    public ModbusConnection getConnection() {
        return connection;
    }
}
