package com.synapse.ms.ithings.binding.vkmodule.protocol.s4;

import com.synapse.ms.ithings.binding.vkmodule.protocol.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Socket4Protocol extends VkmBaseProtocol implements VkmProtocol {

    private static byte COMMAND_SWITCH_RELAY = 0x22;
    private static byte COMMAND_STATE_ALL_RELAYS = 0x23;

    public static byte[] composeCommandRelayUpdate(Relay relay, boolean state, int time) {
        byte[] pack = new byte[4];
        pack[0] = COMMAND_SWITCH_RELAY;
        pack[1] = relay.getNumber();
        pack[2] = (byte)(state ? 1 : 0); // 1-on, 0-off
        pack[3] = (byte)time; // 0 - always
        return pack;
    }

    public static byte[] composeCommandRelayUpdate(Relay relay, boolean state) {
        return composeCommandRelayUpdate(relay, state,0);
    }

    public static byte[] composeCommandStateAllRelays() {
        byte[] pack = new byte[1];
        pack[0] = COMMAND_STATE_ALL_RELAYS;
        return pack;
    }

    // 22 -> 34
    // 23 -> 35
    // 22 00 01 00 22 01 01 00 22 04 00 00 0F
    // 22 00 01 00 22 01 01 00 23 00 00 00 00 00 00 00 00 22 00 01
    public static List<VkmResponse> parse(byte[] message) {
        List<VkmResponse> result = new ArrayList<>();

        byte[] res = getFirstResponse(message);

        do {

            if (res.length > 0) {

                if (res[0] == COMMAND_SWITCH_RELAY) {
                    result.add(new Resp22(res));

                } else if (res[0] == COMMAND_STATE_ALL_RELAYS) {
                    result.add(new Resp23(res));

                }
            }

            message = Arrays.copyOfRange(message, res.length, message.length);
            res = getFirstResponse(message);

        } while (res.length > 0);

        return result;
    }

    private static byte[] getFirstResponse(byte[] message) {

        if (message.length > 9) {
            if (message[9] == COMMAND_SWITCH_RELAY || message[9] == COMMAND_STATE_ALL_RELAYS) {
                return Arrays.copyOf(message, 9);
            }
        }

        if (message.length > 4) {
            if (message[4] == COMMAND_SWITCH_RELAY || message[4] == COMMAND_STATE_ALL_RELAYS) {
                return Arrays.copyOf(message, 4);
            }
        }

        return message;
    }




    public static class Resp22 implements VkmResponse {

        private RelayState relayState;

    public Resp22(byte[] message) {
            relayState = new RelayState(Relay.valueOf(message[1]), message[2]>0);
        }

        public RelayState getState() {
            return relayState;
        }

        @Override
        public byte getNumber() {
            return relayState.getRelay().getNumber();
        }
    }

    public static class Resp23 implements VkmResponse {

        private RelayState[] relays = new RelayState[8];

        public Resp23(byte[] message) {
            for(int i=0; i<relays.length; i++) {
                relays[i] = new RelayState(Relay.valueOf(i), message[i+1]>0);
            }
        }

        public boolean getRelayState0() {
            return getRelayState(0);
        }
        public boolean getRelayState1() {
            return getRelayState(1);
        }
        public boolean getRelayState2() {
            return getRelayState(2);
        }
        public boolean getRelayState3() {
            return getRelayState(3);
        }
        public boolean getRelayState4() {
            return getRelayState(4);
        }
        public boolean getRelayState5() {
            return getRelayState(5);
        }
        public boolean getRelayState6() {
            return getRelayState(6);
        }
        public boolean getRelayState7() {
            return getRelayState(7);
        }


        public boolean getRelayState(int index) {
            return relays[index].getState();
        }

        public List<RelayState> asList() {
            return Arrays.asList(relays);
        }

        @Override
        public byte getNumber() {
            return -1;
        }
    }
}
