package com.synapse.ms.ithings.binding.vkmodule.protocol;

public abstract class VkmBaseProtocol {

    protected static byte COMMAND_CHECK = 0x01;
    protected static byte COMMAND_REBOOT = 0x02;
    protected static byte COMMAND_FIRMWARE_VERSION = 0x03;
    protected static byte COMMAND_SERIAL_NUMBER = 0x04;

    public static byte[] composeCommandCheck() {
        return composeCommand(COMMAND_CHECK);
    }
    public static byte[] composeCommandReboot() {
        return composeCommand(COMMAND_REBOOT);
    }
    public static byte[] composeCommandFirmwareVersion() {
        return composeCommand(COMMAND_FIRMWARE_VERSION);
    }
    public static byte[] composeCommandSerialNumber() {
        return composeCommand(COMMAND_SERIAL_NUMBER);
    }

    private static byte[] composeCommand(byte comm) {
        byte[] pack = new byte[1];
        pack[0] = comm;
        return pack;
    }
}
