package com.synapse.ms.ithings.binding.sonoff;

import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;

import java.util.Map;

public class SonoffColorBulb  extends AbstractRepresenter {

    public SonoffColorBulb(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);
    }

    @Override
    public void configure() {

    }
}
