package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.io.transport.vkmodbus.util.BitVector;

public class DataBitTransformer implements DataTransformer {

    private int bitNumber = 0;

    public DataBitTransformer(int bitNumber) {
        this.bitNumber = bitNumber;
    }

    @Override
    public byte[] transform(byte[] data) {
        BitVector vector = BitVector.createBitVector(data);
        return new byte[] {vector.getBit(bitNumber) ? (byte)1 : 0};
    }
}
