package com.synapse.ms.ithings.binding.geeklink;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.build.AbstractRepresenterFactory;


public class GeeklinkFactory extends AbstractRepresenterFactory {

    public GeeklinkFactory(Domain domain) {
        super(domain);
    }
}
