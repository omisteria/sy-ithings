package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.model.AbstractDomain;

public class VkModuleDomain extends AbstractDomain {

    public static final String GID = "vkmodule";
    public static final String BRIDGE_CLASS = "com.synapse.ms.ithings.binding.vkmodule.VkModuleBridge";

    public VkModuleDomain() {
        super();
        drivers.put("socket2", "com.synapse.ms.ithings.binding.vkmodule.VkModuleSocket2");
        drivers.put("socket4", "com.synapse.ms.ithings.binding.vkmodule.VkModuleSocket4");
        // drivers.put("eastron-smd220", "com.synapse.ms.ithings.binding.modbus.EastronSmd220");
    }

    @Override
    public RepresenterFactory getRepresenterFactory() {
        return new VkModuleFactory(this);
    }

    @Override
    public String getBridgeClassName() {
        return BRIDGE_CLASS;
    }

    @Override
    public String getGid() {
        return GID;
    }
}
