package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.core.config.ConfKey;

public interface VkModuleConfKey extends ConfKey {

    String socketHost         = "socketHost";
    String socketPort         = "socketPort";

}
