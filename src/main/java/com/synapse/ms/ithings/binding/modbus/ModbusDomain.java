package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.model.AbstractDomain;

public class ModbusDomain extends AbstractDomain {

    public static final String GID = "modbus";

    public ModbusDomain() {
        super();
        drivers.put("generic", "com.synapse.ms.ithings.binding.modbus.GenericRepresenter");
    }

    @Override
    public String getBridgeClassName() {
        return "com.synapse.ms.ithings.binding.modbus.ModbusBridge";
    }

    @Override
    public String getGid() {
        return GID;
    }
}
