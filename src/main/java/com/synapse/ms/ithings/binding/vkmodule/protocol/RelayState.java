package com.synapse.ms.ithings.binding.vkmodule.protocol;

public class RelayState implements ChannelState {

    private Relay relay;
    private boolean state;

    public RelayState(Relay relay, boolean state) {
        this.relay = relay;
        this.state = state;
    }

    public Relay getRelay() {
        return relay;
    }

    @Override
    public Channel getChannel() {
        return relay;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
