package com.synapse.ms.ithings.binding.eventshare;

import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.StringType;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.handler.MqttDataHandler;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;
import com.synapse.ms.ithings.io.transport.mqtt.MqttException;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class GenericRepresenter extends AbstractRepresenter {

    public GenericRepresenter(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);
    }

    @Override
    public void configure() {

        EventShareEventBusListener listeners = new EventShareEventBusListener((EventShareBridge)bridge);
        listeners.subscribeHandlers();

        setDataHandler(new RepresenterDataHandler(bridge,this) {
            @Override
            public void initialize() {
                super.initialize();

                EventShareBridge mqttBridge = (EventShareBridge) bridge;
                /*
                homie/device/node/property
                */

                /**
                 * Change State events
                 */
                /*EventShareEventBusListener listeners = new EventShareEventBusListener(mqttBridge);
                SystemEngineContext.getBus().subscribe(listeners);*/

                try {
                    mqttBridge.subscribe("homie/#", new MqttDataHandler() {
                        @Override
                        public void sendRequest(String toTopic, Command command) throws Exception {
                            // empty
                        }

                        @Override
                        public void onReceivedResponse(String fromTopic, byte[] responseData) {
                            if (!StringUtils.isEmpty(fromTopic) && StringUtils.startsWith(fromTopic, "homie/")) {

                                try {
                                    String[] parts = StringUtils.split(fromTopic, "/");

                                    if (parts.length > 4) {
                                        String channelGid = parts[1] + "." + parts[2] + "." + parts[3];
                                        String command = parts[4];

                                        if ("set".equalsIgnoreCase(command)) {
                                            Channel ch = bridge.getRepresenterManager().getChannelRegistry().get(channelGid);
                                            if (ch != null) {
                                                ch.post(new StringType(new String(responseData)));
                                            }
                                        }
                                    }
                                } catch (Exception pe) {
                                    logger.error("Can't parse topic [" + fromTopic + "]");
                                }

                            }
                        }
                    });
                } catch (MqttException e) {
                    logger.error("Can't subscribe", e);
                }
            }
        });
    }
}
