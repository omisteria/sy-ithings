package com.synapse.ms.ithings.binding.vkmodule.protocol;

public interface VkmResponse {

    /*
    Returns number of internal channel of device
     */
    byte getNumber();
}
