package com.synapse.ms.ithings.binding.vkmodule.protocol;

public class Relay implements Channel {

    public static final Relay RELAY0 = new Relay((byte) 0);
    public static final Relay RELAY1 = new Relay((byte) 1);
    public static final Relay RELAY2 = new Relay((byte) 2);
    public static final Relay RELAY3 = new Relay((byte) 3);
    public static final Relay RELAY4 = new Relay((byte) 4);
    public static final Relay RELAY5 = new Relay((byte) 5);
    public static final Relay RELAY6 = new Relay((byte) 6);
    public static final Relay RELAY7 = new Relay((byte) 7);

    private byte number;

    private Relay(byte number) {
        this.number = number;
    }

    public byte getNumber() {
        return number;
    }

    public static Relay valueOf(int num) {
        Relay[] all = {RELAY0, RELAY1, RELAY2, RELAY3, RELAY4, RELAY5, RELAY6, RELAY7};
        for (int i=0;i<all.length; i++) {
            if (all[i].number == (byte)num) return all[i];
        }
        return null;
    }
}
