package com.synapse.ms.ithings.binding.owserver;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.AbstractBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;
import org.owfs.jowfsclient.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public class OwserverBridge extends AbstractBridge {

    //private OwserverConnection owserverConnection;
    private OwfsConnectionFactory factory;
    private OwfsConnection connection;

    public OwserverBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {
            @Override
            public void initialize() {
                super.initialize();

                String host = (String) description.get(OwserverConfKey.owfsHost);
                int port = new BigDecimal((String) description.get(OwserverConfKey.owfsPort)).intValue();

                //factory = new OwfsConnectionFactory(host, port);
                OwfsConnectionConfig owConnectionConfig = new OwfsConnectionConfig(host, port);
                owConnectionConfig.setTemperatureScale(Enums.OwTemperatureScale.CELSIUS);
                // owConnectionConfig.setPersistence(Enums.OwPersistence.ON);
                owConnectionConfig.setBusReturn(Enums.OwBusReturn.ON);
                //factory.setConnectionConfig(owConnectionConfig);

                // connection = factory.createNewConnection();
                connection = OwfsConnectionFactory.newOwfsClientThreadSafe(owConnectionConfig);

                try {
                    connection.listDirectory("/");
                    getActivateStatus().update(ActivateStatus.ONLINE);
                } catch (Exception e) {
                    getActivateStatus().update(ActivateStatus.OFFLINE);
                    e.printStackTrace();
                }

                /*owserverConnection = new OwserverConnection(st -> {
                    if (st.equals(OwserverConnectionState.OPENED)) {
                        getActivateStatus().update(ActivateStatus.ONLINE);
                    } else {
                        getActivateStatus().update(ActivateStatus.OFFLINE);
                    }
                    return null;
                });*/

                // owserverConnection.setHost((String) description.get(OwserverConfKey.owfsHost));
                // owserverConnection.setPort(new BigDecimal((String) description.get(OwserverConfKey.owfsPort)).intValue());

                //getActivateStatus().update(ActivateStatus.INITIALIZING);

                /*Executors.newSingleThreadExecutor().submit(() -> {
                    owserverConnection.start();
                });*/
            }
        });
    }

    @Override
    public void dispose() throws Exception {
        connection.disconnect();
        super.dispose();
    }


    public byte[] readBytes(String sensorId, OwserverDeviceParameter parameter) throws IOException, OwfsException {
        String res = connection.read(parameter.getPath(sensorId));
        return res.getBytes();
    }

    public void writeBytes(String sensorId, OwserverDeviceParameter parameter, byte[] value) throws IOException, OwfsException {
        connection.write(parameter.getPath(sensorId), new String(value));
    }
}
