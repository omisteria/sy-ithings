package com.synapse.ms.ithings.binding.geeklink;

import com.synapse.ms.ithings.core.common.types.*;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;
import com.synapse.ms.ithings.io.transport.mqtt.MqttException;

import java.util.Map;

public class GeeklinkMini extends AbstractRepresenter {

    public GeeklinkMini(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);

        predefinedChannels.add(new PredefinedChannel("ir", ChannelType.IR, UnDefType.UNDEF));
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                super.initialize();

                getChannels().forEach(channel -> {
                    try {
                        Map<String, Object> desc = channel.getDescription();
                        String stateTopic = (String) desc.get(GeeklinkConfKey.stateTopic);

                        if (stateTopic != null) {
                            GeeklinkBridge geeklinkBridge = (GeeklinkBridge) bridge;
                            geeklinkBridge.subscribe(stateTopic, new DataHandler<byte[], byte[]>() {
                                @Override
                                public void sendRequest(Command command) throws Exception {
                                    dataHandler.sendRequest(command, channel.getGid());
                                }

                                @Override
                                public void onReceivedResponse(byte[] responseData) {
                                    dataHandler.onReceivedResponse(responseData, channel.getGid());
                                }
                            });

                            channel.getActivateStatus().update(ActivateStatus.ONLINE);
                        }
                    } catch (MqttException e) {
                        channel.getActivateStatus().update(ActivateStatus.OFFLINE);
                    }
                });

                getActivateStatus().update(ActivateStatus.ONLINE);
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {

                    try {
                        byte[] data = command.as(RawType.class).getBytes();

                        Channel channel = getChannel(channelId);
                        Map<String, Object> desc = channel.getDescription();
                        String commandTopic = (String) desc.get(GeeklinkConfKey.commandTopic);

                        GeeklinkBridge geeklinkBridge = (GeeklinkBridge) bridge;
                        geeklinkBridge.publish(commandTopic, data);

                    } catch (MqttException e) {
                        // set representer status
                    }
                });
            }

            public void onReceivedResponse(byte[] responseData, String channelId) {
                State newState = new RawType(responseData).as(StringType.class);

                Channel channel = representer.getChannel(channelId);
                if (channel != null) {
                    channel.getState().update(newState);
                }
            }
        });
    }
}
