package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.common.types.BooleanType;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.DecimalType;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;
import com.synapse.ms.ithings.io.transport.vkmodbus.Modbus;
import com.synapse.ms.ithings.io.transport.vkmodbus.procimg.InputRegister;
import com.synapse.ms.ithings.io.transport.vkmodbus.util.BitVector;
import org.apache.commons.lang3.StringUtils;

import java.nio.ByteBuffer;
import java.util.Map;

public class GenericRepresenter extends AbstractRepresenter {

    public GenericRepresenter(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                logger.info("Initialize rep "+getGid());
                super.initialize();
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {
                    try {

                        BitVector vector;
                        ModbusConnection connection = ((ModbusBridge)bridge).getConnection();

                        Integer deviceId = Integer.valueOf((String)description.get(ModbusConfKey.id));

                        Channel channel = getChannel(channelId);
                        if (channel != null) {

                            ModbusData data = null;

                            if (command == null) {

                                String commandState = (String)channel.getDescription().get(ModbusConfKey.commandState);
                                data = ModbusData.parse(commandState);

                            } else {

                                String commandChange = (String)channel.getDescription().get(ModbusConfKey.commandChange);
                                data = ModbusData.parse(commandChange);

                            }

                            if (data != null) {
                                switch (data.getFunc()) {
                                    case Modbus.READ_INPUT_REGISTERS:
                                        InputRegister[] inputRegisters = connection.readInputRegister(deviceId, data.getAddress(), data.getLength());

                                        ByteBuffer buff = ByteBuffer.allocate(data.getLength() * 2);
                                        for (int index = 0; index < data.getLength(); index++) {
                                            buff.put(inputRegisters[index].toBytes());
                                        }

                                        onReceivedResponse(buff.array(), channelId);
                                        break;

                                    case Modbus.READ_INPUT_DISCRETES:
                                        vector = connection.readInputDiscretes(deviceId, data.getAddress(), data.getLength());
                                        onReceivedResponse(vector.getBytes(), channelId);
                                        break;

                                    case Modbus.READ_COILS:
                                        vector = connection.readCoils(deviceId, data.getAddress(), data.getLength());
                                        onReceivedResponse(vector.getBytes(), channelId);
                                        break;

                                    case Modbus.WRITE_COIL:
                                        boolean resultState = connection.writeCoil(deviceId, data.getAddress(), command.as(BooleanType.class).getValue());
                                        onReceivedResponse(resultState ? new byte[]{1} : new byte[]{0}, channelId);
                                        break;

                                    case Modbus.WRITE_SINGLE_REGISTER:
                                        int result = connection.writeSingleRegister(deviceId, data.getAddress(),
                                                command.as(DecimalType.class).intValue());
                                        byte[] bytes = new byte[2];
                                        byte[] resbytes = ByteBuffer.allocate(4).putInt(result).array();
                                        bytes[0] = resbytes[2];
                                        bytes[1] = resbytes[3];
                                        onReceivedResponse(bytes, channelId);
                                        break;
                                }
                            }

                        }

                    } catch (Exception e) {
                        logger.error("Problem send request to device", e);
                    }
                });
            }

            /**
             * Handler for incoming data for the device
             * @param responseData
             */
            @Override
            public void onReceivedResponse(byte[] responseData, String channelId) {

                try {
                    // logger.debug(String.format("Receive data %s", ModbusUtils.toHexString(responseData)));

                    Channel channel = getChannel(channelId);
                    if (channel != null) {

                        DataTransformer dataTransformer = resolveDataTransformer((String)channel.getDescription().get(ModbusConfKey.transform));

                        if (channel.getType().equals(ChannelType.NUMBER)) { // TODO: change to FLOAT
                            float val = ByteBuffer.wrap(responseData).getFloat();

                            if (channel.getDescription().get(ModbusConfKey.offset) != null) {
                                Double offset = (Double) channel.getDescription().get(ModbusConfKey.offset);
                                val = (float) (val + offset);
                            }

                            DecimalType newState = new DecimalType(val);
                            channel.getState().update(newState);
                        }

                        if (channel.getType().equals(ChannelType.NUM_INT)) {
                            //System.out.println(responseData);
                            int val = ByteBuffer.wrap(responseData).getInt();

                            if (channel.getDescription().get(ModbusConfKey.offset) != null) {
                                Double offset = (Double) channel.getDescription().get(ModbusConfKey.offset);
                                val = (int) (val + offset);
                            }

                            DecimalType newState = new DecimalType(val);
                            channel.getState().update(newState);
                        }

                        if (channel.getType().equals(ChannelType.NUM_SHORT)) {
                            short val = ByteBuffer.wrap(responseData).getShort();
                            DecimalType newState = new DecimalType(val);
                            channel.getState().update(newState);
                        }

                        if (channel.getType().equals(ChannelType.SWITCH)) {
                            BooleanType newState = new RawType(responseData).as(BooleanType.class);
                            channel.getState().update(newState);
                        }

                        else if (channel.getType().equals(ChannelType.DRY_CONTACT)) {

                            String commandState = (String)channel.getDescription().get(ModbusConfKey.commandState);
                            ModbusData data = ModbusData.parse(commandState);

                            if (data.getFunc() == Modbus.READ_INPUT_DISCRETES) {
                                if (dataTransformer != null) {
                                    responseData = dataTransformer.transform(responseData);
                                }

                                BooleanType newState = new RawType(responseData).as(BooleanType.class);
                                channel.getState().update(newState);
                            }
                        }

                    }
                } catch (Exception e) {
                    logger.error("Problem parse response", e);
                }
            }

        });
    }

    public DataTransformer resolveDataTransformer(String str) {
        if (StringUtils.isNoneEmpty(str)) {
            String[] p = str.split("\\.");
            if ("bit".equals(p[0])) {
                int index = Integer.parseInt(p[1]);
                return new DataBitTransformer(index);
            }
        }
        return null;
    }
}
