package com.synapse.ms.ithings.binding.owserver;

import com.synapse.ms.ithings.core.config.ConfKey;

public interface OwserverConfKey extends ConfKey {

    String owfsHost           = "owfsHost";
    String owfsPort           = "owfsPort";
    String family             = "family";
    String scale             = "scale";
    String precision       = "precision";

}
