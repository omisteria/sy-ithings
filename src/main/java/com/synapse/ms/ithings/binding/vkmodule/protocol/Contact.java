package com.synapse.ms.ithings.binding.vkmodule.protocol;

public class Contact implements Channel {

    public static final Contact CONTACT0 = new Contact((byte) 0);
    public static final Contact CONTACT1 = new Contact((byte) 1);
    public static final Contact CONTACT2 = new Contact((byte) 2);
    public static final Contact CONTACT3 = new Contact((byte) 3);

    private byte number;

    private Contact(byte number) {
        this.number = number;
    }

    public byte getNumber() {
        return number;
    }

    public static Contact valueOf(int num) {
        Contact[] all = {CONTACT0, CONTACT1, CONTACT2, CONTACT3};
        for (int i=0;i<all.length; i++) {
            if (all[i].number == (byte)num) return all[i];
        }
        return null;
    }
}
