package com.synapse.ms.ithings.binding.vkmodule.protocol.s2;

import com.synapse.ms.ithings.binding.vkmodule.protocol.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Socket2Protocol extends VkmBaseProtocol implements VkmProtocol {

    private static byte COMMAND_CONFIG_CONTACT = 0x20;
    private static byte COMMAND_STATE_CONTACT = 0x21;
    private static byte COMMAND_SWITCH_RELAY = 0x22;
    private static byte COMMAND_STATE_ALL_CONTACTS_AND_RELAYS = 0x23;

    public static byte[] composeCommandAllState() {
        byte[] pack = new byte[1];
        pack[0] = COMMAND_STATE_ALL_CONTACTS_AND_RELAYS;
        return pack;
    }

    public static byte[] composeCommandContactState(Contact contact) {
        byte[] pack = new byte[2];
        pack[0] = COMMAND_STATE_CONTACT;
        pack[1] = contact.getNumber();
        return pack;
    }

    public static byte[] composeCommandRelayUpdate(Relay relay, boolean state, int time) {
        byte[] pack = new byte[4];
        pack[0] = COMMAND_SWITCH_RELAY;
        pack[1] = relay.getNumber();
        pack[2] = (byte)(state ? 1 : 0); // 1-on, 0-off
        pack[3] = (byte)time; // 0 - always
        return pack;
    }

    public static byte[] composeCommandRelayUpdate(Relay relay, boolean state) {
        return composeCommandRelayUpdate(relay, state, 0);
    }

    // 22 -> 34
    // 23 -> 35
    // 22 00 01 00 22 01 01 00 22 04 00 00 0F
    // 22 00 01 00 22 01 01 00 23 00 00 00 00 22 00 01
    public static List<VkmResponse> parse(byte[] message) {
        List<VkmResponse> result = new ArrayList<>();

        byte[] res = getFirstResponse(message);

        do {

            if (res.length > 0) {

                if (res[0] == COMMAND_CONFIG_CONTACT) {
                    result.add(new Resp20(res));

                } else if (res[0] == COMMAND_STATE_CONTACT) {
                    result.add(new Resp21(res));

                } else if (res[0] == COMMAND_SWITCH_RELAY) {
                    result.add(new Resp22(res));

                } else if (res[0] == COMMAND_STATE_ALL_CONTACTS_AND_RELAYS) {
                    result.add(new Resp23(res));

                }
            }

            message = Arrays.copyOfRange(message, res.length, message.length);
            res = getFirstResponse(message);

        } while (res.length > 0);

        return result;
    }

    private static byte[] getFirstResponse(byte[] message) {

        if (message.length > 5) {
            if (message[5] == COMMAND_SWITCH_RELAY || message[5] == COMMAND_STATE_ALL_CONTACTS_AND_RELAYS) {
                return Arrays.copyOf(message, 5);
            }
        }

        if (message.length > 4) {
            if (message[4] == COMMAND_SWITCH_RELAY || message[4] == COMMAND_STATE_ALL_CONTACTS_AND_RELAYS) {
                return Arrays.copyOf(message, 4);
            }
        }

        return message;
    }



    public static class Resp20 implements VkmResponse {

        private ContactState contactState;

        public Resp20(byte[] message) {
            contactState = new ContactState(Contact.valueOf(message[1]), message[2]>0);
        }

        public ContactState getState() {
            return contactState;
        }

        public byte getNumber() {
            return contactState.getContact().getNumber();
        }
    }

    public static class Resp21 implements VkmResponse {

        private ContactState contactState;

        public Resp21(byte[] message) {
            contactState = new ContactState(Contact.valueOf(message[1]), message[2]>0);
        }

        public ContactState getState() {
            return contactState;
        }

        public byte getNumber() {
            return contactState.getContact().getNumber();
        }
    }

    public static class Resp22 implements VkmResponse {

        private RelayState relayState;

        public Resp22(byte[] message) {
            relayState = new RelayState(Relay.valueOf(message[1]), message[2]>0);
        }

        public RelayState getState() {
            return relayState;
        }

        public byte getNumber() {
            return relayState.getRelay().getNumber();
        }
    }

    public static class Resp23 implements VkmResponse {

        private ContactState[] contacts = new ContactState[2];
        private RelayState[] relays = new RelayState[2];

        public Resp23(byte[] message) {
            for(int i=0; i<contacts.length; i++) {
                contacts[i] = new ContactState(Contact.valueOf(i), message[i+1] == 0); //relay
            }
            for(int i=0; i<relays.length; i++) {
                relays[i] = new RelayState(Relay.valueOf(i), message[i+2+1]>0);
            }
        }

        public boolean getContactState0() {
            return getContactState(0);
        }
        public boolean getContactState1() {
            return getContactState(0);
        }
        public boolean getRelayState0() {
            return getRelayState(0);
        }
        public boolean getRelayState1() {
            return getRelayState(1);
        }

        public boolean getContactState(int index) {
            return contacts[index].getState();
        }
        public boolean getRelayState(int index) {
            return relays[index].getState();
        }

        public List<ContactState> contactsAsList() {
            return Arrays.asList(contacts);
        }
        public List<RelayState> relaysAsList() {
            return Arrays.asList(relays);
        }

        public byte getNumber() {
            return -1;
        }
    }
}
