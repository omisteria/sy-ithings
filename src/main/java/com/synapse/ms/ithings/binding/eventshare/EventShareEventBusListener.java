package com.synapse.ms.ithings.binding.eventshare;

import com.synapse.ms.ithings.core.common.events.BridgeActivateStatusEvent;
import com.synapse.ms.ithings.core.common.events.RepresenterActivateStatusEvent;
import com.synapse.ms.ithings.core.common.events.bus.BridgeActivateStatusEventListener;
import com.synapse.ms.ithings.core.common.events.bus.RepresenterActivateStatusEventListener;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.io.transport.mqtt.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

public class EventShareEventBusListener {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private EventShareBridge eventShareBridge;

    public EventShareEventBusListener(EventShareBridge eventShareBridge) {
        this.eventShareBridge = eventShareBridge;
    }

    public void subscribeHandlers() {

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                try {
                    String homiePath = "homie/" + event.getChannel().getGid().replaceAll("\\.", "/");
                    // logger.info(">>> MQTT Publish: topic="+homiePath);
                    eventShareBridge.publish(homiePath, event.getState().toString().getBytes());
                } catch (MqttException e) {
                    logger.error("Can't publish to Homie bus", e);
                }
            }
        });

        SystemEngineContext.getBus().subscribe(new BridgeActivateStatusEventListener() {
            @Override
            public void handle(BridgeActivateStatusEvent event) {
                try {

                    Bridge bridge = event.getSource();
                    String bPath = "homie/" + bridge.getGid().replaceAll("\\.", "/");

                    // logger.info(">>> MQTT Publish: topic="+bPath);
                    eventShareBridge.publish(bPath + "/$state", bridge.getActivateStatus().getValue().toString().getBytes());

                } catch (MqttException e) {
                    logger.error("Can't publish to Homie bus", e);
                }
            }
        });

        SystemEngineContext.getBus().subscribe(new RepresenterActivateStatusEventListener() {
            @Override
            public void handle(RepresenterActivateStatusEvent event) {
                try {

                   Representer rep = event.getSource();
                   String bPath = "homie/" + rep.getGid().replaceAll("\\.", "/");

                   // logger.info(">>> MQTT Publish: topic=" + bPath);
                   eventShareBridge.publish(bPath + "/$name", rep.getGid().getBytes());

                   String properties = rep.getChannels().stream().map(ch -> {
                       String[] p = ch.getGid().split("\\.");
                       return p[p.length - 1];
                   }).collect(Collectors.joining(","));

                   eventShareBridge.publish(bPath + "/$properties", properties.getBytes());

                } catch (MqttException e) {
                   logger.error("Can't publish to Homie bus", e);
                }
            }
        });
    }

    /*@Handler
    public void handle(StateEventBusEvent event) {
        try {
            String homiePath = "homie/" + event.getChannel().getGid().replaceAll("\\.", "/");
            logger.info(">>> MQTT Publish: topic="+homiePath);
            eventShareBridge.publish(homiePath, event.getState().toString().getBytes());
        } catch (MqttException e) {
            logger.error("Can't publish to Homie bus", e);
        }
    }*/

/*    // @Handler(delivery = Invoke.Asynchronously)
    @Handler
    public void handle(BridgeActivateStatusEvent event) {

    }

    @Handler
    public void handle(RepresenterActivateStatusEvent event) {

    }*/
}
