package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.binding.vkmodule.protocol.*;
import com.synapse.ms.ithings.binding.vkmodule.protocol.s2.Socket2Protocol;
import com.synapse.ms.ithings.core.common.events.ConnectionListenerAdapter;
import com.synapse.ms.ithings.core.common.types.BooleanType;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class VkModuleSocket2 extends AbstractRepresenter {

    private VkModuleConnection connection;
    private final ExecutorService executorService = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(1000));


    public VkModuleSocket2(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);

        predefinedChannels.add(new PredefinedChannel("contact0", ChannelType.DRY_CONTACT, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("contact1", ChannelType.DRY_CONTACT, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("relay0", ChannelType.RELAY, OnOffType.OFF));
        predefinedChannels.add(new PredefinedChannel("relay1", ChannelType.RELAY, OnOffType.OFF));
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                super.initialize();

                try {
                    String deviceHost = (String) description.get(VkModuleConfKey.socketHost);
                    Integer devicePort = Integer.valueOf((String) description.get(VkModuleConfKey.socketPort));

                    connection = new VkModuleConnection(deviceHost, devicePort);
                    connection.setTimeout(2000);
                    connection.addListener(new ConnectionListenerAdapter() {
                        @Override
                        public void onReceiveData(byte[] message) {
                            onReceivedResponse(message);
                        }

                        @Override
                        public void onConnectionChanged(boolean connected) {
                            logger.info(String.format("Device %s is connected", getGid()));
                            getActivateStatus().update(connected ? ActivateStatus.ONLINE : ActivateStatus.OFFLINE);
                        }
                    });
                    connection.connect();
                } catch (Exception e) {
                    logger.error("Problem set device connection", e);
                }
            }

            @Override
            public void sendRequest(Command command) throws Exception {
                RequestExecutor.doSubmit(() -> {
                    try {

                        if (command == null) {
                            byte[] message = Socket2Protocol.composeCommandAllState();
                            sendCommand(message);
                        }

                    } catch (Exception e) {
                        logger.error("Problem send request to device", e);
                    }
                });
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {
                    try {
                        Channel channel = getChannel(channelId);
                        if (channel != null) {
                            if (channel.getType().equals(ChannelType.RELAY)) {
                                int relayId = Integer.parseInt((String)channel.getDescription().get(VkModuleConfKey.id));
                                Relay relay = Relay.valueOf(relayId);

                                boolean val = command.as(BooleanType.class).getValue();
                                byte[] message = Socket2Protocol.composeCommandRelayUpdate(relay, val);

                                sendCommand(message);
                            }

                            if (channel.getType().equals(ChannelType.DRY_CONTACT)) {
                                int contactId = Integer.parseInt((String)channel.getDescription().get(VkModuleConfKey.id));
                                Contact contact = Contact.valueOf(contactId);

                                byte[] message = Socket2Protocol.composeCommandContactState(contact);
                                sendCommand(message);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Problem send request to device", e);
                    }
                });
            }

            /**
             * Handler for incoming data for the device
             * @param responseData
             */
            @Override
            public void onReceivedResponse(byte[] responseData) {

                // logger.info(String.format("Receive data %s", TransportUtils.toHexString(responseData)));

                List<VkmResponse> responses = Socket2Protocol.parse(responseData);
                responses.forEach(resPart -> {

                    /*if (resPart instanceof Socket2Protocol.Resp21) {
                        Optional<Channel> channelOpt = findChannel(resPart);
                        if (channelOpt.isPresent()) {

                            Channel channel = channelOpt.get();
                            if (!channel.getType().equals(ChannelType.DRY_CONTACT)) return;

                            Socket2Protocol.Resp21 res = (Socket2Protocol.Resp21) resPart;
                            ContactState contactState = res.getState();
                            OnOffType newState = contactState.getState() ? OnOffType.ON : OnOffType.OFF;
                            channel.getState().update(newState);
                        }
                    }*/

                    if (resPart instanceof Socket2Protocol.Resp22) {
                        Optional<Channel> channelOpt = findChannel(((Socket2Protocol.Resp22) resPart).getState());
                        if (channelOpt.isPresent()) {

                            Channel channel = channelOpt.get();
                            if (!channel.getType().equals(ChannelType.RELAY)) return;

                            Socket2Protocol.Resp22 res = (Socket2Protocol.Resp22) resPart;
                            RelayState relayState = res.getState();
                            OnOffType newState = relayState.getState() ? OnOffType.ON : OnOffType.OFF;
                            channel.getState().update(newState);
                        }
                    }

                    if (resPart instanceof Socket2Protocol.Resp23) {
                        Socket2Protocol.Resp23 res = (Socket2Protocol.Resp23) resPart;
                        res.contactsAsList().forEach(cs -> {
                            Optional<Channel> channelOpt = findChannel(cs);
                            if (channelOpt.isPresent()) {
                                Channel channel = channelOpt.get();
                                OnOffType newState = cs.getState() ? OnOffType.ON : OnOffType.OFF;
                                channel.getState().update(newState);
                            }
                        });
                        res.relaysAsList().forEach(rs -> {
                            Optional<Channel> channelOpt = findChannel(rs);
                            if (channelOpt.isPresent()) {
                                Channel channel = channelOpt.get();
                                OnOffType newState = rs.getState() ? OnOffType.ON : OnOffType.OFF;
                                channel.getState().update(newState);
                            }
                        });
                    }
                });
            }

        });
    }

    private void sendCommand(byte[] message) {
        Runnable task1 = () -> {
            try {

                // logger.info(String.format("Send channel[%s] command %s", channelId, TransportUtils.toHexString(message)));
                connection.sendMessage(message);

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    throw new IllegalStateException("Shouldn't be interrupted", e);
                }

            } catch (Exception e) {
                throw new IllegalThreadStateException("Can't write to output stream");
            }
        };

        executorService.submit(task1);
    }

    private Optional<Channel> findChannel(ChannelState deviceChannelState) {
        com.synapse.ms.ithings.binding.vkmodule.protocol.Channel ch = deviceChannelState.getChannel();
        String numberOfChannel = String.valueOf(ch.getNumber());
        return getChannels().stream()
                .filter(c -> {
                    if (c.getType().equals(ChannelType.RELAY)) return ch instanceof Relay;
                    if (c.getType().equals(ChannelType.DRY_CONTACT)) return ch instanceof Contact;
                    return false;
                })
                .filter(c -> numberOfChannel.equals(c.getDescription().get(VkModuleConfKey.id)))
                .findFirst();
    }
}
