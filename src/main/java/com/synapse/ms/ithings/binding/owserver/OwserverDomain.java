package com.synapse.ms.ithings.binding.owserver;

import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.model.AbstractDomain;

/**
 * https://github.com/pakerfeldt/jowfsclient
 */
public class OwserverDomain extends AbstractDomain {

    public static final String GID = "owserver";

    public OwserverDomain() {
        super();
        drivers.put("ds2413", "com.synapse.ms.ithings.binding.owserver.rep.OwserverDS2413");
        drivers.put("ds18b20", "com.synapse.ms.ithings.binding.owserver.rep.OwserverDS18B20");
        drivers.put("ds2438", "com.synapse.ms.ithings.binding.owserver.rep.OwserverDS2438");
    }

    @Override
    public RepresenterFactory getRepresenterFactory() {
        return new OwserverFactory(this);
    }

    @Override
    public String getBridgeClassName() {
        return "com.synapse.ms.ithings.binding.owserver.OwserverBridge";
    }

    @Override
    public String getGid() {
        return GID;
    }
}
