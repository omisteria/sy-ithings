package com.synapse.ms.ithings.binding.vkmodule.protocol;

public class ContactState implements ChannelState {

    private Contact contact;
    private boolean state;

    public ContactState(Contact contact, boolean state) {
        this.contact = contact;
        this.state = state;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public Channel getChannel() {
        return contact;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
