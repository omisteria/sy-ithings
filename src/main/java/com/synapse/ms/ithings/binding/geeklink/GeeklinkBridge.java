package com.synapse.ms.ithings.binding.geeklink;

import com.synapse.ms.ithings.core.model.bridge.BaseMqttBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class GeeklinkBridge extends BaseMqttBridge {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public GeeklinkBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {
            @Override
            public void initialize() {
                super.initialize();

                activateMqttBrokerConnection();
            }
        });
    }
}
