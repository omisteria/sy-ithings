package com.synapse.ms.ithings.binding.vkmodule.protocol;

import com.synapse.ms.ithings.binding.vkmodule.protocol.s4.Socket4Protocol;
import com.synapse.ms.ithings.io.transport.TransportUtils;

import java.util.List;

public class VkmProtocolTest {

    // Soket2

    public byte[] commandInputState(int input) {
        byte[] pack = new byte[2];
        pack[0] = 0x21;

        pack[1] = (byte)input;

        return pack;
    }
    public void parseInputState(byte[] message) {
        if (message[0] == 0x21) { // or 0x20 ?
            int input = (int) message[1];
            int state = (int) message[2];
        }
    }

    // Soket4



    public static void main(String[] args) {

        byte[] res = Socket4Protocol.composeCommandRelayUpdate(Relay.RELAY0, true);

        System.out.println(0x14);
        System.out.println((byte)20);
        System.out.println(20);
        System.out.println(Integer.toHexString(20));

        System.out.println(TransportUtils.toHexString(res));

        byte b = -1;
        System.out.println(Integer.toHexString(b));
        System.out.println(Integer.toHexString(b & 0xFF));

        byte[] message = {0x23, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00};
        System.out.println(TransportUtils.toHexString(message));

        List<VkmResponse> pres = Socket4Protocol.parse(message);
        //System.out.println(((Resp23)pres.get(0)).getRelayState4());
        //System.out.println(((Resp23)pres.get(0)).getRelayState3());

    }
}
