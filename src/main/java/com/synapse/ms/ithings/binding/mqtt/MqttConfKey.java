package com.synapse.ms.ithings.binding.mqtt;

import com.synapse.ms.ithings.core.config.ConfKey;

public interface MqttConfKey extends ConfKey {

    String mqttHost = "mqttHost";
    String mqttPort = "mqttPort";
    String mapFrom = "mapFrom";
}
