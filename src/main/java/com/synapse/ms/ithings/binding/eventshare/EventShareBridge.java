package com.synapse.ms.ithings.binding.eventshare;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.BaseMqttBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;

import java.util.Map;

public class EventShareBridge extends BaseMqttBridge {

    public EventShareBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {
            @Override
            public void initialize() {
                super.initialize();

                activateMqttBrokerConnection();
            }
        });
    }
}
