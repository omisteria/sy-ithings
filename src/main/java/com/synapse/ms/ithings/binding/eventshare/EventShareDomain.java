package com.synapse.ms.ithings.binding.eventshare;

import com.synapse.ms.ithings.core.model.AbstractDomain;

public class EventShareDomain extends AbstractDomain {

    public static final String GID = "eventshare";

    public EventShareDomain() {
        super();
        drivers.put("generic", "com.synapse.ms.ithings.binding.eventshare.GenericRepresenter");
    }

    @Override
    public String getBridgeClassName() {
        return "com.synapse.ms.ithings.binding.eventshare.EventShareBridge";
    }

    @Override
    public String getGid() {
        return GID;
    }
}
