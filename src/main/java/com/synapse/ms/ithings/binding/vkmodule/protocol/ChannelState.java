package com.synapse.ms.ithings.binding.vkmodule.protocol;

public interface ChannelState {
    Channel getChannel();
    boolean getState();
}
