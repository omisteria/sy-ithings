package com.synapse.ms.ithings.binding.mqtt;

import com.synapse.ms.ithings.core.model.AbstractDomain;

public class MqttDomain extends AbstractDomain {

    public MqttDomain() {
        super();
        drivers.put("basic-mapper", "com.synapse.ms.ithings.binding.mqtt.MqttBasicMapper");
    }

    @Override
    public String getGid() {
        return "mqtt";
    }

    public String getBridgeClassName() {
        return "com.synapse.ms.ithings.binding.mqtt.MqttBridge";
    }
}
