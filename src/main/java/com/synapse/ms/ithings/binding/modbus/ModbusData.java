package com.synapse.ms.ithings.binding.modbus;

public class ModbusData {
    private Integer func;
    private Integer address;
    private Integer length = 0;

    public static ModbusData parse(String str) {
        ModbusData d = new ModbusData();
        if (str == null) {
            throw new IllegalStateException("Input string is null");
        }
        if (!str.contains(":")) {
            throw new IllegalStateException("Input string is not well formatted");
        }
        String[] p = str.split(":");
        if (p.length<2) {
            throw new IllegalStateException("Input string contains only one part");
        }

        d.func = Integer.valueOf(p[0]);
        d.address = Integer.valueOf(p[1]);

        if (p.length == 3) {
            d.length = Integer.valueOf(p[2]);
        }
        return d;
    }

    public Integer getFunc() {
        return func;
    }

    public void setFunc(Integer func) {
        this.func = func;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }
}
