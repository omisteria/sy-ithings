package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.build.AbstractRepresenterFactory;

public class VkModuleFactory extends AbstractRepresenterFactory {

    public VkModuleFactory(Domain domain) {
        super(domain);
    }
}
