package com.synapse.ms.ithings.binding.sonoff;

import com.synapse.ms.ithings.core.model.bridge.BaseMqttBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class SonoffBridge extends BaseMqttBridge {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public SonoffBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {
            @Override
            public void initialize() {
                logger.info("Initialize bridge "+getGid());
                super.initialize();
                activateMqttBrokerConnection();
            }
        });
    }

    /*@Override
    public void activate() throws Exception {
        super.activate();
        activateMqttBrokerConnection();
    }*/
}
