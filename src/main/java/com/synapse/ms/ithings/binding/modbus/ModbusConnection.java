package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.common.BridgeException;
import com.synapse.ms.ithings.core.common.events.ConnectionListener;
import com.synapse.ms.ithings.io.transport.vkmodbus.io.ModbusTCPTransaction;
import com.synapse.ms.ithings.io.transport.vkmodbus.msg.*;
import com.synapse.ms.ithings.io.transport.vkmodbus.net.TCPMasterConnection;
import com.synapse.ms.ithings.io.transport.vkmodbus.net.TCPVKMMasterConnection;
import com.synapse.ms.ithings.io.transport.vkmodbus.procimg.InputRegister;
import com.synapse.ms.ithings.io.transport.vkmodbus.procimg.Register;
import com.synapse.ms.ithings.io.transport.vkmodbus.procimg.SimpleInputRegister;
import com.synapse.ms.ithings.io.transport.vkmodbus.util.BitVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ModbusConnection {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private List<ConnectionListener> listeners = new ArrayList<>();

    private String ipAddress;
    private int ipPort;

    private TCPMasterConnection connection;
    private boolean connected;
    private Thread  connectingTread;

    public ModbusConnection(String ipAddress, int ipPort) {
        this.ipAddress = ipAddress;
        this.ipPort = ipPort;
    }

    public synchronized void connect() throws Exception {
        if (connectingTread != null && connectingTread.isAlive()) {
            connectingTread.interrupt();
        }

        connectingTread = new Thread(new Runnable() {
            public void run() {
                try {
                    tryConnect();
                } catch (Exception e) {
                    logger.error("Connection problem", e);
                }
            }
        });
        connectingTread.start();
    }

    private void tryConnect() throws Exception {
        if (!this.connected) {

            logger.info("Connect to: "+this.ipAddress+", "+this.ipPort);
            connection = new TCPVKMMasterConnection(new InetSocketAddress(ipAddress, ipPort));
            connection.connect();

            connected = true;
            logger.info("Successfully connected");

            notifyListeners(listener -> listener.onConnectionChanged(connected));

        }
    }

    public void close() {
        tryClose();
    }

    public void tryClose() {

        logger.info("Close connection, connected: " + connected);

        if (this.connected)
        {
            connection.close();
            this.connected = false;
        }

        notifyListeners(listener -> listener.onConnectionChanged(connected));
    }


    public InputRegister[] readInputRegister(int idDevice, int dataAddress, int dataAddressLength) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusTCPTransaction netTransaction = new ModbusTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new ReadInputRegistersRequest(dataAddress, dataAddressLength);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                logger.error("Transaction error", e);
            }

            ReadInputRegistersResponse responce = (ReadInputRegistersResponse)netTransaction.getResponse();
            return responce.getRegisters();

        } catch (Exception e) {
            throw new BridgeException("Read problem input register for device "+idDevice, e);
        }
    }

    public BitVector readInputDiscretes(int idDevice, int dataAddress, int dataAddressLength) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusTCPTransaction netTransaction = new ModbusTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new ReadInputDiscretesRequest(dataAddress, dataAddressLength);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                logger.error("Transaction error", e);
            }

            ReadInputDiscretesResponse responce = (ReadInputDiscretesResponse)netTransaction.getResponse();
            return responce.getDiscretes();

        } catch (Exception e) {
            throw new BridgeException("Read problem input discretes for device "+idDevice, e);
        }
    }

    public BitVector readCoils(int idDevice, int dataAddress, int dataAddressLength) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusTCPTransaction netTransaction = new ModbusTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new ReadCoilsRequest(dataAddress, dataAddressLength);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                logger.error("Transaction error", e);
            }

            ReadCoilsResponse responce = (ReadCoilsResponse)netTransaction.getResponse();
            return responce.getCoils();

        } catch (Exception e) {
            throw new BridgeException("Read problem coils for device "+idDevice, e);
        }
    }

    public boolean writeCoil(int idDevice, int dataAddress, boolean state) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusTCPTransaction netTransaction = new ModbusTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new WriteCoilRequest(dataAddress, state);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                logger.error("Transaction error", e);
            }

            WriteCoilResponse responce = (WriteCoilResponse)netTransaction.getResponse();
            return responce != null ? responce.getCoil() : false;

        } catch (Exception e) {
            throw new BridgeException("Write problem coil for device "+idDevice, e);
        }
    }

    public int writeSingleRegister(int idDevice, int dataAddress, int value) throws BridgeException {

        try {

            byte[] data = new byte[2];
            data[0] = (byte)((value&0xFF00)>>8);
            data[1] = (byte)((value&0x00FF));

            if (!connection.isConnected()) connection.connect();

            ModbusTCPTransaction netTransaction = new ModbusTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            Register register = new SimpleInputRegister();
            register.setValue(data);

            ModbusRequest request = new WriteSingleRegisterRequest(dataAddress, register);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                logger.error("Transaction error", e);
            }

            WriteSingleRegisterResponse responce = (WriteSingleRegisterResponse)netTransaction.getResponse();
            return responce != null ? responce.getRegisterValue() : 0;

        } catch (Exception e) {
            throw new BridgeException("Write problem single register for device "+idDevice, e);
        }
    }

    public ConnectionListener addListener(ConnectionListener listener) {
        listeners.add(listener);
        return listener;
    }

    public synchronized void notifyListeners(Consumer<? super ConnectionListener> algorithm) {
        this.listeners.forEach(listener -> algorithm.accept(listener));
    }
}
