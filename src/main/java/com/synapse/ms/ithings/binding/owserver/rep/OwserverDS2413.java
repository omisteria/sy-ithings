package com.synapse.ms.ithings.binding.owserver.rep;

import com.synapse.ms.ithings.binding.owserver.OwserverBridge;
import com.synapse.ms.ithings.binding.owserver.OwserverConfKey;
import com.synapse.ms.ithings.binding.owserver.OwserverDeviceParameter;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;

import java.util.Map;

public class OwserverDS2413 extends AbstractRepresenter {

    public OwserverDS2413(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);

        predefinedChannels.add(new PredefinedChannel("PIO.A", ChannelType.RELAY, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("PIO.B", ChannelType.RELAY, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("sensed.A", ChannelType.DRY_CONTACT, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("sensed.B", ChannelType.DRY_CONTACT, UnDefType.UNDEF));
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge, this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                super.initialize();
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {

                    try {


                        OwserverBridge owBridge = (OwserverBridge) bridge;
                        String deviceId = buildDeviceId(description);
                        Channel channel = getChannel(channelId);
                        String selector = channel.getSelector();

                        if (ChannelType.RELAY.equals(channel.getType())) {
                            OnOffType relayValue = command.as(OnOffType.class);
                            if (channel.isInverseState()) {
                                relayValue = relayValue.inverse();
                            }
                            byte[] data = relayValue.as(RawType.class).getBytes();
                            owBridge.writeBytes(deviceId, new OwserverDeviceParameter("uncached", selector), data);

                        } else if (ChannelType.DRY_CONTACT.equals(channel.getType())) {

                            byte[] res = owBridge.readBytes(deviceId, new OwserverDeviceParameter("uncached", selector));
                            onReceivedResponse(res, channelId);
                        }

                    } catch (Exception e) {
                        // set representer status

                    }
                });
            }

            public void onReceivedResponse(byte[] responseData, String channelId) {
                Channel channel = representer.getChannel(channelId);
                if (channel != null) {
                    try {

                        if (ChannelType.DRY_CONTACT.equals(channel.getType())) {
                            OnOffType relayValue = new RawType(responseData).as(OnOffType.class);
                            if (channel.isInverseState()) {
                                relayValue = relayValue.inverse();
                            }

                            channel.getState().update(relayValue);
                        }

                    } catch (Exception e) {
                        logger.warn("Channel ["+channelId+"]: can't convert recieved data ["+new String(responseData)+"]");
                    }
                }
            }

            private String buildDeviceId(Map<String, Object> reprDesc) {
                String family = (String) reprDesc.get(OwserverConfKey.family);
                String id = (String) reprDesc.get(OwserverConfKey.id);
                return family + "." + id;
            }
        });
    }
}
