package com.synapse.ms.ithings.binding.owserver.rep;

import com.synapse.ms.ithings.binding.owserver.OwserverBridge;
import com.synapse.ms.ithings.binding.owserver.OwserverConfKey;
import com.synapse.ms.ithings.binding.owserver.OwserverDeviceParameter;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.DecimalType;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;

import java.util.Map;

public class OwserverDS18B20 extends AbstractRepresenter {

    public OwserverDS18B20(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);

        predefinedChannels.add(new PredefinedChannel("temperature", ChannelType.TEMPERATURE, UnDefType.UNDEF));
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                super.initialize();
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {

                    try {
                        OwserverBridge owBridge = (OwserverBridge) bridge;
                        String deviceId = buildDeviceId(description);
                        Channel channel = getChannel(channelId);
                        String selector = channel.getSelector();

                        if (ChannelType.TEMPERATURE.equals(channel.getType())) {
                            byte[] res = owBridge.readBytes(deviceId, new OwserverDeviceParameter("uncached", selector));
                            onReceivedResponse(res, channelId);
                        }

                    } catch (Exception e) {
                        // set representer status

                    }
                });
            }

            @Override
            public void onReceivedResponse(byte[] responseData, String channelId) {
                Channel channel = representer.getChannel(channelId);
                if (channel != null) {
                    try {
                        DecimalType st = new RawType(responseData).as(DecimalType.class);
                        channel.getState().update(st);
                    } catch (Exception e) {
                        logger.warn("Channel ["+channelId+"]: can't convert recieved data ["+new String(responseData)+"]");
                    }
                }
            }

            private String buildDeviceId(Map<String, Object> reprDesc) {
                String family = (String) reprDesc.get(OwserverConfKey.family);
                String id = (String) reprDesc.get(OwserverConfKey.id);
                return family + "." + id;
            }
        });
    }
}
