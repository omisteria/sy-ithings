package com.synapse.ms.ithings.binding.owserver.rep;

import com.synapse.ms.ithings.binding.owserver.OwserverBridge;
import com.synapse.ms.ithings.binding.owserver.OwserverConfKey;
import com.synapse.ms.ithings.binding.owserver.OwserverDeviceParameter;
import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.DecimalType;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.common.types.UnDefType;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.channel.PredefinedChannel;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;

import java.util.Map;

public class OwserverDS2438 extends AbstractRepresenter {

    public OwserverDS2438(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);

        predefinedChannels.add(new PredefinedChannel("humidity", ChannelType.HUMIDITY, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("temperature", ChannelType.TEMPERATURE, UnDefType.UNDEF));
        predefinedChannels.add(new PredefinedChannel("pressure", ChannelType.PRESSURE, UnDefType.UNDEF));
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                super.initialize();
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {

                    try {
                        OwserverBridge owBridge = (OwserverBridge) bridge;
                        String deviceId = buildDeviceId(description);
                        Channel channel = getChannel(channelId);
                        String selector = channel.getSelector();

                        byte[] res = owBridge.readBytes(deviceId, new OwserverDeviceParameter("uncached", selector));
                        onReceivedResponse(res, channelId);

                    } catch (Exception e) {
                        // set representer status

                    }
                });
            }

            public void onReceivedResponse(byte[] responseData, String channelId) {
                Channel channel = representer.getChannel(channelId);
                if (channel != null) {
                    try {

                        DecimalType st = new RawType(responseData).as(DecimalType.class);

                        if (channel.getDescription().get(OwserverConfKey.scale) != null) {
                            Double scale = (Double) channel.getDescription().get(OwserverConfKey.scale);
                            st = new DecimalType(st.doubleValue() * scale);
                        }

                        if (channel.getDescription().get(OwserverConfKey.precision) != null) {
                            Double precision = (Double) channel.getDescription().get(OwserverConfKey.precision);
                            if (precision == 0) {
                                st = new DecimalType(Math.round(st.doubleValue()));
                            } else {
                                double o = Math.pow(10, precision);
                                st = new DecimalType(Math.round(st.doubleValue() * o) / o);
                            }
                        }

                        channel.getState().update(st);

                    } catch (Exception e) {
                        logger.warn("Channel ["+channelId+"]: can't convert recieved data ["+new String(responseData)+"]");
                    }
                }
            }

            private String buildDeviceId(Map<String, Object> reprDesc) {
                String family = (String) reprDesc.get(OwserverConfKey.family);
                String id = (String) reprDesc.get(OwserverConfKey.id);
                return family + "." + id;
            }
        });
    }
}
