package com.synapse.ms.ithings.binding.owserver;

public class OwserverDeviceParameter {
    private String prefix = "";
    private String path = "";

    /**
     * device parameter for owserver bridge handler
     *
     * @param prefix path prefix (e.g. "uncached/")
     * @param path path without sensor id (e.g. "/humidity")
     */
    public OwserverDeviceParameter(String prefix, String path) {
        if (prefix.endsWith("/")) {
            this.prefix = prefix.substring(0, prefix.length() - 1);
        } else {
            this.prefix = prefix;
        }
        if (path.startsWith("/")) {
            this.path = path;
        } else {
            this.path = "/" + path;
        }
    }

    /**
     * device parameter for owserver bridge handler
     *
     * @param path path without sensor id (e.g. "/humidity")
     */
    public OwserverDeviceParameter(String path) {
        this("", path);
    }

    /**
     * get the full owfs path for a given sensor id
     *
     * @param deviceId
     */
    public String getPath(String deviceId) {
        if (deviceId.startsWith("/")) {
            return prefix + deviceId + path;
        } else {
            return prefix + "/" + deviceId + path;
        }
    }

    @Override
    public String toString() {
        return prefix + "/sensorId" + path;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof OwserverDeviceParameter)) {
            return false;
        }

        return o.toString().equals(toString());
    }
}
