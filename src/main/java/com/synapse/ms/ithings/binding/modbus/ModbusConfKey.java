package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.config.ConfKey;

public interface ModbusConfKey extends ConfKey {

    String socketHost         = "socketHost";
    String socketPort         = "socketPort";
    String function         = "function";
    String address         = "address";
    String length         = "length";
    String commandState    = "commandState";
    String commandChange   = "commandChange";
    String transform   = "transform";
    String offset       = "offset";
    String precision       = "precision";
}
