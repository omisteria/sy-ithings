package com.synapse.ms.ithings.binding.geeklink;

import com.synapse.ms.ithings.core.config.ConfKey;

public interface GeeklinkConfKey extends ConfKey {

    String mqttHost = "mqttHost";
    String mqttPort = "mqttPort";
}
