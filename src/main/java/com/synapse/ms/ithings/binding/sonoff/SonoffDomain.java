package com.synapse.ms.ithings.binding.sonoff;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.AbstractDomain;

import java.util.HashMap;
import java.util.Map;

public class SonoffDomain extends AbstractDomain {

    public static final String GID = "sonoff";

    public SonoffDomain() {
        super();
        drivers.put("basic", "com.synapse.ms.ithings.binding.sonoff.SonoffBasic");
        drivers.put("rf-bridge", "com.synapse.ms.ithings.binding.sonoff.SonoffRfBridge");
        drivers.put("colorbulb", "com.synapse.ms.ithings.binding.sonoff.SonoffColorBulb");
    }

    @Override
    public String getGid() {
        return GID;
    }

    public String getBridgeClassName() {
        return "com.synapse.ms.ithings.binding.sonoff.SonoffBridge";
    }
}
