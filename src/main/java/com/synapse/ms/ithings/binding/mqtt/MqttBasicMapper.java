package com.synapse.ms.ithings.binding.mqtt;

import com.synapse.ms.ithings.core.common.types.Command;
import com.synapse.ms.ithings.core.common.types.FlatJsonType;
import com.synapse.ms.ithings.core.common.types.RawType;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;
import com.synapse.ms.ithings.io.transport.mqtt.MqttException;

import java.util.Map;

public class MqttBasicMapper extends AbstractRepresenter {

    public MqttBasicMapper(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            @Override
            public void initialize() {
                super.initialize();

                getChannels().forEach(channel -> {
                    try {
                        Map<String, Object> desc = channel.getDescription();
                        String stateTopic = (String) desc.get(MqttConfKey.stateTopic);
                        String mapString = (String) desc.get(MqttConfKey.mapFrom);

                        if (stateTopic != null && mapString!=null) {
                            MqttBridge mqttBridge = (MqttBridge) bridge;
                            String[] p1 = mapString.split(",");
                            String[] p2 = p1[0].split("=");

                            mqttBridge.subscribe(stateTopic, new DataHandler<byte[], byte[]>() {
                                @Override
                                public void sendRequest(Command command) throws Exception {
                                    dataHandler.sendRequest(command, channel.getGid());
                                }

                                @Override
                                public void onReceivedResponse(byte[] responseData) {
                                    if (responseData != null && responseData.length>9) {
                                        FlatJsonType mv = new RawType(responseData).as(FlatJsonType.class);
                                        String oid = mv.getByKey(p2[0]);
                                        String obsId = p2[1];
                                        if (obsId.equals(oid)) {
                                            dataHandler.onReceivedResponse(mv.getByKey(p1[1]).getBytes(), channel.getGid());
                                        }
                                    }
                                }
                            });
                        }

                        channel.getActivateStatus().update(ActivateStatus.ONLINE);
                    } catch (MqttException e) {
                        // set representer status
                        channel.getActivateStatus().update(ActivateStatus.OFFLINE);
                    }
                });

                getActivateStatus().update(ActivateStatus.ONLINE);
            }
        });
    }
}
