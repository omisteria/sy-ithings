package com.synapse.ms.ithings.binding.mqtt;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.BaseMqttBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;

import java.util.Map;

public class MqttBridge extends BaseMqttBridge {

    public MqttBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {
            @Override
            public void initialize() {
                super.initialize();

                activateMqttBrokerConnection();
            }
        });
    }
}
