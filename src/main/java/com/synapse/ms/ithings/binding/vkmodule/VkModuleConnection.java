package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.core.common.events.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class VkModuleConnection {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String ipAddress;
    private int ipPort;

    private Socket socket;
    private int socketTimeoutMillis = 3000;
    private boolean connected;
    private Thread  connectingTread;

    private long checkConnectionMillis = 8000;
    private long heartbeatDelayMillis = 5000;

    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private List<ConnectionListener> listeners = new ArrayList<>();

    public VkModuleConnection(String ipAddress, int ipPort) {
        this.ipAddress = ipAddress;
        this.ipPort = ipPort;

        Thread t = new Thread(new Runnable() {
            public void run() {

                try {
                    Thread.sleep(checkConnectionMillis + heartbeatDelayMillis);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                while(!Thread.interrupted()) {

                    try {
                        Thread.sleep(checkConnectionMillis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //send a test signal
                    try {

                        sendMessage(new byte[] {0x01});
                        Thread.sleep(heartbeatDelayMillis);

                    /*} catch (InterruptedException e) {
                        tryToReconnect = false;*/
                    } catch (Exception e) {
                        try {
                            connected = false;
                            connect();
                        } catch (Exception e1) {
                            logger.error("Connection problem", e1);
                        }
                    }

                }
            }
        });
        t.start();
    }

    public synchronized void connect() throws Exception {

        if (connectingTread != null && connectingTread.isAlive()) {
            connectingTread.interrupt();
        }

        connectingTread = new Thread(new Runnable() {
            public void run() {
                try {
                    tryConnect();
                } catch (Exception e) {
                    logger.error("Connection problem", e);
                }
            }
        });
        connectingTread.start();

    }

    private void tryConnect() throws Exception {
        if (!this.connected) {

            logger.info("Connect to: "+this.ipAddress+", "+this.ipPort);

            this.socket = new Socket();

            InetSocketAddress address = new InetSocketAddress(ipAddress, ipPort);
            setTimeout(socketTimeoutMillis);

            this.socket.connect(address, socketTimeoutMillis);

            connected = true;
            //tryToReconnect = true;
            logger.info("Successfully connected");
            notifyListeners(listener -> listener.onConnectionChanged(connected));

            this.inputStream = new DataInputStream(socket.getInputStream());
            this.outputStream = new DataOutputStream(socket.getOutputStream());

            try
            {
                while(connected) {
                    int bytesRead = 0;
                    byte[] inStream = new byte[1025];

                    try {
                        bytesRead = inputStream.read(inStream);
                    } catch (IOException e) {

                    }
                    /*int length = inputStream.readInt();
                    byte[] inStream = new byte[length];
                    try {
                        bytesRead = inputStream.read(inStream, 0, length);
                    } catch (IOException e) {

                    }*/

                    if (bytesRead > 0 && inStream[0]>1) {
                        notifyListeners(listener -> listener.onReceiveData(inStream));
                    }

                }
            }
            catch (Exception e)
            {
                logger.error("Error while reading from socket: " + e);
                throw new IOException("I/O exception - failed to read.\n" + e);
            } finally {
                tryClose();
            }
        } else {
            logger.info("Already connected to: "+this.ipAddress+", "+this.ipPort);
        }
    }

    public synchronized void sendMessage(byte[] outStream) throws Exception {
        /*if (!this.connected) {
            tryConnect();
        }*/
        outputStream.write(outStream);
    }

    public void close() {
        tryClose();
        //tryToReconnect = false;
    }

    public void tryClose() {

        logger.info("Close connection, connected: " + connected);

        // if connected... disconnect, otherwise do nothing
        if (this.connected)
        {
            // try closing the transport...
            try
            {
                if (this.socket != null && !this.socket.isClosed()) {
                    this.socket.close();
                }
            }
            catch (IOException e)
            {
                logger.info("Error while closing the connection, cause:" + e);
            }

            // if everything is fine, set the connected flag at false
            this.connected = false;
        }

        notifyListeners(listener -> listener.onConnectionChanged(connected));
    }

    public void setTimeout(int timeout)
    {
        //store the current socket timeout
        this.socketTimeoutMillis = timeout;

        //set the timeout on the socket, if available
        if (this.socket != null)
        {
            try
            {
                this.socket.setSoTimeout(socketTimeoutMillis);
            }
            catch (IOException ex)
            {
                // TODO: handle?
            }
        }
    }

    public ConnectionListener addListener(ConnectionListener listener) {
        listeners.add(listener);
        return listener;
    }

    public void removeListener(ConnectionListener listener) {
        listeners.remove(listener);
    }

    public synchronized void notifyListeners(Consumer<? super ConnectionListener> algorithm) {
        this.listeners.forEach(listener -> algorithm.accept(listener));
    }
}
