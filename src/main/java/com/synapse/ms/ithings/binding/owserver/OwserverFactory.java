package com.synapse.ms.ithings.binding.owserver;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.build.AbstractRepresenterFactory;
import com.synapse.ms.ithings.core.config.*;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.RuntimeChannel;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;

import java.lang.reflect.Constructor;
import java.util.Map;

public class OwserverFactory extends AbstractRepresenterFactory {

    public OwserverFactory(Domain domain) {
        super(domain);
    }
}
