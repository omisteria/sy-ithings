package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.AbstractBridge;
import com.synapse.ms.ithings.core.model.handler.BridgeDataHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class VkModuleBridge extends AbstractBridge {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public VkModuleBridge(String gid, Map<String, Object> description) {
        super(gid, description);
    }

    @Override
    public void configure() {
        setDataHandler(new BridgeDataHandler(this) {
            @Override
            public void initialize() {
                logger.info("Initialize bridge "+getGid());
                super.initialize();

                getActivateStatus().update(ActivateStatus.ONLINE);
            }
        });
    }
}
