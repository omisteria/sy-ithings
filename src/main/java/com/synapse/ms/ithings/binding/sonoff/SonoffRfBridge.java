package com.synapse.ms.ithings.binding.sonoff;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.synapse.ms.ithings.core.common.types.*;
import com.synapse.ms.ithings.core.model.ActivateStatus;
import com.synapse.ms.ithings.core.model.bridge.Bridge;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.channel.ChannelType;
import com.synapse.ms.ithings.core.model.handler.DataHandler;
import com.synapse.ms.ithings.core.model.handler.RepresenterDataHandler;
import com.synapse.ms.ithings.core.model.handler.RequestExecutor;
import com.synapse.ms.ithings.core.model.represent.AbstractRepresenter;
import com.synapse.ms.ithings.io.transport.mqtt.MqttException;

import java.util.Map;

public class SonoffRfBridge extends AbstractRepresenter {

    public SonoffRfBridge(String uid, Bridge bridge, Map<String, Object> description) {
        super(uid, bridge, description);
    }

    @Override
    public void configure() {
        setDataHandler(new RepresenterDataHandler(bridge,this) {

            /**
             * Initialize device
             */
            @Override
            public void initialize() {
                super.initialize();

                getChannels().forEach(channel -> {
                    try {
                        Map<String, Object> desc = channel.getDescription();
                        String stateTopic = (String) desc.get(SonoffConfKey.stateTopic);

                        if (stateTopic != null) {
                            SonoffBridge sonoffBridge = (SonoffBridge) bridge;
                            sonoffBridge.subscribe(stateTopic, new DataHandler<byte[], byte[]>() {
                                @Override
                                public void sendRequest(Command command) throws Exception {
                                    dataHandler.sendRequest(command, channel.getGid());
                                }

                                @Override
                                public void onReceivedResponse(byte[] responseData) {
                                    dataHandler.onReceivedResponse(responseData, channel.getGid());
                                }
                            });
                        }

                        channel.getActivateStatus().update(ActivateStatus.ONLINE);
                    } catch (MqttException e) {
                        // set representer status
                        channel.getActivateStatus().update(ActivateStatus.OFFLINE);
                    }
                });

                getActivateStatus().update(ActivateStatus.ONLINE);
            }

            /**
             * Send request to device channel
             * @param command
             * @param channelId
             * @throws Exception
             */
            @Override
            public void sendRequest(Command command, String channelId) {
                RequestExecutor.doSubmit(() -> {

                    try {
                        byte[] data = command.as(RawType.class).getBytes();

                        Channel channel = getChannel(channelId);
                        Map<String, Object> desc = channel.getDescription();
                        String commandTopic = (String) desc.get(SonoffConfKey.commandTopic);

                        SonoffBridge sonoffBridge = (SonoffBridge) bridge;
                        sonoffBridge.publish(commandTopic, data);

                    } catch (MqttException e) {
                        // set representer status
                    }
                });
            }

            /*public void onReceivedResponse(byte[] responseData, String channelId) {
                Channel channel = representer.getChannel(channelId);
                if (channel != null) {
                    // TODO: channel handler for input data
                    FlatJsonType st = new RawType(responseData).as(FlatJsonType.class);
                    State newState = StringType.valueOf(st.getByKey("POWER")).as(OnOffType.class);
                    channel.getState().update(newState);
                }
            }*/

            public void onReceivedResponse(byte[] responseData, String channelId) {
                State rawState = new RawType(responseData);

                Channel channel = representer.getChannel(channelId);
                if (channel != null) {

                    try {
                        String data = new Gson().fromJson(rawState.as(StringType.class).toString(), JsonObject.class)
                                .getAsJsonObject("RfReceived")
                                .getAsJsonPrimitive("Data")
                                .getAsString();

                        if (channel.getSelector() != null && channel.getSelector().equals(data)) {

                            if (ChannelType.SWITCH.equals(channel.getType())) {
                                State currValue = channel.getState().getValue();
                                channel.getState().update(currValue.equals(OnOffType.OFF) ? OnOffType.ON : OnOffType.OFF);
                            }

                            if (ChannelType.BUTTON.equals(channel.getType())) {
                                channel.getState().pulse(OnOffType.ON);
                            }
                        }
                    } catch (Exception e) {
                        logger.warn("Cannel ["+channelId+"]: can't convert recieved data ["+responseData+"]");
                    }
                }
            }
        });
    }
}
