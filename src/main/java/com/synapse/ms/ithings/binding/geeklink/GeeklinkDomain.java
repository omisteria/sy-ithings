package com.synapse.ms.ithings.binding.geeklink;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.build.RepresenterFactory;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.AbstractDomain;

import java.util.HashMap;
import java.util.Map;

public class GeeklinkDomain extends AbstractDomain {

    public GeeklinkDomain() {
        super();
        drivers.put("geeklinkmini", "com.synapse.ms.ithings.binding.geeklink.GeeklinkMini");
    }

    @Override
    public RepresenterFactory getRepresenterFactory() {
        return new GeeklinkFactory(this);
    }

    @Override
    public String getBridgeClassName() {
        return "com.synapse.ms.ithings.binding.geeklink.GeeklinkBridge";
    }

    @Override
    public String getGid() {
        return "geeklink";
    }
}
