package com.synapse.ms.ithings.binding.vkmodule.protocol;

public interface Channel {

    byte getNumber();
}
