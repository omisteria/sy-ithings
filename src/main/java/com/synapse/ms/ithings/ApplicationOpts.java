package com.synapse.ms.ithings;

import java.util.Properties;

public class ApplicationOpts {

    public static final String CONFIG_DIR = "config.location";

    private static Properties properties = new Properties();

    public static void parseArguments(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i].charAt(0)) {
                case '-':
                    if (args[i].charAt(1) == '-') {
                        if (args[i].length() < 3)
                            throw new IllegalArgumentException("Not a valid argument: "+args[i]);
                        // --opt
                        String p = args[i].substring(2, args[i].length());
                        String[] parts = p.split("=");
                        properties.setProperty(parts[0], parts[1]);
                    }
                    break;
            }
        }
    }

    public static String getProperty(String propName) {
        //properties.setProperty(CONFIG_DIR, "D:\\Projects\\synapse\\sy-ithings\\config");
        return properties.getProperty(propName);
    }
}
