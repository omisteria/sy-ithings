package com.synapse.ms.ithings.ref;

public class IRCode {

    public static class Orton4100 {

        public static final String powerToggle = "{\"Protocol\":\"NEC\",\"Bits\":32,\"Data\":\"0x020250AF\"}";
    }

    public static class Philips190TW {

        public static final String powerToggle = "{\"Protocol\":\"NEC\",\"Bits\":32,\"Data\":\"0x01FE48B7\"}";

    }

    public static class Fireplace {

        public static final String powerToggle = "{\"Protocol\":\"NEC\",\"Bits\":32,\"Data\":\"0x00FF23DC\"}";

    }
}
