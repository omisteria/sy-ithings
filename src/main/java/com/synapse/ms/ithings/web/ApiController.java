package com.synapse.ms.ithings.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.synapse.ms.ithings.core.JsonExclusionStrategy;
import com.synapse.ms.ithings.core.dto.RepresenterModel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;

import java.util.Collection;
import java.util.stream.Collectors;

import static spark.Spark.get;
import static spark.Spark.post;

public class ApiController {

    private final String PATH_PREFIX = "/api";

    RepresenterManager representerManager;

    public ApiController(RepresenterManager representerManager) {
        this.representerManager = representerManager;

        /**
         *
         */
        get(PATH_PREFIX + "/representers/list", (req, res) -> {
            Collection<Representer> list = representerManager.getRepresenterRegistry().values();
            Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).setPrettyPrinting().create();

            return gson.toJson(new StandardResponse(StatusResponse.SUCCESS,
                    gson.toJsonTree(list.stream().map(RepresenterModel::toModel).collect(Collectors.toList()))));
        });

        /**
         *
         */
        post(PATH_PREFIX + "/representer/list", (req, res) -> {

            return null;
        });
    }
}
