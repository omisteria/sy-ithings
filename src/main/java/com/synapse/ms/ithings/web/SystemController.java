package com.synapse.ms.ithings.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.synapse.ms.ithings.core.JsonExclusionStrategy;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;

import static spark.Spark.get;

public class SystemController {

    RepresenterManager representerManager;

    public SystemController(RepresenterManager representerManager) {

        this.representerManager = representerManager;

        get("/hello", (req, res) -> {

            Representer r = representerManager.getRepresenterRegistry().get("switch1");

            Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).setPrettyPrinting().create();
            return gson.toJson(new StandardResponse(StatusResponse.SUCCESS, gson.toJsonTree(r)));
        });
    }
}
