const ON = 'On';
const OFF = 'Off';

let handlerTurnOnXmasLights = function() {
    // Bedroom's window
    //Gear.ch('sonoff.x-switch1.relay').command(ON);
    // Kitchen's window
    //Gear.ch('sonoff.x-switch3.relay').command(ON);
    // Christmas Tree
    // Gear.ch('sonoff.x-switch4.relay').command(ON);

    //Gear.ch('geeklink.irblaster1.ir').command('{"Protocol":"NEC","Bits":32,"Data":"0x00FF23DC"}');
};
let handlerTurnOffXmasLights = function() {
    // Bedroom's window
    //Gear.ch('sonoff.x-switch1.relay').command(OFF);
    // Kitchen's window
    //Gear.ch('sonoff.x-switch3.relay').command(OFF);
    // Christmas Tree
    // Gear.ch('sonoff.x-switch4.relay').command(OFF);

    //Gear.ch('geeklink.irblaster1.ir').command('{"Protocol":"NEC","Bits":32,"Data":"0x00FF23DC"}');
};

/*Rule.create()
    .when(System.started())
    .exec(handlerTurnOnXmasLights);*/

/*Rule.create()
    .when(Scheduler.cron('*!/9 * * * * ?'))
    .exec(function() {

        // Bedroom's window
        Gear.ch('sonoff.x-switch1.relay').toggle();
        // Humidity switch
        Gear.ch('sonoff.x-switch2.relay').toggle();
        // Kitchen's window
        Gear.ch('sonoff.x-switch3.relay').toggle();
        // Christmas Tree
        Gear.ch('sonoff.x-switch4.relay').toggle();

        Gear.ch('geeklink.irblaster1.ir').toggle();
    });*/

/*Rule.create()
    .when(Scheduler.at('17:00:00'))
    .filter(Date.between('24.12.*','25.1.*'))
    .exec(handlerTurnOnXmasLights);*/

/*Rule.create()
    .when(Scheduler.at('16:30:00'))
    .filter(Date.is('31.12.*'))
    .exec(handlerTurnOnXmasLights);*/

// have to turn off about 3:00 on the 1.01.*
/*Rule.create()
    .when(Scheduler.at('1:00:00'))
    .filter(Date.between('24.12.*','26.1.*'))
    .exec(handlerTurnOffXmasLights);*/



// Humidity controll
/*Rule.create()
    .when(Gear.ch('mqtt.oldgate.humidity').less(45))
    .exec(function() {
        Gear.ch('sonoff.x-switch2.relay').command(ON);
    });
Rule.create()
    .when(Gear.ch('mqtt.oldgate.humidity').greater(46))
    .exec(function() {
        Gear.ch('sonoff.x-switch2.relay').command(OFF);
    });*/


// RF Button Test
/*
Rule.create()
    .when(Gear.ch('sonoff.rfbridge1.bt3').turned(ON))
    .exec(function() {
        Gear.ch('sonoff.x-switch1.relay').command(ON);
    });
Rule.create()
    .when(Gear.ch('sonoff.rfbridge1.bt3').turned(OFF))
    .exec(function() {
        Gear.ch('sonoff.x-switch1.relay').command(OFF);
    });
*/

// Lamp in the corner
/*
Rule.create('Lamp in the corner')
    .when(Gear.ch('mqtt.oldgate.sensorMovementLobby').greater(0))
    .filter(Time.between('18:00:00', '01:00:00')) //!!!!
    .exec(function() {
        Gear.ch('sonoff.x-switch5.relay').command(ON);
        Scheduler.create('lamp-on')
            .delay(360000)
            .exec(function() {
                Gear.ch('sonoff.x-switch5.relay').command(OFF);
            });
    });
*/

// Crismass Tree
/*Rule.create()
    .when(Gear.ch('mqtt.oldgate.sensorMovementLobby').greater(0))
    .filter(Time.between('16:00:00', '01:00:00')) //!!!!!
    .filter(Date.between('24.12.*','25.1.*'))
    .exec(function() {
        Gear.ch('sonoff.x-switch4.relay').command(ON);
        Scheduler.create('xtree-on')
            .delay(420000)
            .exec(function() {
                Gear.ch('sonoff.x-switch4.relay').command(OFF);
            });
    });*/

// Ventilation
// first - temperature, second - pulse duration in secs
var converter = Func.rangeConverter(20.2, 22.4, 120, 600);
converter.sourceValue = 0.5;

/*
Rule.create()
    .when(System.started())
    .exec(function() {


        Process
            .create(function() {
                return {pulsePeriod: 600, pulseDuration: converter.targetValue};
            })
            .exec(function(way) {
                Gear.ch('owserver.vent.fan').command( way ? ON : OFF);
            });
    });

*/
