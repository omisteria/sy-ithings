/*
* Channel has state and event internal properties
* Trigger has event only property
*
* */
const ON = 'On';
const OFF = 'Off';

function logDebug(loggerName, format, args) {

}
function logInfo(loggerName, format, args) {

}
function logWarn(loggerName, format, args) {

}
function logError(loggerName, format, args) {

}

function alias(aliasName, entityId) {
    return $RE.createAlias(aliasName, entityId);
}
function group() {
    return $RE.createGroup();
}

/**
 * Trigger aggregators
 */

// fire if one of triggers is fired
function any() {
    //return $RE.createAgrTriggerAny(arguments);
}

// fire if all triggers were fired in period 200 millisec
function whenAll() {

}
function near() {

}
function stateAll() {

}

function testRule() {

}
function test() {

}

// if (uKitchenFloorHeaterSchedul.value > 0 && uNoAnybodyHome.value != 1 && tempOutdoor.value < 8 && tempHall.value < 23.1) {
// }



// Trigger repeater startDate, fromTime, toTime, repeatEvery

//print(Math.round(1.47));

var sysStarted = System.started();
// var timerEvery9Ses = Scheduler.cron('*/9 * * * * ?');
var timerAtSomeTime = Scheduler.at('22:55:00');
// var timerPeriod = Scheduler.interval(30); // seconds
var testTrigger = Op.or(sysStarted, timerAtSomeTime);






//.when(Op.or(System.started(), Scheduler.cron('*/9 * * * * ?'), Rule.done(testRule), Rule.done('t1')))
// Scheduler.createDelay(500)

/*Rule.create()
    .when(timerEvery9Ses)
    .filter(Op.or(Time.greater('08:00'), Time.greater('08:00')))
    .exec(function() {
        print('Exec object rule 2');

        Scheduler.create()
            .delay(500)
            .exec(function() {
                print("Test~~delay 2");
            });
    });*/

/*Scheduler.interval(function() {
    print("Test~~");
    Thing.ch('sonoff.repr1.A').revertAfter(3000).command(ON);
}, 1500);*/

/*rule("rule1",
    whenAll([Thing.ch('Lamp').turned(ON), Thing.ch('Lamp2').turned(OFF,ON)], 100),
    function() {
        Gear.ch('Lamp').status();
        Gear.ch('Lamp').command(ON);
        print('Print from handler')
    });*/

/*rule(
    whenAny(System.started(), Scheduler.cron("*!/2 * * * * ?"), Scheduler.cron("*!/5 * * * * ?")),
    function() {
        print('Print from aggrafete handler')
        Scheduler.at('12:04', '20s')
        .exec(function() {

        })
        .exec(function() {

        });
    });*/

/*rule(
    whenAny(System.started(), Scheduler.cron("*!/2 * * * * ?"), Scheduler.cron("*!/5 * * * * ?")),
    function() {
        E.createGroup();
        print('Print from aggrafete handler')
    });*/



/*var r1 = Rule.create()
    .when(System.started())
    .exec(
    function() {
        print('Print from handler ' + ON)
    }));

testRule(r1, [
    test(System.started(), function() {return Assert.ruleFired()}),
]);*/

/*
rule(
    System.started(),
    function() {
        print('Print from handler2 ' + OFF)
    });*/

/*rule(
    Scheduler.cron("*!/2 * * * * ?"),
    function() {
        print('Print from handler 3 -----')
    });*/

