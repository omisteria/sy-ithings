// Date.between('4.*.*','6.*.*')
// Date.between('*.4.*','*.6.*')
// Date.between('4.05.*','6.07.*')
// Date.between('4.05.2000','6.07.2001')
// var timerEvery9Ses = Scheduler.cron('*/9 * * * * ?');
var timerAtSomeTime = Scheduler.at('22:55:00');

var testRule = Rule.create('t1')
    .when(testTrigger)
    //.filter(Op.and(Date.between('24.12.*','27.1.*'), Time.between('17:30', '01:00'))) // doesn't fire turning off!!!!
    .exec(function() {
        print('Execute object rule 1, at some time');
    });
