const ON = 'On';
const OFF = 'Off';

Rule.create()
    .when(Gear.ch('mqtt.oldgate.electricityActivePower').greater(55))
    .exec(function() {
        print('+++++++++++++++Electricity Power greater than 55');
    });

Rule.create()
    .when(Gear.ch('mqtt.oldgate.electricityActivePower').less(550))
    .exec(function() {
        print('+++++++++++++++Electricity Power less than 5500');
    });

Rule.create()
    .when(Gear.ch('sonoff.rfbridge1.bt3').turned(ON))
    .exec(function() {
        Gear.ch('sonoff.x-switch1.relay').command(ON);
    });

Rule.create()
    .when(Gear.ch('sonoff.rfbridge1.bt3').turned(OFF))
    .exec(function() {
        Gear.ch('sonoff.x-switch1.relay').command(OFF);
    });
