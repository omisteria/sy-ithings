// first - temperature, second - pulse duration in secs
var converter = Func.rangeConverter(0.0, 1.0, 2, 10);
converter.sourceValue = 0.5;

Process
    .create(function() {
        return {pulsePeriod: 10, pulseDuration: converter.targetValue};
    })
    .exec(function(way) {
        print("Timer " + way + ", " + Func.random(5, 25));
        print("Time " + Date.now());
    });

Scheduler
    .create()
    .delay(17000)
    .exec(function() {
        print('Changed');
        converter.sourceValue = 0.9;
    });
