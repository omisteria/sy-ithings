package com.synapse.ms.ithings.io.transport.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MqttBrokerConnectionTests {

    @Test
    public void messageConsumer() throws /*ConfigurationException,*/ MqttException {

        MqttBrokerConnection connection = new MqttBrokerConnection("123.123.123.123", null,"MqttBrokerConnectionTests");
        // Expect no consumers
        assertFalse(connection.hasConsumers());

        // Add a consumer (expect consumers getTopic() to be called)
        MqttMessageSubscriber subscriber = mock(MqttMessageSubscriber.class);
        when(subscriber.getTopic()).thenAnswer(i -> "topic");
        connection.addConsumer(subscriber);
        verify(subscriber).getTopic();
        assertTrue(connection.hasConsumers());

        // Remove consumer
        connection.removeConsumer(subscriber);
        assertFalse(connection.hasConsumers());
    }

    @Test
    public void reconnectPolicyDefault() throws MqttException, InterruptedException {
        MqttBrokerConnection connection = new MqttBrokerConnection("123.123.123.123", null,
                "MqttBrokerConnectionTests");

        // Check if the default policy is set and that the broker within the policy is set.
        assertTrue(connection.getReconnectStrategy() instanceof PeriodicReconnectStrategy);
        AbstractReconnectStrategy p = connection.getReconnectStrategy();
        assertThat(p.getBrokerConnection(), equalTo(connection));
    }

    @Test
    public void reconnectPolicy()
            throws MqttException, InterruptedException {
        MqttBrokerConnection connection = spy(new MqttBrokerConnection("123.123.123.123", null,
                "MqttBrokerConnectionTests"));

        connection.connectionCallbacks = new MqttBrokerConnection.ConnectionCallbacks(connection);
        connection.connectionToken = mock(IMqttToken.class);

        // Check setter
        //connection.setReconnectStrategy(new PeriodicReconnectStrategy());
        //assertThat(connection.getReconnectStrategy().getBrokerConnection(), equalTo(connection));

        // Prepare a Mock to test if lostConnect is called and
        // if the PeriodicReconnectPolicy indeed calls buildAll()
        PeriodicReconnectStrategy mockPolicy = spy(new PeriodicReconnectStrategy(10000, 0));
        //doNothing().when(connection).buildAll();
        doCallRealMethod().when(connection).start();
        //connection.buildAll();
        mockPolicy.start();
        //mockPolicy.lostConnection();

        // Fake a disconnect
        connection.setReconnectStrategy(mockPolicy);
        doReturn(MqttConnectionState.DISCONNECTED).when(connection).connectionState();
        IMqttToken token = mock(IMqttToken.class);
        when(token.getException()).thenReturn(new org.eclipse.paho.client.mqttv3.MqttException(1));
        connection.isConnecting = true; /* Pretend that buildAll did something */
        connection.connectionCallbacks.onFailure(token, null);

        // Check lostConnect
        verify(mockPolicy).lostConnection();
        Thread.sleep(10);
        //verify(connection).buildAll();
        assertTrue(mockPolicy.isReconnecting());

        // Fake connection established
        connection.connectionCallbacks.onSuccess(token);
        assertFalse(mockPolicy.isReconnecting());
    }

    @Test
    public void timeoutWhenNotReachable() throws MqttException, InterruptedException {
        MqttBrokerConnection connection = spy(new MqttBrokerConnection("123.123.123.123", null,
                "MqttBrokerConnectionTests"));
        connection.connectionCallbacks = new MqttBrokerConnection.ConnectionCallbacks(connection);
        connection.connectionToken = mock(IMqttToken.class);

        ScheduledThreadPoolExecutor s = new ScheduledThreadPoolExecutor(1);
        connection.setTimeoutExecutor(s, 10);
        assertThat(connection.timeoutExecutor, is(s));

        Semaphore semaphore = new Semaphore(1);
        semaphore.acquire();

        MqttConnectionObserver o = new MqttConnectionObserver() {
            @Override
            public void connectionStateChanged(MqttConnectionState state, Throwable error) {
                semaphore.release();
            }
        };
        connection.addConnectionObserver(o);
        connection.start();
        assertNotNull(connection.timeoutFuture);

        assertThat(semaphore.tryAcquire(500, TimeUnit.MILLISECONDS), is(true));
    }

    @Test
    public void connectionObserver() throws MqttException {
        MqttBrokerConnection connection = spy(new MqttBrokerConnection("123.123.123.123", null,
                "MqttBrokerConnectionTests"));
        connection.connectionCallbacks = new MqttBrokerConnection.ConnectionCallbacks(connection);
        connection.connectionToken = mock(IMqttToken.class);

        // Add an observer
        assertFalse(connection.hasConnectionObservers());
        MqttConnectionObserver connectionObserver = mock(MqttConnectionObserver.class);
        connection.addConnectionObserver(connectionObserver);
        assertTrue(connection.hasConnectionObservers());

        // Adding a connection observer should not immediately call its connectionStateChanged() method.
        verify(connectionObserver, times(0)).connectionStateChanged(eq(MqttConnectionState.DISCONNECTED), any());

        // Cause a success callback
        doReturn(MqttConnectionState.CONNECTED).when(connection).connectionState();
        //connection.connectionStateOverwrite = MqttConnectionState.CONNECTED;
        connection.connectionCallbacks.onSuccess(null);
        verify(connectionObserver, times(1)).connectionStateChanged(eq(MqttConnectionState.CONNECTED), isNull());

        // Cause a failure callback with a mocked token
        IMqttToken token = mock(IMqttToken.class);
        org.eclipse.paho.client.mqttv3.MqttException testException = new org.eclipse.paho.client.mqttv3.MqttException(
                1);
        when(token.getException()).thenReturn(testException);

        doReturn(MqttConnectionState.DISCONNECTED).when(connection).connectionState();
        //connection.connectionStateOverwrite = MqttConnectionState.DISCONNECTED;
        connection.connectionCallbacks.onFailure(token, null);
        verify(connectionObserver, times(1)).connectionStateChanged(eq(MqttConnectionState.DISCONNECTED),
                eq(testException));

        // Remove observer
        connection.removeConnectionObserver(connectionObserver);
        assertFalse(connection.hasConnectionObservers());
    }

    @Test
    public void lastWillAndTestamentTests() {
        MqttBrokerConnection connection = spy(new MqttBrokerConnection("123.123.123.123", null,
                "MqttBrokerConnectionTests"));

        assertNull(connection.getLastWill());
        assertNull(MqttWillAndTestament.fromString(""));
        connection.setLastWill(MqttWillAndTestament.fromString("topic:message:1:true"));
        assertTrue(connection.getLastWill().getTopic().equals("topic"));
        assertEquals(1, connection.getLastWill().getQos());
        assertEquals(true, connection.getLastWill().isRetain());
        byte b[] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };
        assertTrue(Arrays.equals(connection.getLastWill().getPayload(), b));
    }

    @Test(expected = IllegalArgumentException.class)
    public void lastWillAndTestamentConstructorTests() {
        new MqttWillAndTestament("", new byte[0], 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void qosInvalid() {
        MqttBrokerConnection connection = spy(new MqttBrokerConnection("123.123.123.123", null,
                "MqttBrokerConnectionTests"));
        connection.setQos(10);
    }

    @Test
    public void setterGetterTests() {
        MqttBrokerConnection connection = spy(new MqttBrokerConnection("123.123.123.123", null,
                "setterGetterTests"));
        assertEquals("URL getter", connection.getHost(), "123.123.123.123");
        assertEquals("Name getter", connection.getPort(), 1883); // Check for non-secure port
        assertEquals("ClientID getter", "setterGetterTests", connection.getClientId());

        connection.setCredentials("user@!", "password123@^");
        assertEquals("User getter/setter", "user@!", connection.getUser());
        assertEquals("Password getter/setter", "password123@^", connection.getPassword());

        assertEquals(MqttBrokerConnection.DEFAULT_KEEPALIVE_INTERVAL, connection.getKeepAliveInterval());
        connection.setKeepAliveInterval(80);
        assertEquals(80, connection.getKeepAliveInterval());

        assertFalse(connection.isRetain());
        connection.setRetain(true);
        assertTrue(connection.isRetain());

        assertEquals(MqttBrokerConnection.DEFAULT_QOS, connection.getQos());
        connection.setQos(2);
        assertEquals(2, connection.getQos());
        connection.setQos(1);
        assertEquals(1, connection.getQos());

        // Check for default ssl context provider and reconnect policy
        assertNotNull(connection.getReconnectStrategy());

        assertThat(connection.connectionState(), equalTo(MqttConnectionState.DISCONNECTED));
    }

    public static void main(String[] args) throws MqttException, InterruptedException {

        MqttBrokerConnectionConfig config = new MqttBrokerConnectionConfig();
        config.host = "192.168.3.10";

        MqttService service = new MqttService();

        MqttBrokerConnection c = service.addBrokerConnection(config.getBrokerID(), config);
        c.start(); // Start connection

        MqttMessageSubscriber subscriber = new MqttMessageSubscriber() {

            @Override
            public void processMessage(String topic, byte[] payload) {
                System.out.println(topic);
                System.out.println(new String(payload));
            }

            @Override
            public String getTopic() {
                return "/sy/ithings/status";
            }
        };

        c.addConnectionObserver((state, error) -> {
            System.out.println(state);
            if (MqttConnectionState.CONNECTED.equals(state)) {
                try {
                    c.addConsumer(subscriber);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread.sleep(20000);
    }
}
