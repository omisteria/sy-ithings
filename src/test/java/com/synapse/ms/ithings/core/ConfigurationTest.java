package com.synapse.ms.ithings.core;

import com.synapse.ms.ithings.core.config.PlainConfiguration;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConfigurationTest {


    @Before
    public void prepare() {

    }

    @Test
    public void readConfigFromFile() {
        PlainConfiguration configuration = PlainConfiguration.loadFromFile("sonoff-test.json");

        assertNotNull(configuration);
        assertNotNull(configuration.get("gid"));
    }

    @Test
    public void convertToOwservConfiguration() {
        PlainConfiguration configuration = PlainConfiguration.loadFromFile("owserver-test.json");

        //OwserverConfiguration owserverConfiguration = configuration.as(OwserverConfiguration.class);

        //assertNotNull(owserverConfiguration);
    }

    @Test
    public void convertToSonoffConfiguration() {
        // PlainConfiguration configuration = PlainConfiguration.loadFromFile("sonoff-test.json");

        // SonoffConfiguration sonoffConfiguration = configuration.as(SonoffConfiguration.class);

        // assertNotNull(sonoffConfiguration);
        /*BaseBridgeDescription bd = configuration.findBridgeDescription("testBridge2");
        assertNotNull(bd);
        assertEquals("testBridge2", bd.getGid());

        BaseRepresenterDescription rd = bd.findRepresenterDescription("repr2");
        assertNotNull(rd);
        assertEquals("repr2", rd.getGid());

        rd = bd.findRepresenterDescription("repr2-not-exists");
        assertNull(rd);*/
    }
}
