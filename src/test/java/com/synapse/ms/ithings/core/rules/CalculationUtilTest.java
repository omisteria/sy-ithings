package com.synapse.ms.ithings.core.rules;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synapse.ms.ithings.core.rules.model.*;

import java.util.Date;
import java.util.Map;
import java.util.function.Function;

public class CalculationUtilTest {

    public static void main(String[] args) {
        Range<Double> r = new FloatRange(-15d, 121.3d);
        //System.out.println("="+r.toIndex(-5));

        Range<Double> rr = new FloatRange(121.3d, 15d);
        //System.out.println("="+rr.toIndex(16));

        Range<Double> r2 = new FloatRange(0.25d, 1d);
        /*Range r2r = new FloatRange(1d, 0.25d);
        System.out.println("="+r.index(103.15d));

        Range r3 = new PeakFloatRange(-15d, 21.3d, 25d);
        System.out.println("="+r2.partBy(r3.index(22d)));
        System.out.println("="+r2.partBy(r3.index(-5d)));
        System.out.println("="+r2.partBy(r3.index(10d)));
        System.out.println("="+r2.partBy(r3.index(18d)));
        System.out.println("="+r2.partBy(r2r.index(0.625d)));*/

        int ventPulseDuration = 2; // 2 minutes
        int ventPulsePeriod = 10; // 10 minutes
        RangeValueHolder<Double, Integer> valueHolder = new RangeValueHolder<>(
                new FloatRange(0d, 1d), // 20.2 - 22.3
                new IntRange(ventPulseDuration, ventPulsePeriod));
        valueHolder.setSourceValue(0.5d);

        ObjectMapper oMapper = new ObjectMapper();
        Function<Void, Map<String, Integer>> processConfResolver = p ->
                oMapper.convertValue(new DiscretProcessConf(10, valueHolder.getTargetValue()), Map.class);

        new DiscreteProcess()
                .create(1000, processConfResolver)
                .exec(way -> {
                    System.out.println("Timer " + way + ", " + (new Date()).toString());
                    return null;
                });

        //changeCs(valueHolder, 0.6d);
        changeCs(valueHolder, 0);
        changeCs(valueHolder, 0.99d);
    }

    private static void changeCs(RangeValueHolder valueHolder, double value) {
        try {
            Thread.sleep(25000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        valueHolder.setSourceValue(value);
        System.out.println("Changed to "+value);
    }
}
