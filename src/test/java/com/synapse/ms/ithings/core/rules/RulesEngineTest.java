package com.synapse.ms.ithings.core.rules;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.RulesEngineStarter;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.model.SystemEngine;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.core.rules.impl.JavaScriptLoadEngine;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.stream.Collectors;

public class RulesEngineTest {

    @Test
    public void start_engine() {

        SystemEngine systemEngine = new SystemEngine();
        RulesEngine rulesEngine = new BaseRulesEngine();
        ScriptLoadEngine scriptLoadEngine = new JavaScriptLoadEngine(rulesEngine, null);
        scriptLoadEngine.start();

        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream("test-rules.js");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String script = bufferedReader.lines().collect(Collectors.joining("\n"));

            scriptLoadEngine.loadScript(script);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (RuleScriptException e) {
            e.printStackTrace();
        }

        rulesEngine.start();


        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void start_engine_with_representers() {

        RepresenterManager representerManager = new RepresenterManager();

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                System.out.println("Bus event "+event.getChannel() + " = " +event.getState() + ", " + event.getChannel().getStateType());
            }
        });

        Arrays.asList("sonoff-test.json")
                .forEach(fileName -> {
                    Domain domain = DomainDiscoveryService.loadFromFile(fileName);
                    if (domain != null) {
                        representerManager.buildAllAsync(domain.getConfiguration());
                    }
                });

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        RulesEngine rulesEngine = new BaseRulesEngine();
        new RulesEngineStarter("test-rules.js", rulesEngine, representerManager).run();


        try {
            Thread.sleep(500000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void start_engine_with_discret_process() {
        RepresenterManager representerManager = new RepresenterManager();
        RulesEngine rulesEngine = new BaseRulesEngine();
        new RulesEngineStarter("rules-test-proc.js", rulesEngine, representerManager).run();

        try {
            Thread.sleep(500000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
