package com.synapse.ms.ithings.core.rules;

import com.synapse.ms.ithings.core.rules.provider.DateProvider;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class DateProviderTest {

    // Date.between('4.*.*','6.*.*')
    // Date.between('*.4.*','*.6.*')
    // Date.between('4.5.*','6.7.*')
    // Date.between('4.05.2000','6.07.2001')

    @Test
    public void compare_pattern_curr() {
        LocalDate now = LocalDate.now();

        DateProvider.DatePattern p = DateProvider.DatePattern.parse("24.12.*");
        Assert.assertTrue(p.isBefore(now));

        p = DateProvider.DatePattern.parse("29.12.*");
        Assert.assertFalse(p.isBefore(now));

        p = DateProvider.DatePattern.parse("22.12.*");
        Assert.assertFalse(p.isAfter(now));

        p = DateProvider.DatePattern.parse("29.12.*");
        Assert.assertTrue(p.isAfter(now));
    }

    @Test
    public void between_dates_exclude() {
        DateProvider t = new DateProvider(null);
        Filter f = t.between("24.12.*","30.12.*");
        Assert.assertTrue(f.filtered(null));

        f = t.between("24.12.*","25.12.*");
        Assert.assertFalse(f.filtered(null));
    }

    @Test
    public void between_dates_include() {
        DateProvider t = new DateProvider(null);
        Filter f = t.between("24.12.*","26.12.*");
        Filter f2 = t.is("26.12.*");
        Assert.assertTrue(f.filtered(null) || f2.filtered(null));

        f2 = t.is("27.12.*");
        Assert.assertFalse(f.filtered(null) || f2.filtered(null));
    }

    @Test
    public void between_dates_over_start_year() {
        DateProvider t = new DateProvider(null);
        Filter f = t.between("24.12.*","26.1.*");

        Assert.assertTrue(f.filtered(null));
    }
}
