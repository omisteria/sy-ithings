package com.synapse.ms.ithings.core.events;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InternalBusTest {

    @Before
    public void prepare() {

    }

    @Test
    public void observe_should_receive_all_events() throws Exception {
        /*final Observer<Object> observer = new ObserverExt();
        final Observer<Object> observer2 = new ObserverExt();
        final SimpleEvent firstEvent = new SimpleEvent();
        final SimpleEvent2 secondEvent = new SimpleEvent2();

        InternalRxBus rxBus = InternalRxBus.getInstance();
        rxBus.subscribe(SimpleEvent.class, observer);
        rxBus.subscribe(SimpleEvent2.class, observer2);
        //rxBus.observe().ofType(SimpleEvent.class).subscribe(observer);

        rxBus.emit(firstEvent);
        rxBus.emit(secondEvent);*/

    }

    class SimpleEvent {
        @Override
        public String toString() {
            return "SimpleEvent{}";
        }
    }

    class SimpleEvent2 {
        @Override
        public String toString() {
            return "SimpleEvent2{}";
        }
    }
}
