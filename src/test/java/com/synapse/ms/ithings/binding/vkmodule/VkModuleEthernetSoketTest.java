package com.synapse.ms.ithings.binding.vkmodule;

import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

public class VkModuleEthernetSoketTest {

    @Test
    public void read_state_of_sensor_soket2() throws Exception {

        try {
            DomainDiscoveryService.registerDomain(new VkModuleDomain());

            RepresenterManager representerManager = new RepresenterManager();
            representerManager.buildAll(PlainConfiguration.loadFromFile("vkm-eth-soket2.json"));
            SystemEngineContext.getBus()
                    .subscribe(new StateEventBusEventListener() {
                        @Override
                        public void handle(StateEventBusEvent event) {
                           System.out.println("Bus event "+event.getChannel().getGid() + ": " + event.getState());
                        }
                   });


            Thread.sleep(1000);

            /*Representer pep = representerManager.getRepresenterRegistry().get("vkm-ethernet.repr1");
            Channel channel =  pep.getChannel("vkm-ethernet.repr1.relay0");
            channel.post(OnOffType.ON);

            Thread.sleep(4000);

            channel.post(OnOffType.OFF);*/

            /*for (int i=0; i<10; i++) {
                representerManager.getBus().publish(new StateEventBusEvent(channel, OnOffType.ON));
            }*/


            //Representer rep1 = representerManager.getRepresenterRegistry().get("repr1");
            //Channel channel1 = rep1.getChannel("1");

            /*channel1.post(OnOffType.ON);
            Thread.sleep(3000);
            channel1.post(OnOffType.OFF);*/


            Thread.sleep(905000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void read_state_of_sensor_soket4() throws Exception {

        try {
            DomainDiscoveryService.registerDomain(new VkModuleDomain());

            RepresenterManager representerManager = new RepresenterManager();
            representerManager.buildAll(PlainConfiguration.loadFromFile("vkm-eth-soket4.json"));
            SystemEngineContext.getBus()
                    .subscribe(new StateEventBusEventListener() {
                        @Override
                        public void handle(StateEventBusEvent event) {
                           System.out.println("Bus event "+event.getChannel().getGid() + ": " + event.getState());
                        }
                   });


            Thread.sleep(5000);

            /*Representer pep = representerManager.getRepresenterRegistry().get("repr1");
            Channel channel =  pep.getChannel("0");*/

            /*for (int i=0; i<10; i++) {
                representerManager.getBus().publish(new StateEventBusEvent(channel, OnOffType.ON));
            }*/


            //Representer rep1 = representerManager.getRepresenterRegistry().get("repr1");
            //Channel channel1 = rep1.getChannel("1");

            /*channel1.post(OnOffType.ON);
            Thread.sleep(3000);
            channel1.post(OnOffType.OFF);*/


            Thread.sleep(905000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
