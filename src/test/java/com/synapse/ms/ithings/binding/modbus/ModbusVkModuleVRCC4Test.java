package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.common.types.BooleanType;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

public class ModbusVkModuleVRCC4Test {

    /*
        {
          "gid": "cont1",
          "type": "DRY_CONTACT",
          "commandState": "2:0:1",
          "pollingPeriod": 30
        },
        {
          "gid": "cont2",
          "type": "DRY_CONTACT",
          "commandState": "2:1:1",
          "pollingPeriod": 40
        },
        {
          "gid": "cont3",
          "type": "DRY_CONTACT",
          "commandState": "2:2:1",
          "pollingPeriod": 5
        },
        {
          "gid": "cont4",
          "type": "DRY_CONTACT",
          "commandState": "2:3:1",
          "pollingPeriod": 15
        },
        {
          "gid": "relay1",
          "type": "SWITCH",
          "commandState": "1:0:1",
          "commandChange": "5:0",
          "pollingPeriod": 7
        }
    */
    @Test
    public void read_data() {

        System.setProperty("com.synapse.ms.ithings.io.modbus.debug", "true");

        try {
            // DomainDiscoveryService.registerDomain(new ModbusDomain());

            RepresenterManager representerManager = new RepresenterManager();
            representerManager.buildAll(PlainConfiguration.loadFromFile("vkm-modbus-vrcc4-test.json"));
            SystemEngineContext.getBus()
                    .subscribe(new StateEventBusEventListener() {
                        @Override
                        public void handle(StateEventBusEvent event) {
                            System.out.println("Bus event "+event.getChannel().getGid() + ": " + event.getState());
                        }
                    });


            Thread.sleep(5000);

            Representer rep = representerManager.getRepresenterRegistry().get("vkm-modbus.vrc-c4");
            Channel ch =  rep.getChannel("vkm-modbus.vrc-c4.relay1");
            ch.post(BooleanType.TRUE);
            Thread.sleep(3000);
            ch.post(BooleanType.FALSE);

            Thread.sleep(905000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
