package com.synapse.ms.ithings.binding.sonoffmqtt;

import com.synapse.ms.ithings.binding.sonoff.SonoffDomain;
import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

import java.util.Arrays;

public class SonoffMqttConnectionTest {

    @Test
    public void read_state_of_sensor() throws Exception {

        DomainDiscoveryService.registerDomain(new SonoffDomain());

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                System.out.println("Bus event "+event.getChannel() + " = " +event.getState());
            }
        });
        RepresenterManager representerManager = new RepresenterManager();
        representerManager.buildAll(PlainConfiguration.loadFromFile("sonoff-test.json"));

        try {
            Thread.sleep(5000);

            Representer rep1 = representerManager.getRepresenterRegistry().get("sonoff.x-switch1");
            Channel channelA = rep1.getChannel("sonoff.x-switch1.relay");
            /*channelA.register(new StateObserver() {
                @Override
                public void onChanged(Channel channel, State state) {
                    System.out.println("1."+state);
                }

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    //System.out.println("2."+evt.getNewValue());
                }
            });*/
            //channelA.getState().addListener(event -> System.out.println("Bus event "+event.getValue()));
            /*addListener(new ChannelListenerAdapter() {
                @Override
                public void onChannelStateChanged(ChannelStateEvent event) {
                    System.out.println("Bus event "+event.getState());
                }
            });*/


            channelA.post(OnOffType.ON);
            //Thread.sleep(3000);
            //channelA.post(OnOffType.OFF);


            Thread.sleep(35000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void initializa_representers_rf_bridge() throws Exception {

        RepresenterManager representerManager = new RepresenterManager();

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                System.out.println("Bus event "+event.getChannel() + " = " +event.getState() + ", " + event.getChannel().getStateType());
            }
        });

        Arrays.asList("sonoff-rf-test.json")
                .forEach(fileName -> {
                    Domain domain = DomainDiscoveryService.loadFromFile(fileName);
                    if (domain != null) {
                        representerManager.buildAllAsync(domain.getConfiguration());
                    }
                });

        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
