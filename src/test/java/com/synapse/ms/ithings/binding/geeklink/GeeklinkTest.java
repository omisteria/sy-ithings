package com.synapse.ms.ithings.binding.geeklink;

import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.common.types.StringType;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.ref.IRCode;
import org.junit.Test;

public class GeeklinkTest {

    @Test
    public void read_state_of_sensor() throws Exception {

        DomainDiscoveryService.registerDomain(new GeeklinkDomain());

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                System.out.println("Bus event "+event.getChannel() + " = " +event.getState());
            }
        });
        RepresenterManager representerManager = new RepresenterManager();
        representerManager.buildAll(PlainConfiguration.loadFromFile("geeklink-test.json"));

        try {
            Thread.sleep(5000);

            Representer rep1 = representerManager.getRepresenterRegistry().get("irblaster1");
            Channel channelIr = rep1.getChannel("ir");


            //channelIr.post(StringType.valueOf("{\"Protocol\":\"NEC\",\"Bits\":32,\"Data\":\"0x01FE48B7\"}"));
            channelIr.post(StringType.valueOf(IRCode.Philips190TW.powerToggle));
            /*Thread.sleep(3000);
            channelA.post(OnOffType.OFF);*/


            Thread.sleep(35000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
