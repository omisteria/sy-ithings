package com.synapse.ms.ithings.binding.mockpoll;

import com.synapse.ms.ithings.core.model.bridge.AbstractBridge;
import com.synapse.ms.ithings.core.model.represent.Representer;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MockBridgeSocketEvent extends AbstractBridge {

    public MockBridgeSocketEvent(String gid, Map<String, Object> description) {
        super(gid, description);

        /*setDataHandler(new DataHandler<byte[]>() {

            @Override
            public AcquisitionStrategy getAcquisitionStrategy() {
                return new SocketAcquisitionStrategy();
            }

            @Override
            public void onReceiveReadData(byte[] data) throws Exception {

            }

            @Override
            public void onReceiveWriteData(byte[] data) throws Exception {

            }

            @Override
            public void sendReadRequest(HandlerCallback callback) throws Exception {

            }

            @Override
            public void sendWriteRequest(HandlerCallback callback) throws Exception {

            }
        });*/
    }


    @Override
    public void activate() throws Exception {

    }

    @Override
    public void dispose() throws Exception {

    }

    @Override
    public List<Representer> getRepresenters() {
        return Collections.EMPTY_LIST;
    }


}
