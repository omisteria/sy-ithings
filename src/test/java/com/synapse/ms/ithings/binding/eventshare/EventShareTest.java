package com.synapse.ms.ithings.binding.eventshare;

import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

public class EventShareTest {
    //

    @Test
    public void read_data() {
        try {
            DomainDiscoveryService.registerDomain(new EventShareDomain());

            RepresenterManager representerManager = new RepresenterManager();
            representerManager.buildAll(PlainConfiguration.loadFromFile("eventshare-test.json"));
            representerManager.buildAll(PlainConfiguration.loadFromFile("vkm-eth-soket4.json"));

            Thread.sleep(5000);
            Representer pep = representerManager.getRepresenterRegistry().get("vkm-ethernet.repr1");
            Channel channel =  pep.getChannel("vkm-ethernet.repr1.0");
            channel.getState().update(OnOffType.ON);

            Thread.sleep(905000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
