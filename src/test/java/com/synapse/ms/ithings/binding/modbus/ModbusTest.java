package com.synapse.ms.ithings.binding.modbus;

import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

public class ModbusTest {

    @Test
    public void read_data() {
        try {
            // DomainDiscoveryService.registerDomain(new ModbusDomain());

            RepresenterManager representerManager = new RepresenterManager();
            representerManager.buildAll(PlainConfiguration.loadFromFile("vkm-modbus-test.json"));
            SystemEngineContext.getBus()
                    .subscribe(new StateEventBusEventListener() {
                        @Override
                        public void handle(StateEventBusEvent event) {
                            System.out.println("Bus event "+event.getChannel().getGid() + ": " + event.getState());
                        }
                    });


            Thread.sleep(5000);

            Representer rep = representerManager.getRepresenterRegistry().get("vkm-modbus.smd220");
            Channel chElectricityActivePower =  rep.getChannel("vkm-modbus.smd220.electricityActivePower");
            // chElectricityActivePower.post(StringType.valueOf(""));


            Thread.sleep(905000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void read_data_arduino() {

        System.setProperty("com.synapse.ms.ithings.io.modbus.debug", "true");

        try {
            RepresenterManager representerManager = new RepresenterManager();
            representerManager.buildAll(PlainConfiguration.loadFromFile("modbus-arduino-vent-test.json"));
            SystemEngineContext.getBus()
                    .subscribe(new StateEventBusEventListener() {
                        @Override
                        public void handle(StateEventBusEvent event) {
                            System.out.println("Bus event "+event.getChannel().getGid() + ": " + event.getState());
                        }
                    });


            Thread.sleep(5000);

            /* {
      "gid": "arduino1",
      "driver": "generic",
      "id": "11",
      "channels": [
        {
          "gid": "temp",
          "type": "NUM_SHORT",
          "commandState": "4:11:1",
          "offset": 0,
          "scale": 1,
          "precision": 1,
          "pollingPeriod": 10
        }

      ]
    } */
            // "commandState": "4:11:1" - read desirable temperature
            // "commandState": "4:13:1" - read current temperature
            // "commandState": "4:15:1" - read GISTEREZIS temperature
            // "commandState": "4:17:1" - read stream intensity
            // "commandState": "4:19:1" - read stream base period
            // "commandState": "4:21:1" - read STATE FAN or 20
            // "commandState": "4:22:1" - read STATE HEATER or 21
            // "commandState": "4:23:1" - read ENABLED FAN or 22
            // "commandState": "4:24:1" - read ENABLED HEATER or 23


    /* {
      "gid": "arduino1",
      "driver": "generic",
      "id": "11",
      "channels": [
        {
          "gid": "temp",
          "type": "NUM_SHORT",
          "commandChange": "6:11:1"
        }

      ]
    } */
    /*I_MODBUS_DEVICE_ID 1
    I_MODBUS_SPEED 2*/

            // "commandChange": "6:11:2" - write desirable temperature
            // "commandChange": "6:15:2" - write AIR_TEMP_GISTEREZIS
            // "commandChange": "6:17:2" - write stream intensity
            // "commandChange": "6:19:2" - write stream AIR BASE_IMPULSE
            // "commandChange": "6:22:2" - write ENABLED FAN
            // "commandChange": "6:23:2" - write ENABLED HEATER
            // "commandChange": "6:25:2" - write PERIOD FREEZE

            Representer rep = representerManager.getRepresenterRegistry().get("vkm-modbus.arduino1");
            Channel ch =  rep.getChannel("vkm-modbus.arduino1.temp");
            //ch.post(DecimalType.valueOf("110"));
            // ch.post(DecimalType.valueOf("170"));
            //ch.post(BooleanType.FALSE);


            Thread.sleep(905000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
