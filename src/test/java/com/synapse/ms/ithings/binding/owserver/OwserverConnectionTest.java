package com.synapse.ms.ithings.binding.owserver;

import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.common.types.OnOffType;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

public class OwserverConnectionTest {

    //

    @Test
    public void testConnection() throws Exception {

        DomainDiscoveryService.registerDomain(new OwserverDomain());

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                System.out.println("Bus event "+event.getChannel() + " = " +event.getState());
            }
        });
        RepresenterManager representerManager = new RepresenterManager();
        representerManager.buildAll(PlainConfiguration.loadFromFile("owserver-test.json"));

        //OwserverConfiguration configuration = PlainConfiguration.loadFromFileAs("owserver-test.json", OwserverConfiguration.class);

        //RepresenterManager manager = new RepresenterManager();
        //manager.buildAll(configuration);

        try {
            Thread.sleep(5000);

            // Representer rep1 = representerManager.getRepresenterRegistry().get("owserver.temperature1");
            /*Channel channel1 = representerManager.getChannelRegistry().get("owserver.switch1.relay");
            if (channel1 != null) {

                channel1.post(OnOffType.ON);
                Thread.sleep(5000);
                channel1.post(OnOffType.OFF);
                Thread.sleep(5000);
                channel1.post(OnOffType.ON);
                Thread.sleep(5000);
                channel1.post(OnOffType.OFF);
            }*/

            Channel channel1 = representerManager.getChannelRegistry().get("owserver.move1.movement");

            /*channelA.post(OnOffType.ON);
            Thread.sleep(3000);
            channelA.post(OnOffType.OFF);*/


            Thread.sleep(155000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
