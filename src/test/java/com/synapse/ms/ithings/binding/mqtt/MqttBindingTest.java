package com.synapse.ms.ithings.binding.mqtt;

import com.synapse.ms.ithings.core.Domain;
import com.synapse.ms.ithings.core.DomainDiscoveryService;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEvent;
import com.synapse.ms.ithings.core.common.events.bus.StateEventBusEventListener;
import com.synapse.ms.ithings.core.config.PlainConfiguration;
import com.synapse.ms.ithings.core.model.SystemEngineContext;
import com.synapse.ms.ithings.core.model.channel.Channel;
import com.synapse.ms.ithings.core.model.represent.Representer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import org.junit.Test;

import java.util.Arrays;

public class MqttBindingTest {

    @Test
    public void read_state_of_sensor() throws Exception {

        RepresenterManager representerManager = new RepresenterManager();

        SystemEngineContext.getBus().subscribe(new StateEventBusEventListener() {
            @Override
            public void handle(StateEventBusEvent event) {
                System.out.println("Bus event "+event.getChannel() + " = " +event.getState() + ", " + event.getChannel().getStateType());
            }
        });

        Arrays.asList("mqtt-test.json")
                .forEach(fileName -> {
                    Domain domain = DomainDiscoveryService.loadFromFile(fileName);
                    if (domain != null) {
                        representerManager.buildAll(domain.getConfiguration());
                    }
                });

        try {
            Representer rep1 = representerManager.getRepresenterRegistry().get("mqtt.oldgate");
            Channel channel1 = rep1.getChannel("mqtt.oldgate.electricityActivePower");

            /*channelA.post(OnOffType.ON);
            Thread.sleep(3000);
            channelA.post(OnOffType.OFF);*/


            Thread.sleep(350000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
