package com.synapse.ms.ithings;

import com.despegar.http.client.GetMethod;
import com.despegar.http.client.HttpClientException;
import com.despegar.http.client.HttpResponse;
import com.despegar.sparkjava.test.SparkServer;
import com.synapse.ms.ithings.core.model.represent.RepresenterManager;
import com.synapse.ms.ithings.web.SystemController;
import org.junit.ClassRule;
import org.junit.Test;
import spark.servlet.SparkApplication;

import static org.junit.Assert.*;

public class SystemContrtollerTest {

    public static class TestContollerTestSparkApplication implements SparkApplication {
        @Override
        public void init() {
            RepresenterManager representerManager = new RepresenterManager();
            new SystemController(representerManager);
        }
    }

    @ClassRule
    public static SparkServer<TestContollerTestSparkApplication> testServer = new SparkServer<>(SystemContrtollerTest.TestContollerTestSparkApplication.class, 4567);

    @Test
    public void serverRespondsSuccessfully() throws HttpClientException {
        GetMethod request = testServer.get("/", false);
        HttpResponse httpResponse = testServer.execute(request);
        assertEquals(404, httpResponse.code());
    }

    @Test
    public void testHelloEp() throws Exception {
		/* The second parameter indicates whether redirects must be followed or not */
        GetMethod get = testServer.get("/hello", false);
        get.addHeader("Test-Header", "test");
        HttpResponse httpResponse = testServer.execute(get);

        assertEquals(200, httpResponse.code());
        assertTrue(new String(httpResponse.body()).contains("SUCCESS"));
        assertNotNull(testServer.getApplication());
    }

    /*@Test
    public void testHello() {

        String inst = "Test String";

        String model = EasyMock.createMock(String.class);
        //replay(model);

        PostsCreateHandler handler = new PostsCreateHandler(inst);
        assertEquals(new Answer(400), handler.process(newPost, Collections.emptyMap(), false));
        assertEquals(new Answer(400), handler.process(newPost, Collections.emptyMap(), true));

        verify(model);
    }*/
}
